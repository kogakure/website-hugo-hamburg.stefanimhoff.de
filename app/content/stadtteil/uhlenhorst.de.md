---
title: Uhlenhorst
slug: uhlenhorst
author: Stefan Imhoff
date: 2015-08-16T18:00:00+02:00
distance: 12
duration: 2:45
---

Von der U-Bahn-Station *Mundsburg* aus bin ich unter den Gleisen der U-Bahn nach Osten gegangen, wo gerade ein neues Wohngebiet gebaut wird. Nach Süden zum Gelände der *Fachhochschule HAW*. Wieder nach Norden unter den Gleisen hindurch, dann die *Richardstraße* nach Süden und über den *Eilbekkanal*.

{{< image src="stadtteile/uhlenhorst-01" alt="Die Gleise der U-Bahn" width="1000" height="1334" caption="Die Gleise der U-Bahn." >}}

{{< image src="stadtteile/uhlenhorst-02" alt="Ein unbekanntes Denkmal von einem Kind mit Hund" width="1000" height="1334" caption="Ein unbekanntes Denkmal von einem Kind mit Hund." >}}

{{< image src="stadtteile/uhlenhorst-03" alt="Hausboote auf dem Eilbekkanal" width="1000" caption="Am Ufer des Eilbekkanals stehen Hausboote" >}}

{{< image src="stadtteile/uhlenhorst-04" alt="Eilbekkanal" width="1000" height="1334" >}}

{{< image src="stadtteile/uhlenhorst-05" alt="Fachhochschule HAW" width="1000" caption="Die Gebäude der Fachhochschule HAW." >}}

{{< image src="stadtteile/uhlenhorst-06" alt="Eilbekkanal" width="1000" height="1334" >}}

{{< image src="stadtteile/uhlenhorst-07" alt="Eilbekkanal" width="1000" >}}

Ich bin am *Kuhmühlenteich* entlanggegangen, um die *St. Gertrud-Kirche* herum, durch das Wohngebiet südlich des *Kuhmühlenteichs*, was noch zu *Uhlenhorst* gehört. Hier stehen, wie an vielen Stellen in *Uhlenhorst* sehr schöne alte Gebäude mit zahlreichen Verzierungen, Stuck, jedes in eigenem Stil. Manch Architekt hat sich mit Namensschild am Haus verewigt.

{{< image src="stadtteile/uhlenhorst-08" alt="Die St. Gertrud-Kirche" width="1000" caption="Die St. Gertrud-Kirche" >}}

{{< image src="stadtteile/uhlenhorst-09" alt="Die St. Gertrud-Kirche" width="1000" height="1334" >}}

{{< image src="stadtteile/uhlenhorst-10" alt="Die St. Gertrud-Kirche" width="1000" height="1334" >}}

{{< image src="stadtteile/uhlenhorst-11" alt="Denkmal mit Schutzheiligen und Kirchen Hamburgs" width="1000" caption="Denkmal mit Schutzheiligen und Kirchen Hamburgs." >}}

{{< image src="stadtteile/uhlenhorst-12" alt="Denkmal für Carl Hermann Manehut" width="1000" height="1334" caption="Denkmal für Carl Hermann Manehut bei der St. Gertrud-Kirche" >}}

{{< image src="stadtteile/uhlenhorst-13" alt="Der Kuhmühlenteich" width="1000" height="1334" caption="Der Kuhmühlenteich." >}}

{{< image src="stadtteile/uhlenhorst-14" alt="Der Kuhmühlenteich" width="1000" height="1334" >}}

{{< image src="stadtteile/uhlenhorst-15" alt="Die Kuhmühlenbrücke" width="1000" height="1334" >}}

{{< image src="stadtteile/uhlenhorst-16" alt="Der Kuhmühlenteich" width="1000" >}}

{{< image src="stadtteile/uhlenhorst-17" alt="Der Kuhmühlenteich" width="1000" >}}

{{< image src="stadtteile/uhlenhorst-18" alt="Die schönen Häuser von Uhlenhorst" width="1000" height="1334" >}}

{{< image src="stadtteile/uhlenhorst-19" alt="Die schönen Häuser von Uhlenhorst" width="1000" height="1334" >}}

{{< image src="stadtteile/uhlenhorst-20" alt="Die schönen Häuser von Uhlenhorst" width="1000" >}}

{{< image src="stadtteile/uhlenhorst-21" alt="Die schönen Häuser von Uhlenhorst" width="1000" height="1334" >}}

{{< image src="stadtteile/uhlenhorst-22" alt="Die schönen Häuser von Uhlenhorst" width="1000" height="1334" >}}

{{< image src="stadtteile/uhlenhorst-23" alt="Die schönen Häuser von Uhlenhorst" width="1000" >}}

Dann weiter nach Westen Richtung *Alster* und immer entlang des Ufers und an der Straße *Schöne Aussicht* entlang, vorbei am *Feenteich* bis zur *Imam-Ali-Moschee*.

Entlang der *Alster* gibt es viel Grünfläche und Sitzgelegenheiten mit Blick auf die *Alster*. Wer hier wohnt, hat eine Menge Geld. Auf vielen Klingelschildern stehen keine Namen, sondern nur Buchstaben oder Initialen, um die Identitäten der (vermutlich) prominenten Käufer zu schützen.

{{< image src="stadtteile/uhlenhorst-24" alt="Das Alsterufer bei Uhlenhorst" width="1000" caption="Das Alsterufer bei Uhlenhorst." >}}

{{< image src="stadtteile/uhlenhorst-25" alt="Das Alsterufer bei Uhlenhorst" width="1000" >}}

{{< image src="stadtteile/uhlenhorst-26" alt="Das Alsterufer bei Uhlenhorst" width="1000" >}}

{{< image src="stadtteile/uhlenhorst-27" alt="Das Alsterufer bei Uhlenhorst" width="1000" >}}

{{< image src="stadtteile/uhlenhorst-28" alt="Das Wolfgang Borchert-Denkmal am Alsterufer" width="1000" height="1334" caption="Das Wolfgang Borchert-Denkmal am Alsterufer." >}}

{{< image src="stadtteile/uhlenhorst-29" alt="Die teuren Häuser entlang der Straße Schöne Aussicht" width="1000" caption="Die teuren Häuser entlang der Straße Schöne Aussicht und in der Fährhausstraße. Ein paar wenige Neubauten, überwiegend Gebäude in alter Bauart." >}}

{{< image src="stadtteile/uhlenhorst-30" alt="Die teuren Häuser entlang der Straße Schöne Aussicht" width="1000" >}}

{{< image src="stadtteile/uhlenhorst-31" alt="Die teuren Häuser entlang der Straße Schöne Aussicht" width="1000" >}}

{{< image src="stadtteile/uhlenhorst-32" alt="Die teuren Häuser entlang der Straße Schöne Aussicht" width="1000" >}}

{{< image src="stadtteile/uhlenhorst-33" alt="Die teuren Häuser entlang der Straße Schöne Aussicht" width="1000" >}}

{{< image src="stadtteile/uhlenhorst-34" alt="Die Gebäude am Feenteich" width="1000" caption="Die Gebäude am Feenteich." >}}

{{< image src="stadtteile/uhlenhorst-35" alt="Imam-Ali-Moschee" width="1000" caption="Das Islamische Zentrum Hamburg in der Imam-Ali-Moschee." >}}

Weiter nach Osten durch Fährhausstraße und *Am Langenzug*, durch Winterhuder Weg und wieder nach Süden. Direkt am Feenteich ist das Konsulat der Russischen Förderation.

{{< image src="stadtteile/uhlenhorst-36" alt="Der Lange Zug" width="1000" caption="Der Lange Zug" >}}

{{< image src="stadtteile/uhlenhorst-37" alt="Der Hofwegkanal" width="1000" height="1334" caption="Der Hofwegkanal" >}}

{{< image src="stadtteile/uhlenhorst-38" alt="Der Hofwegkanal" width="1000" height="1334" >}}

{{< image src="stadtteile/uhlenhorst-39" alt="Heilandskirche am Winterhuder Weg." width="1000" height="1334" caption="Heilandskirche am Winterhuder Weg." >}}

Durch den *Hofweg* nach Norden und durch die Kanalstraße. Über den *Uhlenhorster Kanal* nach Norden und dann bis zum *Winterhuder Weg* nach Osten.

{{< image src="stadtteile/uhlenhorst-40" alt="Gebäude in Uhlenhorst" width="1000" caption="Die meisten Gebäude in Uhlenhorst sind äußerst hübsch, sehr viele schön geschmückte Fassaden." >}}

{{< image src="stadtteile/uhlenhorst-41" alt="Gebäude in Uhlenhorst" width="1000" height="1710" >}}

{{< image src="stadtteile/uhlenhorst-42" alt="Gebäude in Uhlenhorst" width="1000" >}}

{{< image src="stadtteile/uhlenhorst-43" alt="Gebäude in Uhlenhorst" width="1000" height="1309" >}}

{{< image src="stadtteile/uhlenhorst-44" alt="Gebäude in Uhlenhorst" width="1000" height="1016" >}}

{{< image src="stadtteile/uhlenhorst-45" alt="Gebäude in Uhlenhorst" width="1000" height="1334" >}}

{{< image src="stadtteile/uhlenhorst-46" alt="Gebäude in Uhlenhorst" width="1000" height="1334" >}}

{{< image src="stadtteile/uhlenhorst-47" alt="Gebäude in Uhlenhorst" width="1000" >}}

{{< image src="stadtteile/uhlenhorst-48" alt="Gebäude in Uhlenhorst" width="1000" >}}

{{< image src="stadtteile/uhlenhorst-49" alt="Gebäude in Uhlenhorst" width="1000" >}}

Ziel meiner Tour war wieder die U-Bahnstation *Mundsburg*.

{{< image src="stadtteile/uhlenhorst-50" alt="Der U-Bahnhof Mundsburg" width="1000" caption="Der U-Bahnhof Mundsburg." >}}

{{< image src="map/uhlenhorst" alt="Uhlenhorst" width="1000" height="956" >}}

<iframe class="map" src="https://www.google.com/maps/d/u/0/embed?mid=1L2qKPLoC_tKlOo3ZKlu8iKEvYpU" width="1000" height="500">
</iframe>
