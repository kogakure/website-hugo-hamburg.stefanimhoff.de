---
title: HafenCity
slug: hafencity
author: Stefan Imhoff
date: 2015-07-26T18:00:00+02:00
distance: 12
duration: 4:04
---

Von der U-Bahn-Station *Meßberg* bin ich über eine Fußgängerbrücke nach Süden in die *HafenCity* gegangen. Streng genommen beginnt der Stadtteil aber erst ab *Brooktorkai*/*Ericus*. Ich bin einmal am *Spiegel-Gebäude* vorbei über die *Ericusbrücke*, vorbei am *Lohseplatz*, durch die *Koreastraße* und dann die *Shanghaiallee* nach Süden. Östlich der Straße wird noch schwer gebaut, mehrere Gewerbe- und Wohnhäuser befinden sich noch im Bau und nicht jede Straße ist begehbar.

{{< image src="stadtteile/hafencity-01" alt="Deichtorhallen" width="1000" caption="Hier ist zwar noch nicht die HafenCity, aber der Blick auf die Deichtorhallen ist trotzdem schön." >}}

{{< image src="stadtteile/hafencity-02" alt="Fleetinsel" width="1000" >}}

{{< image src="stadtteile/hafencity-03" alt="Das Glasgebäude des Spiegel" width="1000" caption="Das Glasgebäude des Spiegel." >}}

{{< image src="stadtteile/hafencity-06" alt="Das Glasgebäude des Spiegel" width="1000" >}}

{{< image src="stadtteile/hafencity-05" alt="Das Glasgebäude des Spiegel" width="1000" height="1334" >}}

{{< image src="stadtteile/hafencity-04" alt="Das Glasgebäude des Spiegel" width="1000" height="1334" >}}

{{< image src="stadtteile/hafencity-07" alt="Neubauten in der HafenCity" width="1000" height="1334" >}}

{{< image src="stadtteile/hafencity-12" alt="Neubauten in der HafenCity" width="1000" >}}

{{< image src="stadtteile/hafencity-08" alt="Neubauten in der HafenCity" width="1000" >}}

{{< image src="stadtteile/hafencity-11" alt="Baustellen in der HafenCity" width="1000" >}}

{{< image src="stadtteile/hafencity-09" alt="Baustellen in der HafenCity" width="1000" >}}

{{< image src="stadtteile/hafencity-10" alt="Automuseum Prototyp" width="1000" height="1334" >}}

Ich bin durch den *Lohsepark* gegangen, wo sich zahlreiche Geräte wie Trampoline, Sitzmöglichkeiten und Spielgelegenheiten für Kinder befinden. An der U-Bahn-Station *HafenCity Universität* bin ich der Straße bin zum Ende gefolgt. Die *Versmannstraße* ist noch nicht gebaut, ein Weiterkommen unmöglich. Direkt am *Baakenhafen* entstehen ab 2015/2016 neue Wohnungen.

{{< image src="stadtteile/hafencity-13" alt="Lohsepark" width="1000" caption="Der Lohsepark: zahlreiche Trampolins, Sitzgelegenheiten und Kunstwerke." >}}

{{< image src="stadtteile/hafencity-15" alt="Lohsepark" width="1000" >}}

{{< image src="stadtteile/hafencity-14" alt="Lohsepark" width="1000" >}}

In der U-Bahn-Station *HafenCity Universität* habe ich mir die Lichter- und Musikshow angeschaut, die einmal pro Stunde (immer zur vollen Stunde) zwischen 10 und 18 Uhr gespielt wird. Während die Lichtinstallationen auf dem Bahnsteig normalerweise nur langsam ihre Farbe wechselt, wird während der mehrminütigen Show zu klassischer Musik eine wahre Farbexplosion abgefeuert.

{{< image src="stadtteile/hafencity-30" alt="U-Bahn-Station HafenCity Universität" width="1000" caption="U-Bahn-Station HafenCity Universität. Immer zur vollen Stunde (von 10-18 Uhr) gibt es hier eine Lichtershow zu Klassischer Musik." >}}

{{< image src="stadtteile/hafencity-33" alt="U-Bahn-Station HafenCity Universität" width="1000" >}}

{{< image src="stadtteile/hafencity-32" alt="U-Bahn-Station HafenCity Universität" width="1000" >}}

{{< image src="stadtteile/hafencity-31" alt="U-Bahn-Station HafenCity Universität" width="1000" height="1334" >}}

Über eine Brücke bin ich auf den südlichen Teil des *Baakenhafens* gelangt und der *Baakenalle* solange gefolgt bis sie plötzlich im Nichts endete. Dann bin ich umgekehrt und am *Zieselpark* vorbeigegangen. Hier kann man (solange die Wohnungen noch nicht stehen nehme ich an) auf Cross-Segways oder Kettenfahrzeugen auf einem Hindernisparcour durch den Matsch fahren.

{{< image src="stadtteile/hafencity-21" alt="Ziesel-Park" width="1000" caption="Ziesel-Park. Eine Strecke für Kettenfahrzeuge und Cross-Segways. Schlammig und dreckig." >}}

{{< image src="stadtteile/hafencity-22" alt="Ziesel-Park" width="1000" >}}

{{< youtube lrkBWnjbDQE >}}

{{< image src="stadtteile/hafencity-23" alt="Hafen" width="1000" >}}

{{< image src="stadtteile/hafencity-26" alt="Hafen" width="1000" >}}

{{< image src="stadtteile/hafencity-25" alt="Hafen" width="1000" >}}

{{< image src="stadtteile/hafencity-24" alt="Elbbrücken" width="1000" >}}

Ich bin der Umfahrung *Versmannstraße* bis zu den *Elbbrücken* gefolgt und dann umgedreht. Auch dort wird gebaut, das *Quatier Elbbrücken* wird einmal die ganze *Fleetinsel* ausfüllen. Im Park *Baakenhöft* steht eine Aussichtsplattform, von der man den Bau im *Baakenhafen* beobachten kann.

{{< image src="stadtteile/hafencity-29" alt="Aussichtsplattform" width="1000" height="1334" >}}

{{< image src="stadtteile/hafencity-20" alt="Baakenhafen" width="1000" height="1334" caption="Die Großbaustelle am Baakenhafen. Ab 2015/2016 entstehen hier eine große Anzahl neuer Wohnungen." >}}

{{< image src="stadtteile/hafencity-19" alt="Baakenhafen" width="1000" >}}

{{< image src="stadtteile/hafencity-18" alt="Baakenhafen" width="1000" >}}

{{< image src="stadtteile/hafencity-17" alt="Baakenhafen" width="1000" >}}

{{< image src="stadtteile/hafencity-16" alt="Baakenhafen" width="1000" >}}

Vorbei an der Universität entlang am Wasser und entlang an den *Elbakaden*, dann durch die *Hongkongstraße* und vorbei am Internationalen *Maritimen Museum Hamburg*. Hier fand ein riesiges Fest statt, dass von einem Bierhersteller gesponsert wurde und unzählige Menschen nutzen das gute Wetter in den zahlreichen Bierzelten.

{{< image src="stadtteile/hafencity-27" alt="Universität" width="1000" caption="Die neue Universität, Blick von der Aussichtsplattform." >}}

{{< image src="stadtteile/hafencity-28" alt="Universität" width="1000" >}}

{{< image src="stadtteile/hafencity-34" alt="Universität" width="1000" height="1334" >}}

{{< image src="stadtteile/hafencity-39" alt="Maritimes Museum" width="1000" >}}

{{< image src="stadtteile/hafencity-38" alt="Maritimes Museum" width="1000" >}}

{{< image src="stadtteile/hafencity-37" alt="Maritimes Museum" width="1000" >}}

{{< image src="stadtteile/hafencity-35" alt="Baustelle" width="1000" >}}

Ich bin weiter über die *Osakaallee*, durch den *Grasbrookpark*, einem Spielparadies für Kinder, der Straße *Großer Grassbrook* bis zum *Strandkai* gefolgt. Auch hier war es brechend voll. Am *Chicagokai* lag das Kreuzfahrtschiff *MS Amadea* (193 Meter) vor Anker und am Strandhafen, direkt neben dem Unilever-Gebäude fanden die *Extreme Sailing Series* statt. Ein gutes Dutzend Katamarane mit Teams aus aller Welt fuhren hier Rennen. Die Rennen wurden auf Englisch moderiert, von einem DJ begleitet, von Kameras gefilmt und die Rennkadern und Trainer saßen in Glasgebäuden hinter dem Kai. Nach ein paar Rennen bin ich weiter nach Norden gegangen, über die *Marco-Polo-Terrassen*.

{{< image src="stadtteile/hafencity-40" alt="Überseeboulevard" width="1000" height="1334" >}}

{{< image src="stadtteile/hafencity-44" alt="Grassbrookpark" width="1000" height="1334" >}}

{{< image src="stadtteile/hafencity-45" alt="Grassbrookpark" width="1000" >}}

{{< image src="stadtteile/hafencity-43" alt="Neue Gebäude" width="1000" >}}

{{< image src="stadtteile/hafencity-42" alt="Neue Gebäude" width="1000" height="1334" >}}

{{< image src="stadtteile/hafencity-41" alt="Neue Gebäude" width="1000" >}}

{{< image src="stadtteile/hafencity-46" alt="Unilever-Gebäude" width="1000" height="1334" >}}

{{< image src="stadtteile/hafencity-49" alt="Extreme Sailing Series" width="1000" caption="Die Extreme Sailing Series, ein Katamaran-Wettrennen mit Teams aus der ganzen Welt." >}}

{{< image src="stadtteile/hafencity-48" alt="Extreme Sailing Series" width="1000" >}}

{{< youtube va2uRtkndYg >}}

{{< image src="stadtteile/hafencity-47" alt="Kreuzfahrtschiff MS Amadea" width="1000" caption="Das Kreuzfahrtschiff MS Amadea." >}}

Auch hier war es unglaublich voll, die Schlangen vor den Restaurants und Eis-Läden waren sehr lang. Ich bin den *Dalmannkai* entlanggegangen und habe vom Anleger *Elbphilharmonie* aus noch ein paar Fotos und Videos von einem Katamaran-Rennen gemacht und bin anschließend an der Elbphilharmonie vorbeigegangen und auf *Am Kaiserkai* eingebogen. Hier gibt es entlang der Straße und auf Plätzen zwischen den Gebäuden zahlreiche interessante Geschäfte, Restaurants und Büros. Es gibt dort auch einen Seniorenruhesitz, der *Elbelysium* heißt. Ich bezweifle jedoch dass ein Platz dort zur Unsterblichkeit führt …

{{< image src="stadtteile/hafencity-55" alt="Anleger Elbphilharmonie" width="1000" >}}

{{< image src="stadtteile/hafencity-59" alt="Die Elbphilharmonie" width="1000" caption="Die Elbphilharmonie." >}}

{{< image src="stadtteile/hafencity-58" alt="Die Elbphilharmonie" width="1000" height="1334" >}}

{{< image src="stadtteile/hafencity-57" alt="Hafen" width="1000" >}}

{{< image src="stadtteile/hafencity-56" alt="Hafen" width="1000" >}}

{{< image src="stadtteile/hafencity-60" alt="Elbelysium" width="1000" height="1334" >}}

{{< image src="stadtteile/hafencity-54" alt="Unilever-Gebäude" width="1000" >}}

An den *Magellan-Terrassen* habe ich umgedreht und bin direkt am *Kaiserkai* entlanggegangen. Einige der alten Kräne wurden dort stehen gelassen. An der *Elbphilharmonie* bin ich nach Norden über eine Brücke zur Straße *Am Sandtorkai* gegangen und dieser eine Weile gefolgt. Ich habe einen Abstecher über den Schwimmponton im Hafenbecken gemacht, wo zahlreiche historische Schiffen den 2008 eröffneten *Traditionsschiffhafen* bilden.

{{< image src="stadtteile/hafencity-61" alt="Geschäfte" width="1000" height="1334" >}}

{{< image src="stadtteile/hafencity-70" alt="Der Traditionsschiffhafen" width="1000" caption="Der Traditionsschiffhafen." >}}

{{< image src="stadtteile/hafencity-69" alt="Der Traditionsschiffhafen." width="1000" >}}

{{< image src="stadtteile/hafencity-68" alt="HafenCity" width="1000" >}}

{{< image src="stadtteile/hafencity-67" alt="Columbus-Haus" width="1000" height="1334" >}}

{{< image src="stadtteile/hafencity-66" alt="Mosaik-Wand" width="1000" >}}

{{< image src="stadtteile/hafencity-65" alt="Der Traditionsschiffhafen" width="1000" >}}

{{< image src="stadtteile/hafencity-64" alt="Der Traditionsschiffhafen" width="1000" >}}

{{< image src="stadtteile/hafencity-63" alt="Der Traditionsschiffhafen" width="1000" height="1334" >}}

{{< image src="stadtteile/hafencity-62" alt="Der Traditionsschiffhafen" width="1000" >}}

Ich bin am *Sandtorpark* entlang gegangen und über das *Coffee Plaza*, wo eine riesige Kaffeebohne als Kunstwerk steht. Anschließend die *Tokiostraße* entlang und über den Überseeboulevard. Vom *Dar-es-Salaam-Platz* aus konnte ich erneut einen Blick auf das *Internationale Maritime Museum Hamburg* und das riesige Bierfest werfen.

{{< image src="stadtteile/hafencity-71" alt="Kesselhaus" width="1000" height="1334" caption="Das HafenCity InfoCenter im Kesselhaus." >}}

{{< image src="stadtteile/hafencity-73" alt="Überseeboulevard" width="1000" >}}

{{< image src="stadtteile/hafencity-72" alt="Coffee Plaza" width="1000" height="1334" caption="Coffee Plaza, eine riesige Skulptur einer Kaffeebohne." >}}

{{< image src="stadtteile/hafencity-74" alt="Überseeboulevard" width="1000" height="1334" >}}

{{< image src="stadtteile/hafencity-75" alt="Überseeboulevard" width="1000" height="1334" >}}

{{< image src="stadtteile/hafencity-80" alt="Internationale Maritime Museum Hamburg" width="1000" >}}

{{< image src="stadtteile/hafencity-79" alt="Alte Gebäude der HafenCity" width="1000" >}}

{{< image src="stadtteile/hafencity-78" alt="Neue Gebäude der HafenCity" width="1000" >}}

{{< image src="stadtteile/hafencity-77" alt="Altes Hafenamt" width="1000" >}}

{{< image src="stadtteile/hafencity-76" alt="Neue Gebäude der HafenCity" width="1000" height="1334" >}}

Zum Abschluss bin ich die Straße *Brooktorkai* eine Weile entlang gegangen und dann über die *Dienerreihe* und die *Holländisch-Brookfleet-Brücke* gegangen, wo die *HafenCity* endet. Auf der *Fleetinsel* steht das *Wasserschloß*, ein Restaurant mit Blick auf zwei Fleets. Auf dem Rückweg zur U-Bahn *Meßberg* bin ich noch am *Dialog im Dunkeln* vorbeigekommen, wo man in völlig abgedunkelten Räumen von blinden Menschen durch eine Ausstellung geführt wird, und einen Einblick in ein Leben ohne Sehsinn erleben kann. Ebenfalls gibt es dort auch *Dialog im Stillen*, eine Ausstellung über Gehörlosigkeit.

{{< image src="stadtteile/hafencity-81" alt="Restaurant Wasserschloss" width="1000" height="1334" caption="Das Restaurant Wasserschloss." >}}

{{< image src="stadtteile/hafencity-82" alt="Dialog im Dunkeln" width="1000" height="1334" caption="Dialog im Dunkeln." >}}

{{< image src="map/hafencity" alt="HafenCity" width="1000" height="956" >}}

<iframe class="map" src="https://www.google.com/maps/d/u/0/embed?mid=1FlDK0lPn5-xsfQLkF75OHzu3bTEDLX5T" width="1000" height="500">
</iframe>
