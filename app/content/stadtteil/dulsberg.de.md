---
title: Dulsberg
slug: dulsberg
author: Stefan Imhoff
date: 2015-06-25T18:00:00+02:00
distance: 6
duration: 1:14
---

Von der U-Bahn-Station *Strassburger Straße* aus bin ich losgegangen und durch Dulsberg-Süd gewandert.

Fast nur roter Backsteinbau aus den 20er Jahren (wieder aufgebaut, da durch Operation Gomorrha vollständig zerstört). Dulsberg ist für seine Größe außergewöhnlich gut angebunden. Es gibt zwei U-Bahn-Stationen und eine S-Bahn-Station.

{{< image src="stadtteile/dulsberg-01" alt="Bebauung in Dulsberg" width="1000" >}}

{{< image src="stadtteile/dulsberg-03" alt="Bebauung in Dulsberg" width="1000" >}}

{{< image src="stadtteile/dulsberg-04" alt="Bebauung in Dulsberg" width="1000" >}}

Hier leben viele Muslime und Afrikaner. Dulsberg ist einer der ärmsten Stadtteile Hamburgs und hatte bis 2003 die höchste Kriminalitätsrate. Das Wohngebiet ist stellenweise sehr laut, da es vom Friedrich-Ebert-Damm und Ring 2 durchschnitten wird. Und wenn man nach innen wohnt, hört man Lärm von Fußballplätzen.

In Nord-Dulsberg gibt es einen länglichen Park, der sehr schön ist und auch ruhiger, weil die Häuser rundherum den Lärm abfangen.

{{< image src="stadtteile/dulsberg-02" alt="Park in Dulsberg" width="1000" >}}

Ich bin an einem Treff für türkische Männer vorbeigegangen, wo ca. 20 Männer in einem Raum um einen Tisch saßen und sich unterhalten oder gespielt haben.

Es gibt die *Linne-Kampfbahn*, einen Sportplatz mit rotem Belag. Über den Alten Teichweg bin ich noch am *Sportpark Dulsberg* (*Olympiastützpunkt Hamburg*) mit dem *Beach-Center* vorbeigekommen.

{{< image src="stadtteile/dulsberg-08" alt="Olympiastützpunkt Hamburg in Dulsberg" width="1000" >}}

{{< image src="stadtteile/dulsberg-07" alt="Olympiastützpunkt Hamburg in Dulsberg" width="1000" >}}

{{< image src="stadtteile/dulsberg-06" alt="Olympiastützpunkt Hamburg in Dulsberg" width="1000" >}}

{{< image src="stadtteile/dulsberg-05" alt="Olympiastützpunkt Hamburg in Dulsberg" width="1000" >}}

{{< image src="map/dulsberg" alt="Dulsberg" width="1000" height="956" >}}

<iframe class="map" src="https://www.google.com/maps/d/u/0/embed?mid=1AnMqAxAuujp1SmoHMmTPpfvPCfQ" width="1000" height="500">
</iframe>
