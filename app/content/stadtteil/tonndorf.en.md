---
title: Tonndorf
slug: tonndorf
author: Stefan Imhoff
date: 2015-06-14T18:00:00+02:00
distance: 15
duration: 3:32
---

In Tonndorf there is the *Bio-Bäckerei Springer* (an organic backery), where it smells fantastic of fresh bread and baked goods. Opposite is alarge office complex where companies like *Stressless* or Otto have offices.

{{< image src="stadtteile/tonndorf-01" alt="office complex in Tonndorf" width="1000" >}}

South of *Friedrich-Ebert-Damm* there is a residental area, which is poorly maintained. Everything is overgrown with moss, even the street signs. The properties are often overgrown with green and look neglected. The sidewalks are in bad condition, often difficult to walk properly.

Even further south are mainly industry and commerce properties. Everything is pretty bleak, unkempt and ugly. There are countless properties with industry for car (auto repair, tire trade). And a commercial plot for rail needs.

{{< image src="stadtteile/tonndorf-02" alt="industry site" width="1000" caption="You can rent rails?" >}}

In the forst between *Rahlau* and *Wandse* it is quiet and beautiful. There is a *concentration camp memorial* for about 500 women of *KZ Drägerwerk*, who where imprisoned here. Serveral women were abused and one killed for allegedly sabotaging property (she had dropped a gas mask).

{{< image src="stadtteile/tonndorf-03" alt="KZ memorial Drägerwerk" width="1000" >}}

{{< image src="stadtteile/tonndorf-04" alt="KZ memorial Drägerwerk" width="1000" >}}

{{< image src="stadtteile/tonndorf-05" alt="KZ memorial Drägerwerk" width="1000" >}}

At the cementery Tonndorf there are very old graves, many for soldiers of the 2nd World War and some large graves for whole families.

{{< image src="stadtteile/tonndorf-06" alt="Cementary Tonndorf" width="1125" height="1500" >}}

South of the tracks are mostly industrial properties (costume rental, car dealers). There is a huge area, which looks like abandoned, and belongs to the Bundeswehr. It is a disposal warehouse and is well secured.

{{< image src="stadtteile/tonndorf-07" alt="railway crossing" width="1000" caption="Railway crossing. You have to wait very often and very long." >}}

{{< image src="stadtteile/tonndorf-08" alt="An old, weathered cementary" width="1000" caption="Between to industrial properties: An old, weathered cementary." >}}

{{< image src="stadtteile/tonndorf-09" alt="THW" width="1000" caption="Employees of the THW were obviously bored: They build a bench out of euro pallets." >}}

{{< image src="stadtteile/tonndorf-10" alt="Disposal warehouse of the Bundeswehr" width="1000" caption="Right in the center of Tonndorf: A disposal warehouse of the Bundeswehr. Which things might get disposed here? It’s well secured." >}}

Near the train station Hamburg-Tonndorf is the *Studio Hamburg* and a large shopping center. East of it is again residential area, predominate individual houses, very small, partly timeworn, in between but also well-kept houses and a few new buildings. North of the tracks it looks similar.

{{< image src="stadtteile/tonndorf-11" alt="Studio Hamburg" width="1000" caption="Studio Hamburg, they have a lot of antennas and satellite dishes on the roof." >}}

At the *Ostender Teich* is an outdoor pool, the gate was open and I could just go in there. There is a small beach and an area where you can swim.

{{< image src="stadtteile/tonndorf-12" alt="Ostender Teich" width="1000" >}}

{{< image src="stadtteile/tonndorf-13" alt="Ostender Teich" width="1000" >}}

{{< image src="stadtteile/tonndorf-14" alt="Ostender Teich" width="1000" >}}

North of the *Wandse* are 3- to 5-storey houses, some very new and extremely pretty. A brand new condominium is currently under construction right next to the forest.

{{< image src="stadtteile/tonndorf-15" alt="Residental area north of the Wandse" width="1000" >}}

{{< image src="stadtteile/tonndorf-16" alt="Residental area north of the Wandse" width="1000" >}}

Around the *Pohlmannteiche* are again small gardens, access to the lakes is only possible for owners (*Strandpark Ziegelsee*).

{{< image src="stadtteile/tonndorf-17" alt="Strandpark Ziegelsee: Wer in diesem Kleingartenverein ein Grundstück sein Eigen nennt, hat exklusiven Zugang zu einigen wunderschönen Seen." width="1125" height="1500" caption="Strandpark Ziegelsee: Wer in diesem Kleingartenverein ein Grundstück sein Eigen nennt, hat exklusiven Zugang zu einigen wunderschönen Seen." >}}

{{< image src="stadtteile/tonndorf-20" alt="Strandpark Ziegelsee" width="1000" >}}

{{< image src="stadtteile/tonndorf-19" alt="Strandpark Ziegelsee" width="1000" >}}

{{< image src="stadtteile/tonndorf-18" alt="Strandpark Ziegelsee" width="1125" height="1500" >}}

{{< image src="map/tonndorf" alt="Tonndorf" width="1000" height="956" >}}

<iframe class="map" src="https://www.google.com/maps/d/u/0/embed?mid=15seOWRQJj3cIfHjq74xqjrgD7Mg" width="1000" height="500">
</iframe>
