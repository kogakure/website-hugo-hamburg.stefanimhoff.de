---
title: Marienthal
slug: marienthal
author: Stefan Imhoff
date: 2015-06-21T18:00:00+02:00
distance: 9
duration: 1:53
---

From the subway *Wandsbeker Chaussee* I went south. *Hammer Str.* was closed because of a construction.

Marienthal is just a residential area, but a better one. There are many restored, beautiful buildings, clean and well maintained. Everything is well secured, alarm systems and warning signs against burglars everywhere. A lot of expensive cars in the driveways. The closer you get to the highway the less nice the houses are.

{{< image src="stadtteile/marienthal-01" alt="Houses in Marienthal" width="1000" >}}

{{< image src="stadtteile/marienthal-07" alt="Houses in Marienthal" width="1000" >}}

{{< image src="stadtteile/marienthal-06" alt="Houses in Marienthal" width="1000" >}}

{{< image src="stadtteile/marienthal-05" alt="Houses in Marienthal" width="1000" height="938" >}}

{{< image src="stadtteile/marienthal-02" alt="Houses in Marienthal" width="1000" >}}

{{< image src="stadtteile/marienthal-04" alt="Houses in Marienthal" width="1000" >}}

{{< image src="stadtteile/marienthal-03" alt="Houses in Marienthal" width="1000" >}}

There are two geometric parks with lakes, in one of which stands the *Husarendenkmal*.

{{< image src="stadtteile/marienthal-08" alt="Husarendenkmal" width="1125" height="1500" >}}

{{< image src="stadtteile/marienthal-09" alt="Park" width="1000" >}}

{{< image src="stadtteile/marienthal-10" alt="Park" width="1000" >}}

At the highway a huge sound barrier was built, which is very ugly, and loud it is there anyway. Nearby, the Siel was redone, the tubes are huge.

{{< image src="stadtteile/marienthal-11" alt="Sound barrier" width="1000" >}}

{{< image src="stadtteile/marienthal-12" alt="Siel work" width="1000" >}}

In the *Wandsbeker Gehölz* it is nice and quiet, there are several lakes and many people were around on a Sunday. Along the forest there are many retirement homes.

Near Wandsbek station there is a bizarre traffic control: an underpass for pedestrians, above the tracks of the Deutsche Bahn and above a freeway.

{{< image src="map/marienthal" alt="Marienthal" width="1000" height="956" >}}

<iframe class="map" src="https://www.google.com/maps/d/u/0/embed?mid=1-lfaf5JwRwenBQ5K5bkhn2OPlmQ" width="1000" height="500">
</iframe>
