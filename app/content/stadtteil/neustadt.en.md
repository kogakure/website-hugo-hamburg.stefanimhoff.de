---
title: Neustadt
slug: neustadt
author: Stefan Imhoff
date: 2016-09-10T18:00:00+02:00
distance: 16
duration: 4:57
---

The *Neustadt* Hamburg was particularly interesting for me, because I knew the district by profession already well and was curious if I would discover things that I do not know.

From the subway station *Baumwall* I started directly at the harbor, where I went along the *Elbpromenade* and made a detour over the *Überseebrücke* to photograph the ships lying there.

{{< image src="stadtteile/neustadt-01" alt="Neustadt" width="1000" height="853" >}}

{{< image src="stadtteile/neustadt-02" alt="Neustadt" width="1000" height="853" >}}

{{< image src="stadtteile/neustadt-03" alt="Neustadt" width="1000" height="854" >}}

{{< image src="stadtteile/neustadt-04" alt="Neustadt" width="1000" height="853" >}}

{{< image src="stadtteile/neustadt-05" alt="Neustadt" width="1000" height="853" >}}

{{< image src="stadtteile/neustadt-06" alt="Neustadt" width="1000" height="853" >}}

{{< image src="stadtteile/neustadt-07" alt="Neustadt" width="1000" height="853" >}}

Then I went through the southern Neustadt, the so-called *Portuguese quarter*, where Portuguese and Spanish restaurants string together. There are many beautiful buildings in Hanseatic style.

Right next to the office complex of G+J is the *Michelwiese*, a park that stretches from the Michel to the harbor.

{{< image src="stadtteile/neustadt-08" alt="Neustadt" width="1000" height="1501" >}}

Along the Venusberg I walked next to *Alster Elbpark* with Bismarck monument in direction of *St. Michaelis* (Michel). Since I’ve been on the top of the Michel many times, I've been saving myself this time, looking instead at the statues and artworks around the church.

Near the Michels stands the *monument of the Zitronenjette*, whose fingers are scrubbed completely blank, since supposedly bring luck to rub on her finger.

{{< image src="stadtteile/neustadt-09" alt="Neustadt" width="1000" height="853" >}}

{{< image src="stadtteile/neustadt-10" alt="Neustadt" width="1000" height="853" >}}

{{< image src="stadtteile/neustadt-11" alt="Neustadt" width="1000" height="1501" >}}

{{< image src="stadtteile/neustadt-12" alt="Neustadt" width="1000" height="853" >}}

{{< image src="stadtteile/neustadt-13" alt="Neustadt" width="1000" height="1501" >}}

{{< image src="stadtteile/neustadt-14" alt="Neustadt" width="1000" height="1501" >}}

I then passed the small Michel and direction *Grossneumarkt*. After a few detours, follow the northern Neustadt with its expensive shopping streets. Here are all expensive fashion and luxury goods brands settled and the sports car density is extremely high. I went directly along the *Alsterfleet* and the *Alsterarkaden*, through *Neuer Wall* and *Große Bleichen*. I have circled the *Hanse-Viertel*, a large shopping arcade in Neustadt.

{{< image src="stadtteile/neustadt-15" alt="Neustadt" width="1000" height="853" >}}

{{< image src="stadtteile/neustadt-16" alt="Neustadt" width="1000" height="853" >}}

{{< image src="stadtteile/neustadt-17" alt="Neustadt" width="1000" height="1501" >}}

{{< image src="stadtteile/neustadt-18" alt="Neustadt" width="1000" height="853" >}}

{{< image src="stadtteile/neustadt-19" alt="Neustadt" width="1000" height="853" >}}

{{< image src="stadtteile/neustadt-20" alt="Neustadt" width="1000" height="1501" >}}

{{< image src="stadtteile/neustadt-21" alt="Neustadt" width="1000" height="1501" >}}

{{< image src="stadtteile/neustadt-22" alt="Neustadt" width="1000" height="1501" >}}

{{< image src="stadtteile/neustadt-23" alt="Neustadt" width="1000" height="853" >}}

{{< image src="stadtteile/neustadt-24" alt="Neustadt" width="1000" height="853" >}}

{{< image src="stadtteile/neustadt-25" alt="Neustadt" width="1000" height="853" >}}

{{< image src="stadtteile/neustadt-26" alt="Neustadt" width="1000" height="1501" >}}

{{< image src="stadtteile/neustadt-27" alt="Neustadt" width="1000" height="853" >}}

{{< image src="stadtteile/neustadt-28" alt="Neustadt" width="1000" height="1501" >}}

{{< image src="stadtteile/neustadt-29" alt="Neustadt" width="1000" height="1501" >}}

{{< image src="stadtteile/neustadt-30" alt="Neustadt" width="1000" height="1501" >}}

{{< image src="stadtteile/neustadt-31" alt="Neustadt" width="1000" height="1501" >}}

{{< image src="stadtteile/neustadt-32" alt="Neustadt" width="1000" height="1501" >}}

{{< image src="stadtteile/neustadt-33" alt="Neustadt" width="1000" height="853" >}}

{{< image src="stadtteile/neustadt-34" alt="Neustadt" width="1000" height="853" >}}

{{< image src="stadtteile/neustadt-35" alt="Neustadt" width="1000" height="1501" >}}

{{< image src="stadtteile/neustadt-36" alt="Neustadt" width="1000" height="1501" >}}

{{< image src="stadtteile/neustadt-37" alt="Neustadt" width="1000" height="853" >}}

{{< image src="stadtteile/neustadt-38" alt="Neustadt" width="1000" height="853" >}}

{{< image src="stadtteile/neustadt-39" alt="Neustadt" width="1000" height="1501" >}}

{{< image src="stadtteile/neustadt-40" alt="Neustadt" width="1000" height="853" >}}

{{< image src="stadtteile/neustadt-41" alt="Neustadt" width="1000" height="1501" >}}

Past Police Commissariat Hamburg-Mitte and the Axel-Springer-Passage. Also Internet companies like Google (ABC Street) and Facebook (Caffamacherreihe) have offices here.

{{< image src="stadtteile/neustadt-42" alt="Neustadt" width="1000" height="1501" >}}

Then I walked past the *Laeiszhalle* past the Dammtorwall, over the *Gänsemarkt* and through the *Collonades*. At the end of my tour again around the *Casino Espanade* around and then once around the *Binnenalster*. My tour ended at Jungfernstieg.

{{< image src="stadtteile/neustadt-43" alt="Neustadt" width="1000" height="853" >}}

{{< image src="stadtteile/neustadt-44" alt="Neustadt" width="1000" height="1501" >}}

{{< image src="stadtteile/neustadt-45" alt="Neustadt" width="1000" height="853" >}}

{{< image src="stadtteile/neustadt-46" alt="Neustadt" width="1000" height="853" >}}

{{< image src="map/neustadt" alt="Map Neustadt" width="1000" >}}

<iframe class="map" src="https://www.google.com/maps/d/u/0/embed?mid=1SFmPYmjakPn5W_7rWfEl1qM7YlY" width="1000" height="500"></iframe>
