---
title: Farmsen-Berne
slug: farmsen-berne
author: Stefan Imhoff
date: 2015-06-07T18:00:00+02:00
distance: 16
duration: 3:18
---

Farmsen-Berne hat sehr viele *Kleingartenvereine*, nördlich vom *Luisenhof* gibt es überwiegend 3-stöckige Siedlungen, nur mäßig hübsch, aber überwiegend ruhig gelegen.

{{< image src="stadtteile/farmsen-berne-02" alt="Kleingartenvereine" width="1000" >}}

{{< image src="stadtteile/farmsen-berne-01" alt="Kleingartenvereine" width="1000" >}}

Östlich des *Strandbad Farmsen* eher Einfamilienhäuser, gepflegt und spießig. Selbst die Einfahrten ohne Zaun werden symbolisch mit Ketten abgesperrt.

Nahe des *Rückhaltebeckens* an der *Berner Au* gibt es viel Wald (*Berner Wald*), schöne Siedlungen, aber auch einige Plattenbauten. Die Siedlung nördlich des Berner Waldes ist sehr schön und hat gepflegte Häuser.

{{< image src="stadtteile/farmsen-berne-03" alt="Rückhaltebecken" width="1000" >}}

{{< image src="stadtteile/farmsen-berne-04" alt="Rückhaltebecken" width="1000" >}}

{{< image src="stadtteile/farmsen-berne-05" alt="Viele Spielplätze, Wald und Grün." width="1000" caption="Viele Spielplätze, Wald und Grün." >}}

Der *Berner Gutspark* ist äußerst gepflegt und der schönste Park im ganzen Stadtteil. Er eignet sich zum Entspannen und Lesen auf der Wiese bei schönem Wetter. Er ist in wenigen Minuten von der U-Bahn-Station *Berne* aus zu erreichen. Mitten im Park steht das *Berner Gutshaus*.

{{< image src="stadtteile/farmsen-berne-09" alt="Der Berner Gutspark mit dem Berner Gutshaus" width="1000" >}}

{{< image src="stadtteile/farmsen-berne-08" alt="Berner Gutspark" width="1000" >}}

Die Strecke entlang der U-Bahn-Strecke hat überwiegend 3- oder 4-stöckige Siedlungen.

Es gibt viel Grün und Ruhe, gute Joggingstrecken entlang der *Berner Au*. Viele Spielplätze, Sportplätze, Abenteuerspielplätze und Bauspielplätze.

{{< image src="stadtteile/farmsen-berne-06" alt="Abenteuerspielplätze und Bauspielpätze im Wald" width="1000" >}}

{{< image src="stadtteile/farmsen-berne-07" alt="Abenteuerspielplätze und Bauspielpätze im Wald" width="1000" >}}

{{< image src="map/farmsen-berne" alt="Farmsen-Berne" width="1000" >}}

<iframe class="map" src="https://www.google.com/maps/d/u/0/embed?mid=1lrph_CzuZjhnBOCx_v7VKgAVN8I" width="1000" height="500">
</iframe>
