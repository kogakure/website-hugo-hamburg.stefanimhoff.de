---
title: Barmbek-Nord
slug: barmbek-nord
author: Stefan Imhoff
date: 2015-07-18T18:00:00+02:00
distance: 17
duration: 3:36
---

From *Barmbek Station*, where a *round bunker* from World War II is, I went behind *Globetrotter* and the *Museum of Labor* over to the *Osterbekkanal*.

{{< image src="stadtteile/barmbek-nord-01" alt="Round bunker from World War II" width="1000" height="1334" >}}

{{< image src="stadtteile/barmbek-nord-04" alt="Globetrotter" width="1000" >}}

{{< image src="stadtteile/barmbek-nord-03" alt="Museum of Labor" width="1000" >}}

{{< image src="stadtteile/barmbek-nord-02" alt="brick building" width="1000" height="1334" >}}

On the *courtyard of the museum* stands the *T.R.U.D.E.*, the boring head of the Elbtunnel drill, which drilled the 4th Elbtunnel tube. It has an impressive 14.2 meters in diameter, weighs 380 tons and was in use from October 1997 to March 2000.

{{< image src="stadtteile/barmbek-nord-11" alt="Tunnel boring machine T.R.U.D.E." width="1000" height="1334" >}}

{{< image src="stadtteile/barmbek-nord-10" alt="Tunnel boring machine T.R.U.D.E." width="1000" >}}

{{< image src="stadtteile/barmbek-nord-09" alt="Tunnel boring machine T.R.U.D.E." width="1000" >}}

{{< image src="stadtteile/barmbek-nord-08" alt="Tunnel boring machine T.R.U.D.E." width="1000" >}}

I continued to walk along the canal and then looked at the residential areas south of the tracks. As everywhere in Barmbek-Nord, everything is built in red brick, in parts really pretty and in a variety of reds and patterns, so that no uniformity arises.

{{< image src="stadtteile/barmbek-nord-21" alt="Apartments in Barmbek-Nord" width="1000" height="1334" >}}

{{< image src="stadtteile/barmbek-nord-20" alt="Apartments in Barmbek-Nord" width="1000" height="1334" >}}

{{< image src="stadtteile/barmbek-nord-19" alt="Apartments in Barmbek-Nord" width="1000" >}}

{{< image src="stadtteile/barmbek-nord-18" alt="Apartments in Barmbek-Nord" width="1000" height="1334" >}}

{{< image src="stadtteile/barmbek-nord-17" alt="Wohnungen in Barmbek-Nord" width="1000" height="1334" >}}

{{< image src="stadtteile/barmbek-nord-16" alt="Apartments in Barmbek-Nord" width="1000" >}}

{{< image src="stadtteile/barmbek-nord-15" alt="Apartments in Barmbek-Nord" width="1000" >}}

{{< image src="stadtteile/barmbek-nord-14" alt="Apartments in Barmbek-Nord" width="1000" height="1334" >}}

{{< image src="stadtteile/barmbek-nord-13" alt="Apartments in Barmbek-Nord" width="1000" >}}

{{< image src="stadtteile/barmbek-nord-12" alt="Apartments in Barmbek-Nord" width="1000" >}}

Then I went under the tracks and through the *Drosselgasse*, where heavy constructions are in progress right in front of the station. The entire area is to be rebuilt and should upgrade Barmbek-North. In addition to a huge shopping center and commercial buildings there are also condominiums.

{{< image src="stadtteile/barmbek-nord-22" alt="Construction site" width="1000" >}}

I went south, passed under the tracks, and moved along by *CrossFit HH*, where a group of muscle-bound men came back from a running race and turned on the studios property. I guess that was the warm-up.

{{< image src="stadtteile/barmbek-nord-23" alt="Graffiti" width="1000" >}}

Then I went through the residential areas east of the *Bramfelder Straße* and through a small garden club behind the former *Margarine Factory Voss*, where now *Techniker Krankenkasse* sits.

{{< image src="stadtteile/barmbek-nord-24" alt="Former margarine factory Voss" width="1000" >}}

{{< image src="stadtteile/barmbek-nord-27" alt="Former margarine factory Voss" width="1000" >}}

{{< image src="stadtteile/barmbek-nord-26" alt="Former margarine factory Voss" width="1000" >}}

{{< image src="stadtteile/barmbek-nord-25" alt="Former margarine factory Voss" width="1000" >}}

Down the *Bramfelder Straße*, north to *Hellbrookstraße* and back south to the Drosselstraße. I continued through the residential areas and under the tracks in the north, past *Jim Block Barmbek*. There are a lot of shops on *Fuhlsbüttler Straße*.

{{< image src="stadtteile/barmbek-nord-28" alt="Underground station Habichtstraße" width="1000" >}}

{{< image src="stadtteile/barmbek-nord-33" alt="Commercial building" width="1000" >}}

{{< image src="stadtteile/barmbek-nord-32" alt="Barmbeker-Ring-Brücke" width="1000" >}}

{{< image src="stadtteile/barmbek-nord-31" alt="Crossing" width="1000" >}}

{{< image src="stadtteile/barmbek-nord-30" alt="School with pretty doors" width="1000" >}}

{{< image src="stadtteile/barmbek-nord-29" alt="School with pretty doors" width="1000" >}}

Continue east, then north to a large park. Every now and then to the south and north and past the *Bürgerhaus Barmbek*, where the columns and railings of a building are completely covered with knitted patterns.

{{< image src="stadtteile/barmbek-nord-34" alt="Bürgerhaus Barmbek" width="1000" >}}

{{< image src="stadtteile/barmbek-nord-35" alt="Columns and railings decorated with knitted patterns." width="1000" >}}

At the *Barmbeker-Ring-Brücke*, where several streets intersect, I walked west, then passed the district school Barmbek to the north.

South of the *Asklepios Kliniken* and west of the *Wasserturm Palais* is a really nice residential area, extremely quiet with classical, listed buildings. But if you want to live there, you have to dig deep into your pocket (5 rooms, 2 bathrooms, 160m² for about 2,500 € per month).

{{< image src="stadtteile/barmbek-nord-36" alt="Wasserturm Palais" width="1000" height="1334" >}}

{{< image src="stadtteile/barmbek-nord-37" alt="Wasserturm Palais" width="1000" >}}

{{< image src="stadtteile/barmbek-nord-39" alt="Wasserturm Palais" width="1000" >}}

{{< image src="stadtteile/barmbek-nord-38" alt="Wasserturm Palais" width="1000" height="1334" >}}

{{< image src="stadtteile/barmbek-nord-45" alt="residential area" width="1000" height="1334" >}}

{{< image src="stadtteile/barmbek-nord-44" alt="residential area" width="1000" >}}

{{< image src="stadtteile/barmbek-nord-46" alt="residential area" width="1000" >}}

{{< image src="stadtteile/barmbek-nord-42" alt="residential area" width="1000" height="1334" >}}

West of the clinic is the *S-Bahn station Rübenkamp*, where I finished my tour.

{{< image src="stadtteile/barmbek-nord-47" alt="Asklepios Kliniken" width="1000" >}}

{{< image src="stadtteile/barmbek-nord-48" alt="S-Bahn station Rübenkamp" width="1000" >}}

{{< image src="map/barmbek-nord" alt="Barmbek-Nord" width="1000" height="956" >}}

<iframe class="map" src="https://www.google.com/maps/d/u/0/embed?mid=1yyBpChXiQJCHMY7Q8DfQbNAat5I" width="1000" height="500">
</iframe>
