---
title: Rotherbaum
slug: rotherbaum
author: Stefan Imhoff
date: 2016-10-22T18:00:00+02:00
distance: 16
duration: 4:15
---

Vom *Dammtorbahnhof* aus bin ich entlang der Gleise die *Alsterglacis* hinunter zur Alster gegangen und dann am Ufer der Außenalster entlang. Hier gibt es ein italienisches Restaurant und zwei Ruder-Clubs, die zum Großteil auf dem Wasser gebaut sind. Vorbei am US Konsulat, dass auch das *Kleine Weiße Haus an der Alster* genannt wird. Rund um das Konsulat sind die Straßen weiträumig nicht befahrbar und schwere Stahlsäulen und hohe Stahlzäune verhindern, dass ungebetene Gäste per Auto aufs Gelände gelangen.. Außerdem wird es ständig von mehreren deutschen Polizisten bewacht.

{{< image src="stadtteile/rotherbaum-01" alt="Rotherbaum" width="1000" height="667" >}}

{{< image src="stadtteile/rotherbaum-02" alt="Rotherbaum" width="1000" height="1500" >}}

{{< image src="stadtteile/rotherbaum-03" alt="Rotherbaum" width="1000" height="667" >}}

{{< image src="stadtteile/rotherbaum-04" alt="Rotherbaum" width="1000" height="1500" >}}

{{< image src="stadtteile/rotherbaum-05" alt="Rotherbaum" width="1000" height="667" >}}

Am Mittelweg bin ich am *Max-Planck-Institut für ausländisches und internationales Privatrecht* vorbeigekommen und anschließend am *Gartenhaus Fontenay*, in dem heute das Konsulat von Ägypten seinen Sitz hat.

{{< image src="stadtteile/rotherbaum-06" alt="Max-Planck-Institut für ausländisches und internationales Privatrecht" width="1000" height="1500" >}}

{{< image src="stadtteile/rotherbaum-07" alt="Gartenhaus Fontenay" caption="Gartenhaus Fontenay" width="1000" height="667" >}}

Durch die Fontenay, vorbei am noch im Bau befindlichen *The Fontenay Hotel*, das ähnlich wie die *Hamburger Welle* eine gewellte Form hat, bin ich wieder Richtung Alster gegangen und weiter am Ufer entlang nach Norden.

{{< image src="stadtteile/rotherbaum-08" alt="The Fontenay Hotel" caption="The Fontenay Hotel" width="1000" height="667" >}}

{{< image src="stadtteile/rotherbaum-09" alt="Rotherbaum" width="1000" height="667" >}}

{{< image src="stadtteile/rotherbaum-10" alt="Rotherbaum" width="1000" height="1500" >}}

{{< image src="stadtteile/rotherbaum-11" alt="Rotherbaum" width="1000" height="1500" >}}

{{< image src="stadtteile/rotherbaum-12" alt="Rotherbaum" width="1000" height="667" >}}

{{< image src="stadtteile/rotherbaum-13" alt="Rotherbaum" width="1000" height="1500" >}}

{{< image src="stadtteile/rotherbaum-14" alt="Rotherbaum" width="1000" height="667" >}}

{{< image src="stadtteile/rotherbaum-15" alt="Rotherbaum" width="1000" height="667" >}}

{{< image src="stadtteile/rotherbaum-16" alt="Rotherbaum" width="1000" height="1500" >}}

Viele Häuser in Rotherbaum sind äußerst schön und gut gepflegt. In der Milchstraße steht die *Villa Beit*, die fast wie ein kleines Schloß aussieht. Es gibt zahlreiche Konsulate in diesem Stadtteil.

Ich bin weiter nach Süden gegangen zum Park Moorweide und um das Hotel Grand Elysée Hamburg herum.

{{< image src="stadtteile/rotherbaum-17" alt="Villa Beit" caption="Villa Beit" width="1000" height="667" >}}

{{< image src="stadtteile/rotherbaum-18" alt="Villa Beit" width="1000" height="1500" >}}

{{< image src="stadtteile/rotherbaum-19" alt="Rotherbaum" width="1000" height="1500" >}}

In der Feldbrunnenstraße steht ein außergewöhnliches Gebäude: Das Chinesische Teehaus *Yu Garden* mit den typischen gebogenen Dachgiebeln, steinernen Löwenstatuen und runden Toren. Von da aus in einem Bogen wieder nach Süden, vorbei am *Museum für Völkerkunde Hamburg*.

{{< image src="stadtteile/rotherbaum-20" alt="Chinesische Teehaus Yu Garden" width="1000" height="667" >}}

{{< image src="stadtteile/rotherbaum-21" alt="Chinesische Teehaus Yu Garden" width="1000" height="667" >}}

{{< image src="stadtteile/rotherbaum-22" alt="Chinesische Teehaus Yu Garden" width="1000" height="1500" >}}

{{< image src="stadtteile/rotherbaum-24" alt="Kirche St. Johannis" caption="Kirche St. Johannis" width="1000" height="1500" >}}

{{< image src="stadtteile/rotherbaum-23" alt="Museum für Völkerkunde Hamburg" caption="Museum für Völkerkunde Hamburg" width="1000" height="1500" >}}

{{< image src="stadtteile/rotherbaum-25" alt="Museum für Völkerkunde Hamburg" caption="Museum für Völkerkunde Hamburg" width="1000" height="1500" >}}

{{< image src="stadtteile/rotherbaum-26" alt="Rotherbaum" width="1000" height="667" >}}

Anschließend bin ich am Hauptgebäude der *Universität Hamburg* vorbeigegangen und quer durch *Grindelviertel* und über das Universitätsgelände. Vorbei an *Staats- und Universitätsbibliothek Hamburg Carl von Ossietzky*, durch den *Von-Melle-Park* und am *Mensa Philosophenturm* vorbei.

{{< image src="stadtteile/rotherbaum-27" alt="Hauptgebäude der Universität Hamburg" width="1000" height="667" >}}

{{< image src="stadtteile/rotherbaum-28" alt="Hauptgebäude der Universität Hamburg" width="1000" height="667" >}}

{{< image src="stadtteile/rotherbaum-29" alt="Rotherbaum" width="1000" height="667" >}}

{{< image src="stadtteile/rotherbaum-30" alt="Rotherbaum" width="1000" height="667" >}}

{{< image src="stadtteile/rotherbaum-31" alt="Rotherbaum" width="1000" height="666" >}}

{{< image src="stadtteile/rotherbaum-32" alt="Rotherbaum" width="1000" height="667" >}}

{{< image src="stadtteile/rotherbaum-33" alt="Rotherbaum" width="1000" height="1500" >}}

{{< image src="stadtteile/rotherbaum-34" alt="Rotherbaum" width="1000" height="667" >}}

Wieder nach Süden am *Abaton Kino* vorbei und dann durch Grindelallee, am Polizeikommissariat 17 vorbei, am *Zoologischen Museum* und am *Max-Planck-Institut für Meteorologie* vorbei endete meine Tour an der U-Bahn-Station Schlump.

{{< image src="stadtteile/rotherbaum-35" alt="Abaton Kino" caption="Abaton Kino" width="1000" height="1500" >}}

{{< image src="map/rotherbaum" alt="Karte Rotherbaum" width="1000" >}}

<iframe class="map" src="https://www.google.com/maps/d/u/0/embed?mid=11jjb8Lljf57P78rxMt0Er3Mn944" width="1000" height="500"></iframe>
