---
title: HafenCity
slug: hafencity
author: Stefan Imhoff
date: 2015-07-26T18:00:00+02:00
distance: 12
duration: 4:04
---

From the metro station *Meßberg* I went over a pedestrian bridge to the south in the *HafenCity*. Strictly speaking, however, the district begins only after *Brooktorkai* / *Ericus*. Once I passed the *Spiegel-building* over the *Ericusbrücke*, past *Lohseplatz*, *Koreastraße* and then *Shanghaiallee* to the south. East of the road is still heavily built, several commercial and residential buildings are still under construction and not every street is walkable.

{{< image src="stadtteile/hafencity-01" alt="Deichtorhallen" width="1000" caption="This is not the HafenCity, but the view of the Deichtorhallen is still nice." >}}

{{< image src="stadtteile/hafencity-02" alt="Fleetinsel" width="1000" >}}

{{< image src="stadtteile/hafencity-03" alt="The glass building of the Spiegel publishing building" width="1000" caption="The glass building of the Spiegel publishing building" >}}

{{< image src="stadtteile/hafencity-06" alt="The glass building of the Spiegel publishing building" width="1000" >}}

{{< image src="stadtteile/hafencity-05" alt="The glass building of the Spiegel publishing building" width="1000" height="1334" >}}

{{< image src="stadtteile/hafencity-04" alt="The glass building of the Spiegel publishing building" width="1000" height="1334" >}}

{{< image src="stadtteile/hafencity-07" alt="New buildings in HafenCity" width="1000" height="1334" >}}

{{< image src="stadtteile/hafencity-12" alt="New buildings in HafenCity" width="1000" >}}

{{< image src="stadtteile/hafencity-08" alt="New buildings in HafenCity" width="1000" >}}

{{< image src="stadtteile/hafencity-11" alt="New buildings in HafenCity" width="1000" >}}

{{< image src="stadtteile/hafencity-09" alt="Construction sites in the HafenCity" width="1000" >}}

{{< image src="stadtteile/hafencity-10" alt="Automuseum Prototyp" width="1000" height="1334" >}}

I walked through the *Lohsepark*, where there are numerous equipment such as trampolines, seating and play opportunities for children. At the metro station *HafenCity University* I followed the road to the end. The *Versmannstraße* is not yet built, getting ahead is impossible. Directly at *Baakenhafen* arise from 2015/2016 new apartments.

{{< image src="stadtteile/hafencity-13" alt="Lohsepark" width="1000" caption="The Lohsepark: numerous trampolines, seating and artworks." >}}

{{< image src="stadtteile/hafencity-15" alt="Lohsepark" width="1000" >}}

{{< image src="stadtteile/hafencity-14" alt="Lohsepark" width="1000" >}}

In the metro station *HafenCity University* I watched the lights and music show, which is played once an hour (always on the hour) between 10 and 18 o’clock. While the light installations on the platform usually change color slowly, a true color blast is fired during the multi-minute classic music show.

{{< image src="stadtteile/hafencity-30" alt="Subway station HafenCity University" width="1000" caption="Subway station HafenCity University. Every hour on the hour (from 10 am to 6 pm) there is a light show on classical music." >}}

{{< image src="stadtteile/hafencity-33" alt="Subway station HafenCity University" width="1000" >}}

{{< image src="stadtteile/hafencity-32" alt="Subway station HafenCity University" width="1000" >}}

{{< image src="stadtteile/hafencity-31" alt="Subway station HafenCity University" width="1000" height="1334" >}}

Via a bridge I reached the southern part of the *Baakenhafen* and followed the *Baakenalle* until it suddenly ended in nothingness. Then I turned around and passed *Zieselpark*. Here you can drive (as long as the apartments are not yet available) on cross-segways or tracked vehicles on an obstacle course through the mud.

{{< image src="stadtteile/hafencity-21" alt="Ziesel-Park" width="1000" caption="Ziesel-Park. Eine Strecke für Kettenfahrzeuge und Cross-Segways. Schlammig und dreckig." >}}

{{< image src="stadtteile/hafencity-22" alt="Ziesel-Park" width="1000" >}}

{{< youtube lrkBWnjbDQE >}}

{{< image src="stadtteile/hafencity-23" alt="Harbor" width="1000" >}}

{{< image src="stadtteile/hafencity-26" alt="Harbor" width="1000" >}}

{{< image src="stadtteile/hafencity-25" alt="Harbor" width="1000" >}}

{{< image src="stadtteile/hafencity-24" alt="Elbbrücken" width="1000" >}}

I followed the bypass *Versmannstraße* to the *Elbbrücken* and then turned around. Also there will be built, the *Quatier Elbbrücken* will fill the whole *Fleetinsel* once. In the park *Baakenhöft* there is a viewing platform, from which one can observe the construction in the *Baakenhafen*.

{{< image src="stadtteile/hafencity-29" alt="Viewing platform" width="1000" height="1334" >}}

{{< image src="stadtteile/hafencity-20" alt="Baakenhafen" width="1000" height="1334" caption="The big construction site at Baakenhafen. From 2015/2016, a large number of new apartments will be built here." >}}

{{< image src="stadtteile/hafencity-19" alt="Baakenhafen" width="1000" >}}

{{< image src="stadtteile/hafencity-18" alt="Baakenhafen" width="1000" >}}

{{< image src="stadtteile/hafencity-17" alt="Baakenhafen" width="1000" >}}

{{< image src="stadtteile/hafencity-16" alt="Baakenhafen" width="1000" >}}

Past the university along the water and along the *Elbakaden*, then through *Hongkongstraße* and past the International *Maritime Museum Hamburg*. Here was a huge party sponsored by a beer producer and countless people used the good weather in the many beer tents.

{{< image src="stadtteile/hafencity-27" alt="Univerity" width="1000" caption="The new university, view from the observation deck." >}}

{{< image src="stadtteile/hafencity-28" alt="University" width="1000" >}}

{{< image src="stadtteile/hafencity-34" alt="University" width="1000" height="1334" >}}

{{< image src="stadtteile/hafencity-39" alt="Maritimes Museum" width="1000" >}}

{{< image src="stadtteile/hafencity-38" alt="Maritimes Museum" width="1000" >}}

{{< image src="stadtteile/hafencity-37" alt="Maritimes Museum" width="1000" >}}

{{< image src="stadtteile/hafencity-35" alt="Construction site" width="1000" >}}

I continued on the *Osakaallee*, followed by *Grasbrookpark*, a play paradise for children, the road *Großer Grassbrook* to *Strandkai*. Again, it was packed. At *Chicagokai* the cruise ship *MS Amadea* (193 meters) was anchored and at the beach harbor, right next to the Unilever building, the *Extreme Sailing Series* took place. A good dozen catamarans with teams from all over the world were racing here. The races were moderated in English, accompanied by a DJ, filmed by cameras and the racing squads and coaches sat in glass buildings behind the wharf. After a few races I went further north, over the *Marco Polo Terraces*.

{{< image src="stadtteile/hafencity-40" alt="Überseeboulevard" width="1000" height="1334" >}}

{{< image src="stadtteile/hafencity-44" alt="Grassbrookpark" width="1000" height="1334" >}}

{{< image src="stadtteile/hafencity-45" alt="Grassbrookpark" width="1000" >}}

{{< image src="stadtteile/hafencity-43" alt="New buildings" width="1000" >}}

{{< image src="stadtteile/hafencity-42" alt="New buildings" width="1000" height="1334" >}}

{{< image src="stadtteile/hafencity-41" alt="New buildings" width="1000" >}}

{{< image src="stadtteile/hafencity-46" alt="Unilever buildig" width="1000" height="1334" >}}

{{< image src="stadtteile/hafencity-49" alt="Extreme Sailing Series" width="1000" caption="The Extreme Sailing Series, a catamaran race with teams from around the world." >}}

{{< image src="stadtteile/hafencity-48" alt="Extreme Sailing Series" width="1000" >}}

{{< youtube va2uRtkndYg >}}

{{< image src="stadtteile/hafencity-47" alt="Cruise ship MS Amadea" width="1000" caption="Cruise ship MS Amadea" >}}

Again, it was incredibly crowded, the lines in front of the restaurants and ice cream shops were very long. I walked along the *Dalmannkai* and made a few more photos and videos of a catamaran race from the feeder *Elbphilharmonie* and then walked past the Elbphilharmonie and on *Am Kaiserkai*. There are many interesting shops, restaurants and offices along the street and in the squares between the buildings. There is also a senior retirement home called *Elbelysium*. However, I doubt that a place there leads to immortality …

{{< image src="stadtteile/hafencity-55" alt="Pier Elbphilharmonie" width="1000" >}}

{{< image src="stadtteile/hafencity-59" alt="The Elbphilharmonie" width="1000" caption="Die Elbphilharmonie." >}}

{{< image src="stadtteile/hafencity-58" alt="The Elbphilharmonie" width="1000" height="1334" >}}

{{< image src="stadtteile/hafencity-57" alt="Harbor" width="1000" >}}

{{< image src="stadtteile/hafencity-56" alt="Harbor" width="1000" >}}

{{< image src="stadtteile/hafencity-60" alt="Elbelysium" width="1000" height="1334" >}}

{{< image src="stadtteile/hafencity-54" alt="Unilever building" width="1000" >}}

At the *Magellan Terraces* I turned around and walked right along the *Kaiserkai*. Some of the old cranes were left there. At the *Elbphilharmonie* I went north across a bridge to the street *Am Sandtorkai* and followed it for a while. I made a detour over the floating pontoon in the harbor basin, where numerous historic ships form the *Traditionsschiffhafen* opened in 2008.

{{< image src="stadtteile/hafencity-61" alt="Shops" width="1000" height="1334" >}}

{{< image src="stadtteile/hafencity-70" alt="Traditionsschiffhafen" width="1000" caption="Traditionsschiffhafen." >}}

{{< image src="stadtteile/hafencity-69" alt="Traditionsschiffhafen." width="1000" >}}

{{< image src="stadtteile/hafencity-68" alt="HafenCity" width="1000" >}}

{{< image src="stadtteile/hafencity-67" alt="Columbus-Haus" width="1000" height="1334" >}}

{{< image src="stadtteile/hafencity-66" alt="Mosaik Wall" width="1000" >}}

{{< image src="stadtteile/hafencity-65" alt="Traditionsschiffhafen" width="1000" >}}

{{< image src="stadtteile/hafencity-64" alt="Traditionsschiffhafen" width="1000" >}}

{{< image src="stadtteile/hafencity-63" alt="Traditionsschiffhafen" width="1000" height="1334" >}}

{{< image src="stadtteile/hafencity-62" alt="Traditionsschiffhafen" width="1000" >}}

I walked along the *Sandtorpark* and across the *Coffee Plaza*, where a huge coffee bean is a work of art. Then along the *Tokiostraße* and over the Überseeboulevard. From *Dar-es-Salaam-Platz*, I was once again able to take a look at the *International Maritime Museum Hamburg* and the huge beer festival.

{{< image src="stadtteile/hafencity-71" alt="Kesselhaus" width="1000" height="1334" caption="The HafenCity InfoCenter in Kesselhaus." >}}

{{< image src="stadtteile/hafencity-73" alt="Überseeboulevard" width="1000" >}}

{{< image src="stadtteile/hafencity-72" alt="Coffee Plaza" width="1000" height="1334" caption="Coffee Plaza, a huge sculpture of a coffee bean." >}}

{{< image src="stadtteile/hafencity-74" alt="Überseeboulevard" width="1000" height="1334" >}}

{{< image src="stadtteile/hafencity-75" alt="Überseeboulevard" width="1000" height="1334" >}}

{{< image src="stadtteile/hafencity-80" alt="Internationale Maritime Museum Hamburg" width="1000" >}}

{{< image src="stadtteile/hafencity-79" alt="Old buildings of HafenCity" width="1000" >}}

{{< image src="stadtteile/hafencity-78" alt="New buildings of HafenCity" width="1000" >}}

{{< image src="stadtteile/hafencity-77" alt="Old harbor office" width="1000" >}}

{{< image src="stadtteile/hafencity-76" alt="New buildings of HafenCity" width="1000" height="1334" >}}

At the end I went along the street *Brooktorkai* for a while and then walked over the *Dienerreihe* and the *Holländisch-Brookfleet-Brücke*, where the *HafenCity* ends. On *Fleetinsel* stands the *Wasserschloss*, a restaurant overlooking two Fleets. On the way back to the subway *Meßberg* I passed by the *Dialog im Dunkeln*, where blind people guide you through an exhibition in completely darkened rooms, and get an insight into a life without sight. There is also *Dialog im Stillen*, an exhibition about Deafness.

{{< image src="stadtteile/hafencity-81" alt="Restaurant Wasserschloss" width="1000" height="1334" caption="Das Restaurant Wasserschloss." >}}

{{< image src="stadtteile/hafencity-82" alt="Dialog im Dunkeln" width="1000" height="1334" caption="Dialog im Dunkeln." >}}

{{< image src="map/hafencity" alt="HafenCity" width="1000" height="956" >}}

<iframe class="map" src="https://www.google.com/maps/d/u/0/embed?mid=1FlDK0lPn5-xsfQLkF75OHzu3bTEDLX5T" width="1000" height="500">
<
