---
title: Uhlenhorst
slug: uhlenhorst
author: Stefan Imhoff
date: 2015-08-16T18:00:00+02:00
distance: 12
duration: 2:45
---

From the underground station *Mundsburg* I went under the tracks of the subway to the east, where a new residential area is being built. Then to the south to the area of *Fachhochschule HAW*. Back to the north under the tracks, then *Richardstraße* to the south and over the *Eilbekkanal*.

{{< image src="stadtteile/uhlenhorst-01" alt="The tracks of the subway" width="1000" height="1334" caption="The tracks of the subway" >}}

{{< image src="stadtteile/uhlenhorst-02" alt="An unknown monument of a child with a dog" width="1000" height="1334" caption="An unknown monument of a child with a dog" >}}

{{< image src="stadtteile/uhlenhorst-03" alt="Houseboats on the Eilbekkanal" width="1000" caption="On the banks of the Eilbekkanal are houseboats" >}}

{{< image src="stadtteile/uhlenhorst-04" alt="Eilbekkanal" width="1000" height="1334" >}}

{{< image src="stadtteile/uhlenhorst-05" alt="Fachhochschule HAW" width="1000" caption="Die Gebäude der Fachhochschule HAW." >}}

{{< image src="stadtteile/uhlenhorst-06" alt="Eilbekkanal" width="1000" height="1334" >}}

{{< image src="stadtteile/uhlenhorst-07" alt="Eilbekkanal" width="1000" >}}

I walked along *Kuhmühlenteich* to the *St. Gertrud Church*, through the residential area south of *Kuhmühlenteich*, which still belongs to *Uhlenhorst*. Here are, as in many places in *Uhlenhorst* very beautiful old buildings with numerous decorations, stucco, each in its own style. Some architects have made themselves unforgotten with a name tag on the house.

{{< image src="stadtteile/uhlenhorst-08" alt="St. Gertrud-Kirche" width="1000" caption="St. Gertrud-Kirche" >}}

{{< image src="stadtteile/uhlenhorst-09" alt="St. Gertrud-Kirche" width="1000" height="1334" >}}

{{< image src="stadtteile/uhlenhorst-10" alt="St. Gertrud-Kirche" width="1000" height="1334" >}}

{{< image src="stadtteile/uhlenhorst-11" alt="Memorial with patron saints and churches of Hamburg" width="1000" caption="Memorial with patron saints and churches of Hamburg" >}}

{{< image src="stadtteile/uhlenhorst-12" alt="Monument to Carl Hermann Manehut" width="1000" height="1334" caption="Monument to Carl Hermann Manehut at the St. Gertrude Church" >}}

{{< image src="stadtteile/uhlenhorst-13" alt="Kuhmühlenteich" width="1000" height="1334" caption="Der Kuhmühlenteich." >}}

{{< image src="stadtteile/uhlenhorst-14" alt="Kuhmühlenteich" width="1000" height="1334" >}}

{{< image src="stadtteile/uhlenhorst-15" alt="Kuhmühlenbrücke" width="1000" height="1334" >}}

{{< image src="stadtteile/uhlenhorst-16" alt="Kuhmühlenteich" width="1000" >}}

{{< image src="stadtteile/uhlenhorst-17" alt="Kuhmühlenteich" width="1000" >}}

{{< image src="stadtteile/uhlenhorst-18" alt="The beautiful houses of Uhlenhorst" width="1000" height="1334" >}}

{{< image src="stadtteile/uhlenhorst-19" alt="The beautiful houses of Uhlenhorst" width="1000" height="1334" >}}

{{< image src="stadtteile/uhlenhorst-20" alt="The beautiful houses of Uhlenhorst" width="1000" >}}

{{< image src="stadtteile/uhlenhorst-21" alt="The beautiful houses of Uhlenhorst" width="1000" height="1334" >}}

{{< image src="stadtteile/uhlenhorst-22" alt="The beautiful houses of Uhlenhorst" width="1000" height="1334" >}}

{{< image src="stadtteile/uhlenhorst-23" alt="The beautiful houses of Uhlenhorst" width="1000" >}}

then continue to the west towards *Alster* and always along the shore and along the road *Schöne Aussicht*, past the *Feenteich* to the *Imam Ali Mosque*.

Along the *Alster* there is plenty of green space and seating with a view of the *Alster*. If you live here, you have a lot of money. There are no names on many bells, but only letters or initials to protect the identities of (presumably) prominent buyers.

{{< image src="stadtteile/uhlenhorst-24" alt="The Alsterufer at Uhlenhorst" width="1000" caption="The Alsterufer at Uhlenhorst" >}}

{{< image src="stadtteile/uhlenhorst-25" alt="The Alsterufer at Uhlenhorst" width="1000" >}}

{{< image src="stadtteile/uhlenhorst-26" alt="The Alsterufer at Uhlenhorst" width="1000" >}}

{{< image src="stadtteile/uhlenhorst-27" alt="The Alsterufer at Uhlenhorst" width="1000" >}}

{{< image src="stadtteile/uhlenhorst-28" alt="The Wolfgang Borchert monument on the banks of the Alster" width="1000" height="1334" caption="The Wolfgang Borchert monument on the banks of the Alster" >}}

{{< image src="stadtteile/uhlenhorst-29" alt="The expensive houses along the street Schöne Aussicht" width="1000" caption="The expensive houses along the street Schöne Aussicht and in the Fährhausstraße. A few new buildings, mostly old-style buildings." >}}

{{< image src="stadtteile/uhlenhorst-30" alt="The expensive houses along the street Schöne Aussicht" width="1000" >}}

{{< image src="stadtteile/uhlenhorst-31" alt="The expensive houses along the street Schöne Aussicht" width="1000" >}}

{{< image src="stadtteile/uhlenhorst-32" alt="The expensive houses along the street Schöne Aussicht" width="1000" >}}

{{< image src="stadtteile/uhlenhorst-33" alt="The expensive houses along the street Schöne Aussicht" width="1000" >}}

{{< image src="stadtteile/uhlenhorst-34" alt="The buildings at the Feenteich" width="1000" caption="The buildings at the Feenteich" >}}

{{< image src="stadtteile/uhlenhorst-35" alt="Imam Ali Mosque" width="1000" caption="The Islamic Center Hamburg in the Imam Ali Mosque." >}}

Continue east through Fährhausstraße and *Am Langenzug*, through Winterhuder Weg and back to the south. Right on Feenteich is the Consulate of the Russian Federation.

{{< image src="stadtteile/uhlenhorst-36" alt="Der Lange Zug" width="1000" caption="Der Lange Zug" >}}

{{< image src="stadtteile/uhlenhorst-37" alt="Der Hofwegkanal" width="1000" height="1334" caption="Der Hofwegkanal" >}}

{{< image src="stadtteile/uhlenhorst-38" alt="Der Hofwegkanal" width="1000" height="1334" >}}

{{< image src="stadtteile/uhlenhorst-39" alt="Heilandskirche on Winterhuder Weg." width="1000" height="1334" caption="Heilandskirche on Winterhuder Weg." >}}

Through the *Hofweg* to the north and through the canal road. Via the *Uhlenhorster Kanal* to the north and then to *Winterhuder Weg* to the east.

{{< image src="stadtteile/uhlenhorst-40" alt="Building in Uhlenhorst" width="1000" caption="Most of the buildings in Uhlenhorst are extremely pretty, with many beautifully decorated facades." >}}

{{< image src="stadtteile/uhlenhorst-41" alt="Building in Uhlenhorst" width="1000" height="1710" >}}

{{< image src="stadtteile/uhlenhorst-42" alt="Building in Uhlenhorst" width="1000" >}}

{{< image src="stadtteile/uhlenhorst-43" alt="Building in Uhlenhorst" width="1000" height="1309" >}}

{{< image src="stadtteile/uhlenhorst-44" alt="Building in Uhlenhorst" width="1000" height="1016" >}}

{{< image src="stadtteile/uhlenhorst-45" alt="Building in Uhlenhorst" width="1000" height="1334" >}}

{{< image src="stadtteile/uhlenhorst-46" alt="Building in Uhlenhorst" width="1000" height="1334" >}}

{{< image src="stadtteile/uhlenhorst-47" alt="Building in Uhlenhorst" width="1000" >}}

{{< image src="stadtteile/uhlenhorst-48" alt="Building in Uhlenhorst" width="1000" >}}

{{< image src="stadtteile/uhlenhorst-49" alt="Building in Uhlenhorst" width="1000" >}}

The goal of my tour was again the subway station *Mundsburg*.

{{< image src="stadtteile/uhlenhorst-50" alt="The underground station Mundsburg" width="1000" caption="The underground station Mundsburg" >}}

{{< image src="map/uhlenhorst" alt="Uhlenhorst" width="1000" height="956" >}}

<iframe class="map" src="https://www.google.com/maps/d/u/0/embed?mid=1L2qKPLoC_tKlOo3ZKlu8iKEvYpU" width="1000" height="500">
</iframe>
