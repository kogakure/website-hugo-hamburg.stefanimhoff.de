---
title: Hohenfelde
slug: hohenfelde
author: Stefan Imhoff
date: 2015-08-21T18:00:00+02:00
distance: 6
duration: 1:30
---

*Hohenfelde* is a small district, which is why I just needed an hour and a half to walk it completely. From the subway station *Lübecker Straße* I went first down the *Steinhauerdamm*, over the tracks and then along the *Bürgerweide* to *Sankt-Ansgar-Schule*.

{{< image src="stadtteile/hohenfelde-01" alt="Hohenfelde" width="1000" >}}

{{< image src="stadtteile/hohenfelde-02" alt="Hohenfelde" width="1000" >}}

{{< image src="stadtteile/hohenfelde-03" alt="Hohenfelde" width="1000" >}}

{{< image src="stadtteile/hohenfelde-04" alt="Hiobs-Hospital" width="1000" height="1334" >}}

{{< image src="stadtteile/hohenfelde-05" alt="Alida Schmidt Stift" width="1000" height="1334" >}}

{{< image src="stadtteile/hohenfelde-06" alt="Hohenfelde" width="1000" >}}

{{< image src="stadtteile/hohenfelde-07" alt="Sankt-Ansgar-Schule" width="1000" height="1334" >}}

From there I walked across the grounds of the *Marien-Krankenhaus*. There are also some old people’s homes here and I met an elderly, confused woman who asked me for a place, but then did not know which place and where she wanted to go.

{{< image src="stadtteile/hohenfelde-08" alt="Marien-Krankenhaus" width="1000" height="1698" >}}

{{< image src="stadtteile/hohenfelde-09" alt="Marien-Krankenhaus" width="1000" >}}

Through the *Angerstraße* and then north to *Lübecker Straße* past the round shaped office building *Hamburger Welle*. I turned into the *Wandsbeker Stieg*, passed the REWE supermarket and then went again and again north and south through *Hohenfelde*. The district was destroyed during *Operation Gomorrah* in 1943 up to 70 percent, which is why the majority of the houses are build in the clinker brick building of the post-war period, but there also a few newer buildings in between. Especially beautiful are the houses that have survived the bombing. The closer you get to *Uhlenhorst*, the more of it is preserved.

{{< image src="stadtteile/hohenfelde-10" alt="Hamburger Welle" width="1000" height="433" caption="Hamburger Welle" >}}

{{< image src="stadtteile/hohenfelde-11" alt="Building in Hohenfelde" width="1000" height="1334" >}}

{{< image src="stadtteile/hohenfelde-12" alt="Building in Hohenfelde" width="1000" >}}

{{< image src="stadtteile/hohenfelde-13" alt="Building in Hohenfelde" width="1000" height="1334" >}}

{{< image src="stadtteile/hohenfelde-14" alt="Building in Hohenfelde" width="1000" >}}

{{< image src="stadtteile/hohenfelde-15" alt="Poseidon-Plastik" width="1000" height="1334" >}}

{{< image src="stadtteile/hohenfelde-16" alt="Subway station Uhlenhorst" width="1000" >}}

{{< image src="stadtteile/hohenfelde-17" alt="Building in Hohenfelde" width="1000" height="1334" >}}

{{< image src="stadtteile/hohenfelde-18" alt="Building in Hohenfelde" width="1000" >}}

{{< image src="stadtteile/hohenfelde-19" alt="Building in Hohenfelde" width="1000" >}}

{{< image src="stadtteile/hohenfelde-20" alt="Building in Hohenfelde" width="1000" >}}

{{< image src="stadtteile/hohenfelde-21" alt="Building in Hohenfelde mit Graffiti" width="1000" height="1334" >}}

I went further west, past the *Alster swimming pool* and then north towards *Alster*. In the *Buchstraße* I could see a completely gutted house in which only the beautiful, old facade is preserved.

{{< image src="stadtteile/hohenfelde-22" alt="Die Alster-Schwimmhalle" width="1000" height="1334" caption="Die Alster-Schwimmhalle" >}}

{{< image src="stadtteile/hohenfelde-23" alt="Die Alster-Schwimmhalle" width="1000" >}}

{{< image src="stadtteile/hohenfelde-24" alt="Entkerntes Haus nahe der Alster" width="1000" caption="Entkerntes Haus nahe der Alster" >}}

Directly at the *Alster* on the *Alsterwiese Schwanenwik* it was packed, the entire meadow was filled with countless people, partly grilling or picnic holding. Directly at the shore is the bronze sculpture *Drei Mann im Boot* by *Edwin Scharff*. A little further afield, one of the *Vier Männer auf Bojen* by *Stephan Balkenhol*.

{{< image src="stadtteile/hohenfelde-25" alt="Alsterwiese Schwanenwik" width="1000" caption="Die Alsterwiese Schwanenwik" >}}

{{< image src="stadtteile/hohenfelde-26" alt="Alsterwiese Schwanenwik" width="1000" height="1334" >}}

{{< image src="stadtteile/hohenfelde-27" alt="Bronze sculpture Drei Mann im Boot by Edwin Scharff." width="1000" height="1334" caption="Bronze scuplture <em>Drei Mann im Boot</em> by Edwin Scharff." >}}

{{< image src="stadtteile/hohenfelde-28" alt="Bronze sculpture Drei Mann im Boot by Edwin Scharff." width="1000" height="1334" >}}

I went under the *Schwanenwikbrücke*, along the banks of the *Wandse* to the east, through *Papenhuder Str.*, *Graumannsweg* and *Schottweg*. Then through *Ifflandstraße* to *Kuhmühle*. At the subway station *Uhlandstraße* ended my tour.

{{< image src="stadtteile/hohenfelde-29" alt="The Wandse" width="1000" >}}

{{< image src="stadtteile/hohenfelde-30" alt="Schwanenwikbrücke and the Wandse" width="1000" >}}

{{< image src="stadtteile/hohenfelde-31" alt="Schwanenwikbrücke with view on the Alster lake" width="1000" height="1334" >}}

{{< image src="stadtteile/hohenfelde-32" alt="Building in Hohenfelde" width="1000" height="1334" >}}

{{< image src="stadtteile/hohenfelde-33" alt="Building in Hohenfelde" width="1000" >}}

{{< image src="stadtteile/hohenfelde-34" alt="Building in Hohenfelde" width="1000" >}}

{{< image src="stadtteile/hohenfelde-35" alt="Building in Hohenfelde" width="1000" height="1334" >}}

{{< image src="stadtteile/hohenfelde-36" alt="Building in Hohenfelde" width="1000" height="1334" >}}

{{< image src="stadtteile/hohenfelde-37" alt="The pharmacy at the Kuhmühle" width="1000" height="1334" caption="The pharmacy at the Kuhmühle" >}}

{{< image src="stadtteile/hohenfelde-38" alt="View of the Mundsburg towers in Barmbek-Süd" width="1000" height="1334" caption="View of the Mundsburg towers in Barmbek-Süd" >}}

{{< image src="stadtteile/hohenfelde-39" alt="Building in Hohenfelde" width="1000" >}}

{{< image src="map/hohenfelde" alt="Hohenfelde" width="1000" height="956" >}}

<iframe class="map" src="https://www.google.com/maps/d/u/0/embed?mid=1rW1Yd6AvY-Fq5uKXtx_EOmvdBnY" width="1000" height="500"></iframe>
