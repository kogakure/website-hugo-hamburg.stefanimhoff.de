---
title: Farmsen-Berne
slug: farmsen-berne
author: Stefan Imhoff
date: 2015-06-07T18:00:00+02:00
distance: 16
duration: 3:18
---

Farmsen-Berne has a lot of *allotment clubs*, north of *Luisenhof* are mostly 3-storey estates, moderately pretty, but mostly quiet.

{{< image src="stadtteile/farmsen-berne-02" alt="allotment clubs" width="1000" >}}

{{< image src="stadtteile/farmsen-berne-01" alt="allotment clubs" width="1000" >}}

East of *Strandbad Farmsen* you’ll find mostly single-family homes, well maintained and bourgeois. Even the driveways without a fence are symbolically locked with hanging chains.

Near the retention basin at *Berner Au* there is a lot of forest (*Berner Wald*), beautiful settlements, but also some skyscrapers. The settlement north of Berner Wald is very beautiful and has well-kept houses.

{{< image src="stadtteile/farmsen-berne-03" alt="retention basin" width="1000" >}}

{{< image src="stadtteile/farmsen-berne-04" alt="retention basin" width="1000" >}}

{{< image src="stadtteile/farmsen-berne-05" alt="A lot of playgrounds, forest and green." width="1000" caption="A lot of playgrounds, forest and green." >}}

The *Berner Gutspark* is extremly well maintained and the most beautiful park in the entire district. It is suitable for relaxing and reading on the lawn in fine weather. It can be reached in a few minutes from the subway station *Berne*. In the middle of the park is the *Berner Gutshaus*.

{{< image src="stadtteile/farmsen-berne-09" alt="Berner Gutspark with Berner Gutshaus" width="1000" >}}

{{< image src="stadtteile/farmsen-berne-08" alt="Berner Gutspark" width="1000" >}}

The route along the metro line has mostly 3- or 4-story settlements.

There is a lot of green and peacefulness, good jogging trails along *Berner Au*. Many playgrounds, sports fields, adventure playgrounds and construction play grounds.

{{< image src="stadtteile/farmsen-berne-06" alt="adventure playground and construction playground in the forest" width="1000" >}}

{{< image src="stadtteile/farmsen-berne-07" alt="adventure playground and construction playground in the forest" width="1000" >}}

{{< image src="map/farmsen-berne" alt="Farmsen-Berne" width="1000" >}}

<iframe class="map" src="https://www.google.com/maps/d/u/0/embed?mid=1lrph_CzuZjhnBOCx_v7VKgAVN8I" width="1000" height="500"></iframe>
