---
title: Altstadt
slug: altstadt
author: Stefan Imhoff
date: 2016-05-07T18:00:00+02:00
distance: 18
duration: 5:17
---

It was a sunny day with temperatures above 20 ° C, when I started my tour through the old town of Hamburg. From *Hauptbahnhof* I headed for Deichtorhallen, past the *City-Hof-Passage*, some of the first skyscrapers in Hamburg built in the 1950s. Even if they are not really a beauty, they have historical value.

{{< image src="stadtteile/altstadt-01" alt="City-Hof-Passage" width="1000" caption="City-Hof-Passage" >}}

{{< image src="stadtteile/altstadt-02" alt="Place in front of the Deichtorhallen" width="1000" >}}

I walked along the banks of the Zollkanal, the Spiegel building on the left, to *Meßberg* and from there to the north past the *Hachez Chocoversum*, which, despite the high temperatures, was well attended. Through the historic *Kontorhausviertel* (which belongs to the UNESCO World Heritage) with *Sprinkenhof*, *Mohlenhof*, *Chilehaus* and other beautiful facades.

{{< image src="stadtteile/altstadt-03" alt="Shore of the customs channel" width="1000" >}}

{{< image src="stadtteile/altstadt-04" alt="Shore of the customs channel" width="1000" >}}

{{< image src="stadtteile/altstadt-05" alt="Hachez Chocoversum" width="1000" >}}

{{< image src="stadtteile/altstadt-06" alt="Kontorhausviertel" width="1000" caption="Kontorhausviertel" >}}

{{< image src="stadtteile/altstadt-07" alt="Kontorhausviertel" width="1000" >}}

{{< image src="stadtteile/altstadt-08" alt="Kontorhausviertel" width="1000" >}}

{{< image src="stadtteile/altstadt-09" alt="Kontorhausviertel" width="1000" >}}

{{< image src="stadtteile/altstadt-10" alt="Kontorhausviertel: Chilehaus" width="1000" height="1334" >}}

{{< image src="stadtteile/altstadt-11" alt="Kontorhausviertel: Sprinken-Hof" width="1000" >}}

{{< image src="stadtteile/altstadt-12" alt="Kontorhausviertel: Chilehaus" width="1000" >}}

{{< image src="stadtteile/altstadt-13" alt="Kontorhausviertel: Mohlenhof" width="1000" height="1334" >}}

{{< image src="stadtteile/altstadt-14" alt="Building in the Altstadt" width="1000" >}}

{{< image src="stadtteile/altstadt-15" alt="Building in the Altstadt" width="1000" >}}

{{< image src="stadtteile/altstadt-16" alt="Building in the Altstadt" width="1000" height="1334" >}}

At *Domplatz*, where there is a park with seating today, was probably formerly the *Hammaburg*, which gave Hamburg its name. It was less a *castle*, but more a mound with wooden palisades, five to six meters high, 15 meters wide and about 130 meters in length and width.

There is also the seat of *Parship*, the *Zeit* and the *Church of Scientology Hamburg*, in whose shop window *L. Ron Hubbards* books are laid out and all sorts of publicity for the psycho-tests of the church are made. There is also the *Sankt Petri church*, which I visited later.

{{< image src="stadtteile/altstadt-17" alt="Domplatz" width="1000" caption="Domplatz" >}}

{{< image src="stadtteile/altstadt-18" alt="Domplatz with view of Sankt Petri Kirche" width="1000" height="1334" >}}

{{< image src="stadtteile/altstadt-19" alt="Domplatz" width="1000" >}}

{{< image src="stadtteile/altstadt-20" alt="Domplatz" width="1000" >}}

For the time being, however, I took the path to the south and crossed the northern *Speicherstadt*. The Speicherstadt was built in 1883. Past *Dialog im Stillen* and *Dialog im Dunkeln*, *Wasserschloß* and *Holländischer Brook* and *Fleetschlösschen*. On the canals numerous boats of the harbor cruises were on the way.

{{< image src="stadtteile/altstadt-22" alt="Speicherstadt" width="1000" >}}

{{< image src="stadtteile/altstadt-23" alt="Speicherstadt" width="1000" >}}

{{< image src="stadtteile/altstadt-24" alt="Speicherstadt" width="1000" height="1334" >}}

{{< image src="stadtteile/altstadt-25" alt="Speicherstadt" width="1000" height="1334" >}}

{{< image src="stadtteile/altstadt-26" alt="Speicherstadt" width="1000" >}}

From there I went over the maiden bridge to the north and walked around the *Kirche St. Katharinen* and passed through the *Grimm*, before I went to the Speicherstadt again. In front of the Miniatur Wunderland, it was packed, and busloads of tourists were carted to the port’s birthday party.

{{< image src="stadtteile/altstadt-27" alt="Kirche St. Katharinen" width="1000" height="1334" caption="Kirche St. Katharinen" >}}

{{< image src="stadtteile/altstadt-28" alt="View of St. Nikolai Kirche" width="1000" >}}

{{< image src="stadtteile/altstadt-29" alt="Kirche St. Katharinen" width="1000" height="1334" >}}

{{< image src="stadtteile/altstadt-30" alt="Modern building" width="1000" >}}

{{< image src="stadtteile/altstadt-31" alt="Speicherstadt" width="1000" >}}

{{< image src="stadtteile/altstadt-32" alt="Propeller" width="1000" >}}

{{< image src="stadtteile/altstadt-33" alt="Building in the Altstadt" width="1000" >}}

After a round on the *Cremon Island* I looked at the *Nikolaifleet*, where the Port of Hamburg was built in 1188. Here is still some of the Althamburg buildings have been preserved. Up the *Deichstraße*, where you can see the historic town houses of the city. Here started also the *Hamburger Brand* of May 5, 1842 began. The house in which the fire broke out is today called *Zum Brandanfang*.

{{< image src="stadtteile/altstadt-34" alt="Nikolaifleet" width="1000" caption="Nikolaifleet" >}}

{{< image src="stadtteile/altstadt-35" alt="Deichstraße" width="1000" height="1334" caption="Deichstraße" >}}

{{< image src="stadtteile/altstadt-36" alt="Deichstraße" width="1000" >}}

From *Steintwiete* I went down the *Rödingsmarkt*, where the tracks of the subway run above ground on a bridge. I made a detour through the street *Herrlichkeit* that runs along the *Alsterfleet*. Here are modern apartments overlooking the Fleet.

{{< image src="stadtteile/altstadt-37" alt="Rödingsmarkt" width="1000" caption="Rödingsmarkt" >}}

{{< image src="stadtteile/altstadt-38" alt="Flats at Alsterfleet" width="1000" >}}

Then I headed towords *St. Nikolai Church*, which today is a memorial against the war. The nave was completely destroyed by bombs in World War II, leaving only the outer walls and the tower.

{{< image src="stadtteile/altstadt-39" alt="St. Nikolai Kirche" width="1000" height="1334" >}}

{{< image src="stadtteile/altstadt-40" alt="Memorial at St. Nikolai Kirche" width="1000" >}}

{{< image src="stadtteile/altstadt-41" alt="St. Nikolai Kirche" width="1000" height="1334" >}}

{{< image src="stadtteile/altstadt-42" alt="St. Nikolai Kirche" width="1000" >}}

{{< image src="stadtteile/altstadt-43" alt="St. Nikolai Kirche" width="1000" >}}

{{< image src="stadtteile/altstadt-44" alt="St. Nikolai Kirche" width="1000" >}}

Over the Trostbrücke, past the old stock exchange, where until 1842 also the old town hall stood, which was vainly blown up, in order to stop the *Hamburg fire*. At the *Handelskammer Hamburg* I turned into the *Mönkedamm* and then walked over the *Old Wall* towards *Rathaus*. In the courtyard of the town hall is a beautiful fountain and the outdoor areas of *Restaurant Parliament Hamburg*. I took a little break inside the town hall before going on to St. Peter's Church.

{{< image src="stadtteile/altstadt-45" alt="Zum Alten Rathaus" width="1000" >}}

{{< image src="stadtteile/altstadt-46" alt="Handelskammer Hamburg" width="1000" >}}

{{< image src="stadtteile/altstadt-47" alt="Rathaus" width="1000" caption="Rathaus" >}}

{{< image src="stadtteile/altstadt-48" alt="Rathaus" width="1000" >}}

{{< image src="stadtteile/altstadt-49" alt="Rathaus" width="1000" height="1334" >}}

{{< image src="stadtteile/altstadt-50" alt="Rathaus" width="1000" >}}

{{< image src="stadtteile/altstadt-51" alt="Rathaus" width="1000" height="1334" >}}

{{< image src="stadtteile/altstadt-52" alt="Rathaus" width="1000" height="1334" >}}

{{< image src="stadtteile/altstadt-53" alt="Rathaus" width="1000" height="1334" >}}

{{< image src="stadtteile/altstadt-54" alt="Rathaus" width="1000" height="1334" >}}

{{< image src="stadtteile/altstadt-55" alt="Rathaus" width="1000" height="1334" >}}

{{< image src="stadtteile/altstadt-56" alt="Rathaus" width="1000" height="1334" >}}

I have decided without further ado to climb the church tower, as this is the highest vantage point of Hamburg. In contrast to the *Kirche St. Michaelis* there is no lift, but only a lot of steps, which run in quite dizzying heights. There are several floors where you can get a view of the city from round windows. The highest point of 123 meters is directly in the top of the church tower, where just enough room for 3-4 people. It also fluctuated slightly due to the weight of the visitors or the wind. Definitely not for people with vertigo, but the view is fantastic.

{{< image src="stadtteile/altstadt-57" alt="Church of Scientology Hamburg" width="1000" height="1334" >}}

{{< image src="stadtteile/altstadt-58" alt="Sankt Petri Kirche" width="1000" height="1334" caption="Sankt Petri Kirche" >}}

{{< image src="stadtteile/altstadt-59" alt="Bells in the Sankt Petri church" width="1000" height="1334" >}}

{{< image src="stadtteile/altstadt-60" alt="View from the steeple of St. Petri Kirche" width="1000" >}}

{{< image src="stadtteile/altstadt-61" alt="View from the steeple of St. Petri Kirche" width="1000" height="1334" >}}

{{< image src="stadtteile/altstadt-62" alt="View from the steeple of St. Petri Kirche" width="1000" >}}

{{< image src="stadtteile/altstadt-63" alt="View from the steeple of St. Petri Kirche" width="1000" >}}

{{< image src="stadtteile/altstadt-64" alt="View from the steeple of St. Petri Kirche" width="1000" >}}

{{< image src="stadtteile/altstadt-66" alt="Church tower of Sankt Petri Kirche" width="1000" height="1334" >}}

{{< image src="stadtteile/altstadt-65" alt="Church tower of Sankt Petri Kirche" width="1000" >}}

{{< image src="stadtteile/altstadt-67" alt="Church tower of Sankt Petri Kirche" width="1000" height="1334" >}}

I then went through the *Mönckebergstraße* and the *Alstertor* up towards *Binnenalster*, past the *Thalia Theater*. I headed south again through *Spitalerstraße* and around the *Kirche St. Jacobi*. Then I walked along the Bugenhagenstraße and once surrounded Saturn, before I went once again along the *Mönckebergstraße* and *Spitalerstraße*. It was incredibly crowded and loud: Peruvian panpipes, plastic scrap drummers and pounding rock music from the fashion shops.

{{< image src="stadtteile/altstadt-68" alt="Building near Mönckebergstraße" width="1000" >}}

{{< image src="stadtteile/altstadt-69" alt="Thalia Theater" width="1000" >}}

{{< image src="stadtteile/altstadt-70" alt="Building" width="1000" >}}

{{< image src="stadtteile/altstadt-71" alt="Elbphilharmonie-Kulturcafé" width="1000" >}}

{{< image src="stadtteile/altstadt-72" alt="Hauptbahnhof" width="1000" caption="Hauptbahnhof" >}}

{{< image src="stadtteile/altstadt-73" alt="Building in Brandsende" width="1000" >}}

{{< image src="stadtteile/altstadt-74" alt="Building" width="1000" >}}

Then I went through the northern part of the old town and through the street *Brandsende*, where the *Hamburger Brand* found its end. Finally, I went to *Glockengießerwall* down to the *Hauptbahnhof*, where my tour ended.

{{< image src="stadtteile/altstadt-75" alt="Glockengießerwall" width="1000" >}}

{{< image src="stadtteile/altstadt-76" alt="Glockengießerwall" width="1000" >}}

{{< image src="map/altstadt" alt="Altstadt" width="1000" >}}

<iframe class="map" src="https://www.google.com/maps/d/u/0/embed?mid=12OxD8wbv4uwG-kZjHADAHLsgvhI" width="1000" height="500"></iframe>
