---
title: Hammerbrook
slug: hammerbrook
author: Stefan Imhoff
date: 2015-09-12T18:00:00+02:00
distance: 11
duration: 2:39
---

From the main station I went south to the *Deichtorplatz*, past the *Deichtorhallen* to the *Oberhafen* and then to the southeast. Directly at the *Oberhafen* is the *Fruchthof*, here begins the district *Hammerbrook*.

{{< image src="stadtteile/hammerbrook-01" alt="Hauptbahnhof" width="1000" >}}

{{< image src="stadtteile/hammerbrook-02" alt="Construction site" width="1000" height="1334" >}}

{{< image src="stadtteile/hammerbrook-03" alt="View to the Michel" width="1000" height="1334" >}}

{{< image src="stadtteile/hammerbrook-04" alt="Deichtorhallen" width="1000" >}}

{{< image src="stadtteile/hammerbrook-05" alt="Deichtorhallen" width="1000" >}}

{{< image src="stadtteile/hammerbrook-06" alt="Deichtorhallen" width="1000" height="1334" >}}

{{< image src="stadtteile/hammerbrook-07" alt="Deichtorhallen" width="1000" height="361" >}}

{{< image src="stadtteile/hammerbrook-08" alt="Spiegel building" width="1000" >}}

{{< image src="stadtteile/hammerbrook-09" alt="Oberhafen" width="1000" >}}

{{< image src="stadtteile/hammerbrook-10" alt="Fruchhof Hamburg" width="1000" caption="Fruchhof Hamburg" >}}

{{< image src="stadtteile/hammerbrook-11" alt="Fruchhof Hamburg" width="1000" >}}

{{< image src="stadtteile/hammerbrook-12" alt="Fruchhof Hamburg" width="1000" height="1334" >}}

{{< image src="stadtteile/hammerbrook-13" alt="Brücke am Oberhafen" width="1000" >}}

{{< image src="stadtteile/hammerbrook-14" alt="Oberhafen" width="1000" >}}

{{< image src="stadtteile/hammerbrook-15" alt="Oberhafen" width="1000" >}}

I walked along the water and looked at the floodgate between *Oberhafen* and *Schleusenkanal*. Unfortunately I could see it only from a distance, because some smartass came up with the great idea to transform the information sign and the best place for a photo into a fenced-in commercial area. I followed the water for a while, until I realized that the huge trading area where only fruit was traded and blocked my way to *Hammerbrook*, extended to *Bille*. So I had to turn around and wander it from the other side.

{{< image src="stadtteile/hammerbrook-16" alt="Schleusenkanal" width="1000" caption="Schleusenkanal" >}}

{{< image src="stadtteile/hammerbrook-17" alt="Schleusenkanal" width="1000" >}}

{{< image src="stadtteile/hammerbrook-18" alt="Brückenschmuck am Schleusenkanal" width="1000" height="1334" >}}

{{< image src="stadtteile/hammerbrook-19" alt="Schleusenkanal" width="1000" >}}

{{< image src="stadtteile/hammerbrook-28" alt="Commercial property at Oberhafen" width="1000" caption="Hammerbrook has predominantly commercial and industrial sites: a large fruit trade at the Oberhafen, only very few apartments, but old factory buildings and large warehouses." >}}

I went down the *Amsinckstraße*, where on one side only huge glass buildings and on the other side the fruit trade was located. On the *Süderstraße* I went into the huge industrial area, and followed the road *Grüner Deich* to the *flood basin* followed. In *Hammerbrook* many major insurance companies, banks, car dealerships or trading companies have their headquarters.

The city is crossed by several train tracks, both the S-Bahn from the main station to the south, as well as the tracks of the Deutsche Bahn. The tracks run at height on bridges over the city.

{{< image src="stadtteile/hammerbrook-20" alt="Train tracks above Hammerbrook" width="1000" height="1334" >}}

{{< image src="stadtteile/hammerbrook-21" alt="Commercial properties in Hammerbrook" width="1000" >}}

{{< image src="stadtteile/hammerbrook-22" alt="Commercial properties in Hammerbrook" width="1000" height="1334" >}}

{{< image src="stadtteile/hammerbrook-23" alt="Commercial properties in Hammerbrook" width="1000" height="1334" >}}

There are hardly any houses in *Hammerbrook*, just over 2,000 people live in the large district, most of the houses are in very poor condition and offer little desirable living space.

{{< image src="stadtteile/hammerbrook-24" alt="Commercial properties in Hammerbrook" width="1000" >}}

{{< image src="stadtteile/hammerbrook-25" alt="Residential properties in Hammerbrook" width="1000" >}}

From the bridges over the *Hochwasserbassin* you have a fantastic view of the *Berliner Bogen*. Further to the east of *Hammerbrook* are companies like Stadtreinigung and *SVG-Hamburg Straßenverkehrsgenossenschaft*. There is also an event location, which is located in an old warehouse and can be booked for fashion shoots or the like. If you want to register your car or need a license plate, you will also have to drive to this part of *Hammerbrook*. Around the registration office all sorts of car dealerships, TÜV Hanse, shops for signs or rapid approvals have settled.

{{< image src="stadtteile/hammerbrook-26" alt="Warehouses and event location" width="1000" >}}

{{< image src="stadtteile/hammerbrook-27" alt="Graffiti" width="1000" >}}

{{< image src="stadtteile/hammerbrook-29" alt="View of the Berliner Bogen" width="1000" height="1334" >}}

{{< image src="stadtteile/hammerbrook-30" alt="View of the Berliner Bogen" width="1000" >}}

{{< image src="stadtteile/hammerbrook-31" alt="Canals in Hammerbrook" width="1000" >}}

{{< image src="stadtteile/hammerbrook-32" alt="Canals in Hammerbrook" width="1000" >}}

{{< image src="stadtteile/hammerbrook-33" alt="Canals in Hammerbrook" width="1000" height="1334" >}}

{{< image src="stadtteile/hammerbrook-34" alt="Canals in Hammerbrook" width="1000" >}}

{{< image src="stadtteile/hammerbrook-35" alt="Canals in Hammerbrook" width="1000" >}}

{{< image src="stadtteile/hammerbrook-36" alt="Hamburger Stadtreinigung" width="1000" height="1334" >}}

{{< image src="stadtteile/hammerbrook-37" alt="Numerous car dealerships and registration offices" width="1000" height="1334" >}}

I went to the northeastern part of *Hammerbrook* via the *south channel*. Here is the *Berufliche Schule Recycling- und Umwelttechnik*, which sits in parts in a beautiful old brick building with ornate decorations. At the entrance of the school is a huge robot made of pieces of scrap metal.

{{< image src="stadtteile/hammerbrook-38" alt="Berufliche Schule Recycling- und Umwelttechnik" width="1000" height="1334" caption="Berufliche Schule Recycling- und Umwelttechnik" >}}

{{< image src="stadtteile/hammerbrook-39" alt="Berufliche Schule Recycling- und Umwelttechnik" width="1000" height="1334" >}}

I walked the *Wendenstraße* westwards, on one side of which *Hammerbrook* lies and on the other side *Borgfelde*. This road I have already partially gone on my tour of *Borgfelde* along.

On the bank of the *Victoriakais* (to be seen well from the *Wendenbrücke*) several floating houses (*Floating Homes*) are anchored. Here you can visit the different models before buying such a house.

{{< image src="stadtteile/hammerbrook-40" alt="Floating houses at the Victoriakai" width="1000" caption="Floating houses at the Victoriakai" >}}

{{< image src="stadtteile/hammerbrook-41" alt="Kai 10" width="1000" caption="Kai 10" >}}

{{< image src="stadtteile/hammerbrook-43" alt="Canals in Hammerbrook" width="1000" >}}

Over the *Heidenkampsweg*, which one drives along, if one wants to leave Hamburg over the landing bridges or wants to enter I went further west, past the rapid-transit railway station *Hammerbrook* and then over the *middle channel* to the north. A modern office building joins the other one here.

{{< image src="stadtteile/hammerbrook-44" alt="Office building in Hammerbrook" width="1000" height="1334" >}}

{{< image src="stadtteile/hammerbrook-45" alt="Office building in Hammerbrook" width="1000" >}}

{{< image src="stadtteile/hammerbrook-46" alt="Office building in Hammerbrook" width="1000" >}}

{{< image src="stadtteile/hammerbrook-47" alt="Office building in Hammerbrook" width="1000" height="1334" >}}

{{< image src="stadtteile/hammerbrook-48" alt="Office building in Hammerbrook" width="1000" >}}

{{< image src="stadtteile/hammerbrook-49" alt="Office building in Hammerbrook" width="1000" >}}

{{< image src="stadtteile/hammerbrook-50" alt="Office building in Hammerbrook" width="1000" >}}

{{< image src="stadtteile/hammerbrook-51" alt="Office building in Hammerbrook" width="1000" height="1334" >}}

{{< image src="stadtteile/hammerbrook-52" alt="Office building in Hammerbrook" width="1000" >}}

{{< image src="stadtteile/hammerbrook-53" alt="Office building in Hammerbrook" width="1000" height="470" caption="A variety of insurance companies have their headquarters in Hammerbrook." >}}

{{< image src="stadtteile/hammerbrook-54" alt="Construction site in Hammerbrook" width="1000" >}}

I walked down the *Nagelsweg* to the *middle channel*, where *Circus Roncalli* had set up camp in a huge square. On the banks of the *central canal* was *Kai 10*, a floating event location for meetings and celebrations at anchor, a big party was in progress.

{{< image src="stadtteile/hammerbrook-55" alt="Circus Roncalli" width="1000" >}}

{{< image src="stadtteile/hammerbrook-42" alt="Kai 10" width="1000" >}}

I went up the *Högerdamm*, on the left side countless sidings of the main station and a design hotel, on the right side of some of the few, very beautiful, old houses that have survived the bombing. The proximity to the train station unfortunately reduces the condition of the houses, which are not well maintained. I walked under the *Nordkanalbrücke* along *Repsoldstraße* and *Rosenallee*, where there are more old, beautiful houses.

{{< image src="stadtteile/hammerbrook-56" alt="Older houses in Hammerbrook" width="1000" height="1334" caption="A few houses have survived the bombing of Hammerbrook." >}}

{{< image src="stadtteile/hammerbrook-57" alt="Design Hotel" width="1000" >}}

{{< image src="stadtteile/hammerbrook-58" alt="Older houses in Hammerbrook" width="1000" >}}

{{< image src="stadtteile/hammerbrook-59" alt="Car bridge" width="1000" height="1334" >}}

{{< image src="stadtteile/hammerbrook-60" alt="Car bridge" width="1000" >}}

From there through *Spaldingstraße* and *Nordkanalstraße* to the east. Again, one office building after another, some hotels that benefit from their proximity to the station. In addition, a large skateboard system with hall and numerous ramps in the *Spaldingstraße*.

{{< image src="stadtteile/hammerbrook-61" alt="Older buildings in Hammerbrook" width="1000" height="1334" >}}

{{< image src="stadtteile/hammerbrook-62" alt="Older buildings in Hammerbrook" width="1000" height="1334" >}}

{{< image src="stadtteile/hammerbrook-63" alt="Older buildings in Hammerbrook" width="1000" height="1334" >}}

{{< image src="stadtteile/hammerbrook-64" alt="City mission Hamburg" width="1000" height="1334" >}}

{{< image src="stadtteile/hammerbrook-65" alt="Older buildings in Hammerbrook" width="1000" height="1334" >}}

{{< image src="stadtteile/hammerbrook-66" alt="Older buildings in Hammerbrook" width="1000" >}}

{{< image src="stadtteile/hammerbrook-67" alt="pedestrian tunnel" width="1000" height="1334" >}}

{{< image src="stadtteile/hammerbrook-68" alt="Car bridge" width="1000" >}}

{{< image src="stadtteile/hammerbrook-69" alt="Commercial building in Hammerbrook" width="1000" >}}

My tour ended on the *Ankelmannsplatz*, directly at the S and U Bahn station *Berliner Tor*, where also the modern glass building *Berliner Bogen* stands, which with 14,000 square meters glass area largest glass building in Europe.

{{< image src="stadtteile/hammerbrook-70" alt="Berliner Bogen" width="1000" height="521" caption="The glass building Berliner Bogen, with 14,000 square meters of glass, the largest glass building in Europe." >}}

{{< image src="stadtteile/hammerbrook-71" alt="Berliner Bogen" width="1000" >}}

{{< image src="stadtteile/hammerbrook-72" alt="Berliner Bogen" width="1000" >}}

{{< image src="map/hammerbrook" alt="Hammerbrook" width="1000" height="956" >}}

<iframe class="map" src="https://www.google.com/maps/d/u/0/embed?mid=1p3UJHRb9zhVSrtVda6V-FVTKe8w" width="1000" height="500"></iframe>
