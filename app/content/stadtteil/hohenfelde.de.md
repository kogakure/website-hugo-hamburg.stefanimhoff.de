---
title: Hohenfelde
slug: hohenfelde
author: Stefan Imhoff
date: 2015-08-21T18:00:00+02:00
distance: 6
duration: 1:30
---

*Hohenfelde* ist ein kleiner Stadtteil, weshalb ich gerade einmal eineinhalb Stunden gebraucht habe, ihn komplett zu durchwandern. Von der U-Bahn-Station *Lübecker Straße* aus bin ich zuerst den *Steinhauerdamm* hinunter gegangen, über die Gleise und dann die *Bürgerweide* entlang bis zur *Sankt-Ansgar-Schule*.

{{< image src="stadtteile/hohenfelde-01" alt="Hohenfelde" width="1000" >}}

{{< image src="stadtteile/hohenfelde-02" alt="Hohenfelde" width="1000" >}}

{{< image src="stadtteile/hohenfelde-03" alt="Hohenfelde" width="1000" >}}

{{< image src="stadtteile/hohenfelde-04" alt="Hiobs-Hospital" width="1000" height="1334" >}}

{{< image src="stadtteile/hohenfelde-05" alt="Alida Schmidt Stift" width="1000" height="1334" >}}

{{< image src="stadtteile/hohenfelde-06" alt="Hohenfelde" width="1000" >}}

{{< image src="stadtteile/hohenfelde-07" alt="Sankt-Ansgar-Schule" width="1000" height="1334" >}}

Von dort aus quer durch das Gelände des *Marien-Krankenhauses*. Hier gibt es auch einige Seniorenheime und mir ist eine ältere, verwirrte Frau begegnet, die mich nach einem Ort gefragt hat, dann aber nicht mehr wusste, welcher Ort und wohin sie eigentlich wollte.

{{< image src="stadtteile/hohenfelde-08" alt="Marien-Krankenhaus" width="1000" height="1698" >}}

{{< image src="stadtteile/hohenfelde-09" alt="Marien-Krankenhaus" width="1000" >}}

Durch die *Angerstraße* und dann nach Norden zur *Lübecker Straße* vorbei an dem rund geformten Bürogebäude *Hamburger Welle*. Ich bin in den *Wandsbeker Stieg* eingebogen, vorbei am REWE Supermarkt und dann immer wieder nach Norden und Süden durch *Hohenfelde* gegangen. Der Stadtteil wurde während der *Operation Gomorrha* 1943 zu 70 Prozent zerstört, weshalb der Großteil der Häuser Klinkerbau der Nachkriegszeit ist, dazwischen aber auch ein paar neuere Gebäude. Besonders schön sind die Häuser anzuschauen, die den Bombenangriff überstanden haben. Je näher man *Uhlenhorst* kommt, desto mehr davon sind erhalten.

{{< image src="stadtteile/hohenfelde-10" alt="Hamburger Welle" width="1000" height="433" caption="Hamburger Welle" >}}

{{< image src="stadtteile/hohenfelde-11" alt="Gebäude in Hohenfelde" width="1000" height="1334" >}}

{{< image src="stadtteile/hohenfelde-12" alt="Gebäude in Hohenfelde" width="1000" >}}

{{< image src="stadtteile/hohenfelde-13" alt="Gebäude in Hohenfelde" width="1000" height="1334" >}}

{{< image src="stadtteile/hohenfelde-14" alt="Gebäude in Hohenfelde" width="1000" >}}

{{< image src="stadtteile/hohenfelde-15" alt="Poseidon-Plastik" width="1000" height="1334" >}}

{{< image src="stadtteile/hohenfelde-16" alt="U-Bahn-Station Uhlenhorst" width="1000" >}}

{{< image src="stadtteile/hohenfelde-17" alt="Gebäude in Hohenfelde" width="1000" height="1334" >}}

{{< image src="stadtteile/hohenfelde-18" alt="Gebäude in Hohenfelde" width="1000" >}}

{{< image src="stadtteile/hohenfelde-19" alt="Gebäude in Hohenfelde" width="1000" >}}

{{< image src="stadtteile/hohenfelde-20" alt="Gebäude in Hohenfelde" width="1000" >}}

{{< image src="stadtteile/hohenfelde-21" alt="Gebäude in Hohenfelde mit Graffiti" width="1000" height="1334" >}}

Ich bin weiter nach Westen gegangen, vorbei an der *Alster-Schwimmhalle* und dann nach Norden Richtung *Alster*. In der *Buchstraße* konnte ich ein völlig entkerntes Haus sehen, bei dem nur die schöne, alte Fassade erhalten bleibt.

{{< image src="stadtteile/hohenfelde-22" alt="Die Alster-Schwimmhalle" width="1000" height="1334" caption="Die Alster-Schwimmhalle" >}}

{{< image src="stadtteile/hohenfelde-23" alt="Die Alster-Schwimmhalle" width="1000" >}}

{{< image src="stadtteile/hohenfelde-24" alt="Entkerntes Haus nahe der Alster" width="1000" caption="Entkerntes Haus nahe der Alster" >}}

Direkt an der *Alster* auf der *Alsterwiese Schwanenwik* war es brechend voll, die komplette Wiese war mit unzähligen Menschen gefüllt, teils grillend oder Picknick haltend. Direkt am Ufer steht die Bronzeplastik *Drei Mann im Boot* von *Edwin Scharff*. Etwas weiter draußen schwimmt einer der *Vier Männer auf Bojen* von *Stephan Balkenhol*.

{{< image src="stadtteile/hohenfelde-25" alt="Die Alsterwiese Schwanenwik" width="1000" caption="Die Alsterwiese Schwanenwik" >}}

{{< image src="stadtteile/hohenfelde-26" alt="Die Alsterwiese Schwanenwik" width="1000" height="1334" >}}

{{< image src="stadtteile/hohenfelde-27" alt="Bronzeplastik Drei Mann im Boot von Edwin Scharff." width="1000" height="1334" caption="Bronzeplastik <em>Drei Mann im Boot</em> von Edwin Scharff." >}}

{{< image src="stadtteile/hohenfelde-28" alt="Bronzeplastik Drei Mann im Boot von Edwin Scharff." width="1000" height="1334" >}}

Ich bin unter der *Schwanenwikbrücke* hindurchgegangen, am Ufer der *Wandse* entlang nach Osten, durch *Papenhuder Str.*, *Graumannsweg* und *Schottweg*. Dann durch die *Ifflandstraße* zur *Kuhmühle*. An der U-Bahn-Station *Uhlandstraße* endete meine Tour.

{{< image src="stadtteile/hohenfelde-29" alt="Die Wandse" width="1000" >}}

{{< image src="stadtteile/hohenfelde-30" alt="Die Schwanenwikbrücke und die Wandse" width="1000" >}}

{{< image src="stadtteile/hohenfelde-31" alt="Die Schwanenwikbrücke mit Blick auf die Alster" width="1000" height="1334" >}}

{{< image src="stadtteile/hohenfelde-32" alt="Gebäude in Hohenfelde" width="1000" height="1334" >}}

{{< image src="stadtteile/hohenfelde-33" alt="Gebäude in Hohenfelde" width="1000" >}}

{{< image src="stadtteile/hohenfelde-34" alt="Gebäude in Hohenfelde" width="1000" >}}

{{< image src="stadtteile/hohenfelde-35" alt="Gebäude in Hohenfelde" width="1000" height="1334" >}}

{{< image src="stadtteile/hohenfelde-36" alt="Gebäude in Hohenfelde" width="1000" height="1334" >}}

{{< image src="stadtteile/hohenfelde-37" alt=" Die Apotheke an der Kuhmühle" width="1000" height="1334" caption="Die Apotheke an der Kuhmühle" >}}

{{< image src="stadtteile/hohenfelde-38" alt="Blick auf die Mundsburg-Türme in Barmbek-Süd" width="1000" height="1334" caption="Blick auf die Mundsburg-Türme in Barmbek-Süd" >}}

{{< image src="stadtteile/hohenfelde-39" alt="Gebäude in Hohenfelde" width="1000" >}}

{{< image src="map/hohenfelde" alt="Hohenfelde" width="1000" height="956" >}}

<iframe class="map" src="https://www.google.com/maps/d/u/0/embed?mid=1rW1Yd6AvY-Fq5uKXtx_EOmvdBnY" width="1000" height="500"></iframe>
