---
title: Barmbek-Süd
slug: barmbek-sued
author: Stefan Imhoff
date: 2015-07-11T18:00:00+02:00
distance: 16
duration: 3:40
---

Von der U-Bahn-Station *Mundsburg* aus bin ich über die große Straßenkreuzung beim *Mundsburg-Center* gegangen. Direkt auf der Kreuzung befindet sich ein Mahnmal, das an die 370 Menschen erinnern soll, die am 30. Juli 1943 im bombardierten Karstadt-Bunker in der Hamburger Straße umkamen.

{{< image src="stadtteile/barmbek-sued-01" alt="Die drei Hochhäuser des Mundsburg-Centers" width="1125" height="1500" caption="Die drei Hochhäuser des Mundsburg-Centers." >}}

{{< image src="stadtteile/barmbek-sued-03" alt="Mahnmal vor dem Mundsburg-Center" width="1000" caption="Mahnmal vor dem Mundsburg-Center für die 370 Menschen, die am 30. Juli 1943 im Karstadt-Bunker durch einen Bombenangriff ums Leben kamen." >}}

{{< image src="stadtteile/barmbek-sued-02" alt="U-Bahn-Station Mundsburg" width="1000" caption="U-Bahn-Station Mundsburg, die noch in Uhlenhorst liegt. Aber schon ab der Straße beginnt Barmbek-Süd." >}}

Ich bin ins Wohngebiet hinter der *Hamburger Meile* eingebogen und nach Norden gegangen. Viel roter Backsteinbau, dazwischen aber auch ältere Gebäude mit viel Stuck und Verzierungen.

Dann wieder nach Süden und vorbei am *Alten Schützenhof* (1867), in dem der Berufskriminelle *Adolf Petersen* (*Lord von Barmbeck*) Anfang des 20. Jahrhunderts eine Zeit lang eine Kneipe, die ein Treffpunkt für Verbrecher war. Später gab er seine Kneipe auf und widmete sich ganz dem Safe-Knacken und seinen Raubzügen, die ihm immer mal wieder Gefängnisaufenthalte einbrachten, einmal sogar 12 Jahre. Als er 1933 erneut eingesperrt wurde, erhängte er sich in der Zelle.

{{< image src="stadtteile/barmbek-sued-04" alt="Alter Schützenhof" width="1000" >}}

Ich bin weiter hinter der Hamburger Meile entlang gegangen, am Bartholomäusbad vorbei.

{{< image src="stadtteile/barmbek-sued-05" alt="Bartholomäusbad" width="1125" height="1500" >}}

In der Gegend stehen sehr schöne Altbauten mit Stuck und viel Schmuck an den Fassaden.

{{< image src="stadtteile/barmbek-sued-06" alt="Altbauten" width="1125" height="1500" caption="Schöne Gebäude mit viel Stuck, hier in der Bauweise „Hamburger Burg“." >}}

{{< image src="stadtteile/barmbek-sued-07" alt="Feuerwache Barmbek" width="1125" height="1500" >}}

Ich bin durch den Johannes-Prassek-Park bei Alster-City gegangen und habe von einer Brücke über dem Osterbekkanal auf teure Penthouse-Wohnungen geschaut.

{{< image src="stadtteile/barmbek-sued-14" alt="Osterbekkanal" width="1000" >}}

{{< image src="stadtteile/barmbek-sued-13" alt="Osterbekkanal" width="1125" height="1500" >}}

{{< image src="stadtteile/barmbek-sued-12" alt="Penthouse-Wohnungen am Osterbekkanal" width="1000" >}}

{{< image src="stadtteile/barmbek-sued-11" alt="Penthouse-Wohnungen am Osterbekkanal" width="1000" >}}

Ich habe mir die *Alster-City* einmal von der Nähe angesehen und bin dann durch das Wohngebiet direkt daneben gegangen, was außerordentlich ruhig und idyllisch ist.

{{< image src="stadtteile/barmbek-sued-17" alt="Schöne Wohngebiete rund um Alster-Nord, ruhig, modern und grün." width="1000" caption="Schöne Wohngebiete rund um Alster-Nord, ruhig, modern und grün." >}}

{{< image src="stadtteile/barmbek-sued-09" alt="Schöne Wohngebiete rund um Alster-Nord, ruhig, modern und grün." width="1000" caption="Schöne Wohngebiete rund um Alster-Nord, ruhig, modern und grün." >}}

{{< image src="stadtteile/barmbek-sued-08" alt="Alster-City" width="1000" >}}

{{< image src="stadtteile/barmbek-sued-16" alt="Schöne Wohngebiete rund um Alster-Nord, ruhig, modern und grün." width="1000" caption="Schöne Wohngebiete rund um Alster-Nord, ruhig, modern und grün." >}}

{{< image src="stadtteile/barmbek-sued-15" alt="Schöne Wohngebiete rund um Alster-Nord, ruhig, modern und grün." width="1000" caption="Schöne Wohngebiete rund um Alster-Nord, ruhig, modern und grün." >}}

{{< image src="stadtteile/barmbek-sued-10" alt="Alster-City" width="1000" >}}

Am *Amtsgericht Hamburg-Barmbek* vorbei, wieder nach Norden und dann auf dem *Biedermannplatz* nach Süden gegangen. Dann immer mal wieder nach Norden und Süden. An der Kreuzung *Barmbeker Markt* und *Hamburger Straße* bin ich in die *Reesestraße* nach Norden gegangen bis zum Kanal.

{{< image src="stadtteile/barmbek-sued-18" alt="Theater am Biedermannplatz" width="1125" height="1500" >}}

{{< image src="stadtteile/barmbek-sued-19" alt="Altbauten in Barmbek-Süd" width="1125" height="1500" >}}

{{< image src="stadtteile/barmbek-sued-20" alt="Kreuzung" width="1000" >}}

{{< image src="stadtteile/barmbek-sued-22" alt="Barmbeker Markt" width="1000" >}}

{{< image src="stadtteile/barmbek-sued-21" alt="U-Bahn-Station Dehnhaide" width="1125" height="1500" >}}

Weiter auf der Straße *Flachsland*, dort konnte ich einen Blick auf die *T.R.U.D.E.* (Tief runter unter die Elbe) auf der anderen Seite des Kanals werfen, die im Hof des *Museums für Arbeit* steht. Sie ist der ehemals größte Bohrkopf der Welt, mit dem die Röhren des Elbtunnels gegraben wurden. Über die *Bramfelder Straße* und wieder nach Süden, sehr lange bis über den *Eilbekkanal* und wieder nach Norden.

{{< image src="stadtteile/barmbek-sued-23" alt="Blick auf die T.R.U.D.E." width="1000" >}}

{{< image src="stadtteile/barmbek-sued-24" alt="Theater an der Marschnerstrasse" width="1125" height="1500" >}}

{{< image src="stadtteile/barmbek-sued-25" alt="Eilbekkanal" width="1000" >}}

Am *Hansa-Kolleg* vorbei und dann durch einen Park entlang der *Wandse*. Einmal um die riesige *Schön-Klinik*, einem Krankenhaus mit 700 Betten und 1500 Angestellten, weiter nach Norden.

{{< image src="stadtteile/barmbek-sued-26" alt="Hansa-Kolleg" width="1000" >}}

{{< image src="stadtteile/barmbek-sued-27" alt="Schön-Klinik" width="1000" >}}

{{< image src="stadtteile/barmbek-sued-29" alt="Schön-Klinik" width="1000" >}}

{{< image src="stadtteile/barmbek-sued-28" alt="Schön-Klinik" width="1000" >}}

Dann vorbei am *Denkmal für die verhungerten Barmbeker und Flüchtlinge der Französischen Belagerung Hamburgs*. Zum Schluss entlang der Straße *Dehnhaide* bis zur U-Bahn *Dehnhaide*.

{{< image src="stadtteile/barmbek-sued-30" alt="Denkmal für die verhungerten Barmbeker und Flüchtlinge der Französischen Belagerung Hamburgs" width="1125" height="1500" >}}

{{< image src="map/barmbek-sued" alt="Barmbek-Süd" width="1000" height="956" >}}

<iframe class="map" src="https://www.google.com/maps/d/u/0/embed?mid=1Be0ZZcIQaxNoPTXlfab19m-Fj20" width="1000" height="500">
</iframe>
