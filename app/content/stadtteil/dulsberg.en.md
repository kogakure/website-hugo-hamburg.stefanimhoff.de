---
title: Dulsberg
slug: dulsberg
author: Stefan Imhoff
date: 2015-06-25T18:00:00+02:00
distance: 6
duration: 1:14
---

From the subway station *Strassburger Straße* I started and hiked through Dulsberg-Süd.

The buildings are mad almost only from red brick from the 20s (rebuilt, the districts was completely destroyed by Operation Gomorrah). Dulsberg is exceptionally well connected for its size. There are two subway stations and one suburban train station.

{{< image src="stadtteile/dulsberg-01" alt="Building in Dulsberg" width="1000" >}}

{{< image src="stadtteile/dulsberg-03" alt="Building in Dulsberg" width="1000" >}}

{{< image src="stadtteile/dulsberg-04" alt="Building in Dulsberg" width="1000" >}}

Many Muslims and Africans live here. Dulsberg is one of the poorest districts of Hamburg and had until 2003 the highest crime rate. The residential area is very noisy in some places, since it is intersected by Friedrich-Ebert-Damm and Ring 2. And if you live inside, you can hear noise from football fields.

In Nord-Dulsberg there is an elongated park, which is very beautiful and also quieter, because the houses all around catch the noise.

{{< image src="stadtteile/dulsberg-02" alt="Park in Dulsberg" width="1000" >}}

I passed a meeting place for Turkish men, where about 20 men sat in a room around a table and talked or played.

There is the *Linne-Kampfbahn*, a sports field with a red surface. Along the Old Teichweg I walked past *Sportpark Dulsberg* (*Olympic base Hamburg*) with the *Beach Center*.

{{< image src="stadtteile/dulsberg-08" alt="Olympic base Hamburg in Dulsberg" width="1000" >}}

{{< image src="stadtteile/dulsberg-07" alt="Olympic base Hamburg in Dulsberg" width="1000" >}}

{{< image src="stadtteile/dulsberg-06" alt="Olympic base Hamburg in Dulsberg" width="1000" >}}

{{< image src="stadtteile/dulsberg-05" alt="Olympic base Hamburg in Dulsberg" width="1000" >}}

{{< image src="map/dulsberg" alt="Dulsberg" width="1000" height="956" >}}

<iframe class="map" src="https://www.google.com/maps/d/u/0/embed?mid=1AnMqAxAuujp1SmoHMmTPpfvPCfQ" width="1000" height="500">
</iframe>
