---
title: St. Georg
slug: st-georg
author: Stefan Imhoff
date: 2015-10-11T18:00:00+02:00
distance: 12
duration: 3:03
---

From the main train station I went past the *Museum für Kunst und Gewerbe* to the south, through the *Kurt-Schuchermacher-Allee*, past the *Besenbinderhof* and then the *Norderstraße* along the tracks of the railway. I passed by the *Agentur für Arbeit*, where there is a huge metal artwork. In the *Kurt Schumacher House* the SPD Hamburg has its seat, which is located in *St. Georg* has been the strongest party for years.

{{< image src="stadtteile/st-georg-01" alt="Museum für Kunst und Gewerbe" width="1125" height="1500" >}}

{{< image src="stadtteile/st-georg-02" alt="Museum für Kunst und Gewerbe" width="1125" height="1500" >}}

{{< image src="stadtteile/st-georg-03" alt="Graffiti" width="1000" >}}

{{< image src="stadtteile/st-georg-04" alt="Pedestrian tunnel to Hammerbrook" width="1125" height="1500" >}}

{{< image src="stadtteile/st-georg-05" alt="Along the tracks" width="1000" >}}

{{< image src="stadtteile/st-georg-06" alt="Bundesagentur für Arbeit" width="1125" height="1500" >}}

{{< image src="stadtteile/st-georg-07" alt="Bundesagentur für Arbeit" width="1125" height="1500" >}}

{{< image src="stadtteile/st-georg-08" alt="Memorial" width="1125" height="1500" >}}

{{< image src="stadtteile/st-georg-10" alt="Kurt-Schumacher-Haus" width="1000" >}}

Continue through the *Jürgen-W-Scheutzow-Park* to the S-Bahn station *Berliner Tor*, from where the roof of the *Berliner Bogen* was visible. I walked between the skyscrapers of the *Hamburger Business Center* at *Berliner Tor*, where big companies like Siemens, IBM or Commerzbank have their offices.

{{< image src="stadtteile/st-georg-11" alt="Jürgen-W-Scheutzow-Park" width="1125" height="1500" >}}

{{< image src="stadtteile/st-georg-12" alt="S-Bahn station Berliner Tor with a view of the Berliner Bogen" width="1000" >}}

{{< image src="stadtteile/st-georg-13" alt="Hamburger Business Center" width="1000" >}}

{{< image src="stadtteile/st-georg-14" alt="Hamburger Business Center" width="1000" >}}

{{< image src="stadtteile/st-georg-15" alt="Hamburger Business Center" width="1125" height="1500" >}}

Past the *Studierendenwerk Hamburg* and the *Hochschule für Angewandte Wissenschaften Hamburg* (HAW) and then straight to the southwest to *Lindenstraße*. Already from a distance I could see the minarets of the *Centrum-Mosque* Hamburg, which were painted in 2009 as part of an art project with green and white hexagons. Continue through the *Böckmannstraße*, past the mosque and into the *Kleine Pulverteich*, where the *Al Nour Mosque* stands.

{{< image src="stadtteile/st-georg-16" alt="Studierendenwerk Hamburg" width="1000" caption="Studierendenwerk Hamburg" >}}

{{< image src="stadtteile/st-georg-17" alt="Hochschule für Angewandte Wissenschaften Hamburg (HAW)" width="1125" height="1500" caption="Hochschule für Angewandte Wissenschaften Hamburg (HAW)" >}}

{{< image src="stadtteile/st-georg-18" alt="Stift" width="1125" height="1500" >}}

{{< image src="stadtteile/st-georg-19" alt="Stift" width="1125" height="1500" >}}

{{< image src="stadtteile/st-georg-20" alt="Graffiti" width="1000" >}}

{{< image src="stadtteile/st-georg-21" alt="Centrum-Moschee Hamburg" width="1125" height="1500" >}}

{{< image src="stadtteile/st-georg-22" alt="Centrum-Moschee Hamburg" width="1125" height="1500" >}}

*St. Georg* is a neighborhood of contrasts: only a few meters away from the women's entrance of the mosque are gay and lesbian bars and pubs. The further you move north, the hipper gets the district. In *Steindamm* there are countless Arabic, Turkish and Indian restaurants; Here you will find the most delicious kebab of the city. But there are also numerous erotic shops, sex clubs, numerous hotels and the cinema *Savoy*, in which many films run in the original language.

*St. Georg* has some of the most beautiful façades in late-Classicist style, especially around the *Hansaplatz*, *Steinplatz*, *Brennerstraße* and *Lange Reihe*. From *Hansaplatz* I went through the *Brennerstraße*, the *Lohmühlenpark* to the large *Asklepios Klinik St. Georg*. *St. Georg* owes its name to a leprosy hospital founded in 1194 outside the walls of Hamburg, which was dedicated to *Saint George*. The hospital stays true to this tradition. There are also numerous pens in *St. Georg*, founded by wealthy merchants for the needy. I went over the clinic area to the northwest to the Alster.

{{< image src="stadtteile/st-georg-23" alt="The beautiful façades of St. George in late neoclassical style" width="1125" height="1500" >}}

{{< image src="stadtteile/st-georg-24" alt="The beautiful façades of St. George in late neoclassical style" width="1000" >}}

{{< image src="stadtteile/st-georg-25" alt="The beautiful façades of St. George in late neoclassical style" width="1125" height="1500" >}}

{{< image src="stadtteile/st-georg-26" alt="St. Georg" width="1000" >}}

{{< image src="stadtteile/st-georg-27" alt="The beautiful façades of St. George in late neoclassical style" width="1125" height="1500" >}}

{{< image src="stadtteile/st-georg-28" alt="The beautiful façades of St. George in late neoclassical style" width="1125" height="1500" >}}

{{< image src="stadtteile/st-georg-29" alt="The beautiful façades of St. George in late neoclassical style" width="1125" height="1500" >}}

{{< image src="stadtteile/st-georg-30" alt="The beautiful façades of St. George in late neoclassical style" width="1125" height="1500" >}}

{{< image src="stadtteile/st-georg-31" alt="The beautiful façades of St. George in late neoclassical style" width="1000" >}}

{{< image src="stadtteile/st-georg-32" alt="The beautiful façades of St. George in late neoclassical style" width="1125" height="1500" >}}

{{< image src="stadtteile/st-georg-33" alt="Modern buildings in St. Georg" width="1000" >}}

{{< image src="stadtteile/st-georg-34" alt="The beautiful façades of St. George in late neoclassical style" width="1125" height="1500" >}}

{{< image src="stadtteile/st-georg-35" alt="The beautiful façades of St. George in late neoclassical style" width="1000" >}}

{{< image src="stadtteile/st-georg-36" alt="The beautiful façades of St. George in late neoclassical style" width="1125" height="1500" >}}

{{< image src="stadtteile/st-georg-37" alt="The beautiful façades of St. George in late neoclassical style" width="1000" >}}

{{< image src="stadtteile/st-georg-38" alt="The beautiful façades of St. George in late neoclassical style" width="1125" height="1500" >}}

{{< image src="stadtteile/st-georg-39" alt="Fountain" width="1125" height="1500" >}}

{{< image src="stadtteile/st-georg-40" alt="Stift" width="1000" >}}

{{< image src="stadtteile/st-georg-41" alt="Stift" width="1125" height="1500" >}}

From there along the shore, past restaurants and sailing schools to *Hotel Atlantic*. Then the *Holzdamm* down to the main station. Here you will find *Ohnsorg-Theater* and *Deutsche Schauspielhaus*. Along the station to the south - here also start the city tours by bus - then through the *Steintorweg* back to the north.

{{< image src="stadtteile/st-georg-42" alt="Park" width="1000" >}}

{{< image src="stadtteile/st-georg-43" alt="Asklepios Klinik St. Georg" width="1125" height="1500" >}}

{{< image src="stadtteile/st-georg-44" alt="Modern architecture at the Alster" width="1000" >}}

{{< image src="stadtteile/st-georg-45" alt="Modern architecture at the Alster" width="1000" >}}

{{< image src="stadtteile/st-georg-46" alt="The beautiful façades of St. George in late neoclassical style" width="1000" >}}

{{< image src="stadtteile/st-georg-47" alt="Hotel Atlantic" width="1000" >}}

{{< image src="stadtteile/st-georg-48" alt="Marina on the Alster" width="1000" >}}

{{< image src="stadtteile/st-georg-49" alt="Marina on the Alster" width="1000" >}}

{{< image src="stadtteile/st-georg-50" alt="Marina on the Alster" width="1000" >}}

{{< image src="stadtteile/st-georg-51" alt="Marina on the Alster" width="1000" height="458" >}}

{{< image src="stadtteile/st-georg-52" alt="Main station" width="1000" >}}

{{< image src="stadtteile/st-georg-53" alt="The beautiful façades of St. George in late neoclassical style" width="1000" >}}

{{< image src="stadtteile/st-georg-54" alt="Deutsches Schauspielhaus" width="1000" >}}

{{< image src="stadtteile/st-georg-55" alt="Deutsches Schauspielhaus" width="1000" >}}

{{< image src="stadtteile/st-georg-56" alt="The beautiful façades of St. George in late neoclassical style" width="1125" height="1500" >}}

Around the *Trinity Church* and the whole street *Koppel* along. Back through the *Lange Reihe*. Since 1998 it is again chic in *St. Georg*, especially around the *Lange Reihe*, rents have risen sharply. In the street you will find many interesting shops, crafts, art, bars and restaurants.

{{< image src="stadtteile/st-georg-57" alt="Dreieinigkeitskirche" width="1125" height="1500" caption="Dreieinigkeitskirche" >}}

{{< image src="stadtteile/st-georg-58" alt="Dreieinigkeitskirche" width="1125" height="1500" >}}

{{< image src="stadtteile/st-georg-59" alt="Building in St. Georg" width="1125" height="1500" >}}

{{< image src="stadtteile/st-georg-60" alt="Building in St. Georg" width="1125" height="1500" >}}

{{< image src="stadtteile/st-georg-61" alt="Building in St. Georg" width="1125" height="1500" >}}

{{< image src="stadtteile/st-georg-62" alt="Building in St. Georg" width="1125" height="1500" >}}

{{< image src="stadtteile/st-georg-63" alt="Building in St. Georg" width="1000" >}}

{{< image src="stadtteile/st-georg-64" alt="Building in St. Georg" width="1125" height="1500" >}}

{{< image src="stadtteile/st-georg-65" alt="Building in St. Georg" width="1000" >}}

{{< image src="stadtteile/st-georg-66" alt="Building in St. Georg" width="1000" >}}

{{< image src="stadtteile/st-georg-67" alt="Building in St. Georg" width="1125" height="1500" >}}

{{< image src="stadtteile/st-georg-68" alt="Building in St. Georg" width="1000" >}}

{{< image src="stadtteile/st-georg-69" alt="Building in St. Georg" width="1000" >}}

{{< image src="stadtteile/st-georg-70" alt="Building in St. Georg" width="1125" height="1500" >}}

This is where multicultural diversity is lived out: lesbian and gay couples strolling hand in hand, fully-tattooed-full-bearded fathers with several children, skaters with headphones, heavily veiled Muslim women. Only a *weapons* that prohibits pistols, knives, baseball bats and pepper spray, suggests that things may not always be peaceful here at night.

{{< image src="stadtteile/st-georg-71" alt="Waffen-Verboten-Schild" width="1125" height="1500" >}}

Through *Kirchweg* and *Rostocker Straße* I went on and past the new *Mariendom*, where a monument to *St. Ansgar* stands. After a few more streets my tour ended at the subway station *Lohmühlenstraße*.

{{< image src="stadtteile/st-georg-72" alt="Mariendom" width="1125" height="1500" >}}

{{< image src="stadtteile/st-georg-73" alt="Memorial for St. Ansgar at the Mariendom" width="1125" height="1500" caption="Denkmal für St. Ansgar" >}}

{{< image src="stadtteile/st-georg-74" alt="Memorial" width="1000" >}}

{{< image src="map/st-georg" alt="St. Georg" width="1000" height="956" >}}

<iframe class="map" src="https://www.google.com/maps/d/u/0/embed?mid=1G9X5uJngjJHg0tdY-ina7vWCCn4" width="1000" height="500"></iframe>
