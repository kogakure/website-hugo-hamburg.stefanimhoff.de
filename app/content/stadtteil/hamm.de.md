---
title: Hamm
slug: hamm
author: Stefan Imhoff
date: 2015-09-06T18:00:00+02:00
distance: 23
duration: 4:48
---

Meine Tour durch *Hamm* war die längste, die ich bisher gemacht habe. *Hamm* besteht aus drei Teilen: Nord, Mitte und Süd. Ich habe meine Tour an der U-Bahn-Station *Burgstraße* begonnen und mir zuerst den Norden angeschaut.

Der Großteil von *Hamm* wurde durch die *Operation Gomorrha* 1943 zerstört, weshalb es nur hin- und wieder Häuser im alten Stil gibt. Ansonsten gibt es viel Klinker- und Backsteinbauweise, aber auch verputzte Häuser. Viele Wohnungen haben wirklich kleine Fenster und ich konnte öfter sehen, dass dort bei Sonnenschein Licht angeschaltet war.

{{< image src="stadtteile/hamm-01" alt="Gebäude in Hamm" width="1000" height="1334" >}}

{{< image src="stadtteile/hamm-02" alt="Gebäude in Hamm" width="1000" >}}

{{< image src="stadtteile/hamm-03" alt="Gebäude in Hamm" width="1000" height="1334" >}}

{{< image src="stadtteile/hamm-04" alt="Gebäude in Hamm" width="1000" >}}

{{< image src="stadtteile/hamm-05" alt="Gebäude in Hamm" width="1000" height="1334" >}}

{{< image src="stadtteile/hamm-06" alt="Gebäude in Hamm" width="1000" height="1334" >}}

{{< image src="stadtteile/hamm-07" alt="Gebäude in Hamm" width="1000" >}}

Nach den ersten Siedlungen im Nordwesten bin ich durch den *Thörls-Park* gegangen, der Größtenteils ein langer, grüner Streifen ist, der entlang der Gleise führt.

{{< image src="stadtteile/hamm-08" alt="Thörls-Park" width="1000" >}}

{{< image src="stadtteile/hamm-09" alt="Bahngleise beim Thörls-Park" width="1000" height="1334" >}}

{{< image src="stadtteile/hamm-10" alt="Thörls-Park" width="1000" >}}

{{< image src="stadtteile/hamm-11" alt="Park" width="1000" height="1334" >}}

{{< image src="stadtteile/hamm-12" alt="Plastik" width="1000" height="1334" >}}

Vor der *Dreifaltigkeitskirche* bin ich nach Norden gegangen und dann durch den *Hammer Park* spaziert. Neben einem größeren Teich gibt es auch einen schönen Blumen- und Heckengarten, einen Spielplatz, mehrere Wiesen, einen Minigolfplatz und Tischtennisplatten. Direkt neben dem Kinderspielplatz steht ein Gedenkstein für *Joachim Heinrich Campe*.

{{< image src="stadtteile/hamm-13" alt="Hammer Park" width="1000" caption="Hammer Park" >}}

{{< image src="stadtteile/hamm-14" alt="Hammer Park" width="1000" >}}

{{< image src="stadtteile/hamm-15" alt="Hammer Park" width="1000" height="1334" >}}

{{< image src="stadtteile/hamm-16" alt="Hammer Park" width="1000" height="466" >}}

{{< image src="stadtteile/hamm-17" alt="Hammer Park" width="1000" >}}

{{< image src="stadtteile/hamm-18" alt="Hammer Park" width="1000" >}}

{{< image src="stadtteile/hamm-19" alt="Hammer Park" width="1000" >}}

{{< image src="stadtteile/hamm-20" alt="Gedenksein für J. H. Campe im Hammer Park" width="1000" height="1334" >}}

{{< image src="stadtteile/hamm-21" alt="Hammer Park" width="1000" height="1334" >}}

{{< image src="stadtteile/hamm-22" alt="Hammer Park" width="1000" height="1334" >}}

{{< image src="stadtteile/hamm-23" alt="Hammer Park" width="1000" height="1334" >}}

{{< image src="stadtteile/hamm-24" alt="Hammer Park" width="1000" height="1334" >}}

Anschließend bin ich durch die Wohngebiete nördlich des Parks gegangen und durch den Kleingartenverein *Hammer Hof*. Von da aus wieder südlich bis zur *Dreifaltigkeitskirche* mit dem historischen, denkmalgeschützten Friedhof. Neben einem Mahnmal für den Krieg steht dort auch ein Denkmal für die Gefallenen des 1. Weltkrieges und das *Sieveking-Mausoleum*.

{{< image src="stadtteile/hamm-25" alt="Kleingartenverein Hammer Hof" width="1000" caption="Kleingartenverein <em>Hammer Hof</em>" >}}

{{< image src="stadtteile/hamm-26" alt="Dreifaltigkeitskirche" width="1000" height="1334" caption="Dreifaltigkeitskirche" >}}

{{< image src="stadtteile/hamm-27" alt="Dreifaltigkeitskirche" width="1000" height="1334" >}}

{{< image src="stadtteile/hamm-28" alt="Dreifaltigkeitskirche" width="1000" height="1334" >}}

{{< image src="stadtteile/hamm-29" alt="Dreifaltigkeitskirche mit dem historischen, denkmalgeschützten Friedhof" width="1000" height="1334" >}}

{{< image src="stadtteile/hamm-30" alt="Sieveking-Mausoleum" width="1000" caption="Sieveking-Mausoleum" >}}

{{< image src="stadtteile/hamm-31" alt="Sieveking-Mausoleum" width="1000" >}}

{{< image src="stadtteile/hamm-32" alt="Dreifaltigkeitskirche mit dem historischen, denkmalgeschützten Friedhof" width="1000" >}}

{{< image src="stadtteile/hamm-33" alt="Dreifaltigkeitskirche mit dem historischen, denkmalgeschützten Friedhof" width="1000" >}}

{{< image src="stadtteile/hamm-34" alt="Dreifaltigkeitskirche mit dem historischen, denkmalgeschützten Friedhof" width="1000" >}}

Ich bin weiter durch den Park südlich der *Dreifaltigkeitskirche* gegangen und an der *Hammer Landstraße* entlang bis zur U-Bahn-Station *Rauhes Haus*. Weiter nach Süden und den *Droopweg* entlang. Die *Döhnerstraße* hinunter, die ihren Namen aber nicht vom Döner bekommen hat, sondern von *Friedrich Adolf Döhner*, einem Kaufmann und Politiker. In *Hamm* Leben wohl ziemlich viele Muslime, denn mir sind eine Menge Frauen mit Kopftüchern begegnet.

{{< image src="stadtteile/hamm-35" alt="U-Bahn-Station Rauhes Haus" width="1000" >}}

Das Wohnen in *Hamm* ist nichts für Personen, die Ruhe brauchen, da das Lärmpensum besonders entlang der *Eiffelstraße* enorm ist. Zusätzlich liegt Hamm direkt in der Einflugschneise des Flughafens und die Jets überqueren den Stadtteil sehr häufig in ziemlichem Tiefflug.

Ich habe mir die Wohngebiete in *Hamm-Mitte* in einem großen Bogen angesehen und bin dann entlang des *Freibads Aschberg* nach Süden gegangen. Dort gibt es ein Tierheim und das Gelände des Polizeihundsportvereins. Südlich entlang der *Bille* gibt es nur noch große Kleingartenverein-Flächen, ebenso gegenüber auf der *Billerhuder Insel* (die zu *Rothenburgsort* gehört). Unzählige Jachten liegen hier an den Ufern vor Anker und Familien fahren mit ihren Booten auf den Kanälen herum.

{{< image src="stadtteile/hamm-36" alt="Industriegebäude in Hamm" width="1000" height="1334" >}}

{{< image src="stadtteile/hamm-37" alt="Kanäle in Hamm" width="1000" >}}

{{< image src="stadtteile/hamm-38" alt="Kanäle in Hamm" width="1000" >}}

{{< image src="stadtteile/hamm-39" alt="Bille bei Hamm" width="1000" >}}

{{< image src="stadtteile/hamm-40" alt="Bille bei Hamm" width="1000" >}}

{{< image src="stadtteile/hamm-41" alt="Bille bei Hamm" width="1000" >}}

{{< image src="stadtteile/hamm-42" alt="Bille bei Hamm" width="1000" height="1334" >}}

{{< image src="stadtteile/hamm-43" alt="Bille bei Hamm" width="1000" height="1334" >}}

{{< image src="stadtteile/hamm-44" alt="Braune Brücke" width="1000" >}}

Über den *Rückerskanal* bin ich nach Westen gegangen, wo es überwiegend Industrie- und Gewerbeflächen gibt. Weiter nach Süden über den Südkanal um die mit Kanonen und einem goldenen Schiff verzierte Säule vor dem *Störtebeker-Haus*. Anschließend nach Osten die *Süderstraße* entlang vorbei am *Elbschloss an der Bille*. Entlang der *Bille* stehen einige schöne Neubauten mit Wasserblick.

{{< image src="stadtteile/hamm-45" alt="Störtebeker-Haus" width="1000" height="1334" >}}

{{< image src="stadtteile/hamm-46" alt="Störtebeker-Haus" width="1000" height="1334" >}}

{{< image src="stadtteile/hamm-47" alt="Störtebeker-Haus" width="1000" height="1334" >}}

{{< image src="stadtteile/hamm-48" alt="Störtebeker-Haus" width="1000" height="1334" >}}

{{< image src="stadtteile/hamm-49" alt="Störtebeker-Haus" width="1000" height="1334" >}}

{{< image src="stadtteile/hamm-50" alt="Störtebeker-Haus" width="1000" height="1334" >}}

{{< image src="stadtteile/hamm-51" alt="Elbschloss an der Bille" width="1000" >}}

{{< image src="stadtteile/hamm-52" alt="Elbschloss an der Bille" width="1000" height="1334" >}}

{{< image src="stadtteile/hamm-53" alt="Häuser an der Bille" width="1000" >}}

{{< image src="stadtteile/hamm-54" alt="Häuser an der Bille" width="1000" >}}

{{< image src="stadtteile/hamm-55" alt="Häuser an der Bille" width="1000" height="1334" >}}

{{< image src="stadtteile/hamm-56" alt="Häuser an der Bille" width="1000" height="273" caption="Häuser an der Bille" >}}

{{< image src="stadtteile/hamm-57" alt="Boote am Ufer der Bille" width="1000" >}}

{{< image src="stadtteile/hamm-58" alt="Boote am Ufer der Bille" width="1000" >}}

Im Westlichen Teil von *Hamm-Süd* gibt es ausschließlich Gewerbe. Zahlreiche Lagerhallen, Druckereien, Segelclubs und ein riesiges Gelände des FKK-Nachtclubs *Babylon*. Von dort aus bin ich wieder nach Norden gegangen, über *Südkanal* und *Mittelkanal*, dann die *Eiffelstraße* entlang. Meine Tour endete wieder an der U-Bahn-Station *Burgstraße*.

{{< image src="map/hamm" alt="Hamm" width="1000" height="956" >}}

<iframe class="map" src="https://www.google.com/maps/d/u/0/embed?mid=1lFkhA9I9nRWtbpbqIQbtFf5_nGw" width="1000" height="500"></iframe>
