---
title: Borgfelde
slug: borgfelde
author: Stefan Imhoff
date: 2015-08-28T18:00:00+02:00
distance: 6
duration: 1:23
---

*Borgfelde* is one of the smallest districts in Hamburg. I started my tour at the subway station *Burgstraße* and went first through the *upper Borgfelde*.

{{< image src="stadtteile/borgfelde-01" alt="Building in Borgfelde" width="1000" height="1334" >}}

{{< image src="stadtteile/borgfelde-02" alt="Church in Borgfelde" width="1000" height="1334" >}}

*Borgfelde* is divided into *upper* and *lower Borgfelde*. The wealthy citizens used to live in beautiful houses on the hillside, and the workers lived in the lower *Borgfelde*. In between there is a steep slope called *Geesthang*.

{{< image src="stadtteile/borgfelde-03" alt="Park at Geethang" width="1000" >}}

Since large parts of *Borgfelde* have fallen victim to the bombs, there are hardly any older houses, mostly 4-5 storey buildings of the postwar period. In the *upper Borgfelde* in front of the *Geesthang* there is a long park in which the sculpture *Drei Vogelsäulen für Borgfelde* by *Klaus Becker* stands. There are a number of benches along the park path, but the noise of *Borgfelder Straße* makes it no pleasure to sit here.

{{< image src="stadtteile/borgfelde-04" alt="Park at Geesthang" width="1000" >}}

{{< image src="stadtteile/borgfelde-05" alt="Sculpture &quot;Drei Vogelsäulen für Borgfelde&quot; by Klaus Becker" width="1000" height="1334" caption="Scuplture <em>Drei Vogelsäulen für Borgfelde</em> by Klaus Becker" >}}

On the wall of the *Erlöserkirche* in the *Jungestraße* there is the war victims memorial of *Hans Kock*.

{{< image src="stadtteile/borgfelde-06" alt="Erlöserkirche" width="1000" height="1334" caption="Erlöserkirche" >}}

{{< image src="stadtteile/borgfelde-07" alt="War Victim Memorial by Hans Kock" width="1000" caption="War Victim Memorial by Hans Kock" >}}

After walking through the streets of *upper Borgfelde* I descended via the only stone staircase on *Geesthang* to the lower part of the district. There are hardly any residential buildings left, but mostly commercial ones: many car dealerships, banks, office buildings, several schools, some offices and city buildings and the HCAT (*Hamburg Center of Aviation Training*).

{{< image src="stadtteile/borgfelde-08" alt="Borgfelde" width="1000" height="1334" >}}

{{< image src="stadtteile/borgfelde-09" alt="Borgfelde" width="1000" height="1334" >}}

{{< image src="stadtteile/borgfelde-10" alt="Borgfelde" width="1000" height="1334" >}}

{{< image src="stadtteile/borgfelde-11" alt="The stairs from the upper Borgfelde in the lower Borgfelde" width="1000" caption="The stairs from the upper Borgfelde in the lower Borgfelde" >}}

{{< image src="stadtteile/borgfelde-12" alt="Unten Borgfelde and the Geesthang" width="1000" >}}

{{< image src="stadtteile/borgfelde-13" alt="HCAT (Hamburg Centre of Aviation Training)" width="1000" >}}

{{< image src="stadtteile/borgfelde-14" alt="Unten Borgfelde" width="1000" height="1334" >}}

{{< image src="stadtteile/borgfelde-15" alt="Unten Borgfelde" width="1000" >}}

I walked down the *Anckelmannstraße* and then along the *Eiffelstraße* and then over the *central canal* and through the *Wendenstraße*. Several people have fished on the shore in front of the Education and Training Center.

{{< image src="stadtteile/borgfelde-20" alt="Central chanel in Borgfelde" width="1000" caption="Central chanel in Borgfelde" >}}

{{< image src="stadtteile/borgfelde-21" alt="Central chanel in Borgfelde" width="1000" >}}

{{< image src="stadtteile/borgfelde-22" alt="Central canal in Borgfelde" width="1000" height="1334" >}}

{{< image src="stadtteile/borgfelde-23" alt="Canal in Borgfelde" width="1000" >}}

{{< image src="stadtteile/borgfelde-24" alt="Central canal in Borgfelde" width="1000" >}}

The *lower Borgfelde* is dead in the evening, except for a few songs from a building near the canal was no trace of people to find. I passed a Muslim center and a few orphaned commercial estates, searching in vain for tenants.

{{< image src="stadtteile/borgfelde-16" alt="Commercial areas in Unten Borgelde" width="1000" height="1334" >}}

{{< image src="stadtteile/borgfelde-17" alt="Commercial areas in Unten Borgelde" width="1000" >}}

{{< image src="stadtteile/borgfelde-18" alt="Commercial areas in Unten Borgelde" width="1000" height="1334" >}}

{{< image src="stadtteile/borgfelde-19" alt="Commercial areas in Unten Borgelde" width="1000" >}}

Directly close to the S-Bahn and U-Bahn station *Berliner Tor* there is a park and there is a round bunker from the war. It is not nice here, but the noise of the cars that drive from the *Elbbrücken* to Hamburg and the numerous dirt and garbage only make sure that you only want to leave the park quickly. At the subway *Berliner Tor* ended my tour of *Borgfelde*.

{{< image src="stadtteile/borgfelde-25" alt="Near the Berlin gate" width="1000" >}}

{{< image src="stadtteile/borgfelde-27" alt="Park with round bunker near the U-Bahn and S-Bahn station Berliner Tor" width="1000" height="1334" caption="Park with round bunker near the U-Bahn and S-Bahn station Berliner Tor" >}}

{{< image src="stadtteile/borgfelde-28" alt="Park near the U-Bahn and S-Bahn station Berliner Tor" width="1000" >}}

{{< image src="stadtteile/borgfelde-26" alt="Park near the U-Bahn and S-Bahn station Berliner Tor" width="1000" >}}

{{< image src="map/borgfelde" alt="Borgfelde" width="1000" height="956" >}}

<iframe class="map" src="https://www.google.com/maps/d/u/0/embed?mid=1RpVtrvZpzpLrLvZACEnKWzRdyng" width="1000" height="500"></iframe>
