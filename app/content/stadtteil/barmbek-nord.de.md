---
title: Barmbek-Nord
slug: barmbek-nord
author: Stefan Imhoff
date: 2015-07-18T18:00:00+02:00
distance: 17
duration: 3:36
---

Vom *Bahnhof Barmbek*, wo ein *Rundbunker* aus dem Zweiten Weltkrieg steht, bin ich hinter *Globetrotter* und am *Museum der Arbeit* vorbei bis zum *Osterbekkanal* gegangen.

{{< image src="stadtteile/barmbek-nord-01" alt="Rundbunker aus dem Zweiten Weltkrieg" width="1000" height="1334" >}}

{{< image src="stadtteile/barmbek-nord-04" alt="Globetrotter" width="1000" >}}

{{< image src="stadtteile/barmbek-nord-03" alt="Museum der Arbeit" width="1000" >}}

{{< image src="stadtteile/barmbek-nord-02" alt="Backsteinbau" width="1000" height="1334" >}}

Auf dem *Hof des Museums* steht die *T.R.U.D.E.*, der Bohrkopf des Elbtunnelbohrers, der die 4. Elbtunnelröhre gebohrt hat. Er hat beeindruckende 14,2 Meter im Durchmesser, wiegt 380 Tonnen und war von Oktober 1997 bis März 2000 im Einsatz.

{{< image src="stadtteile/barmbek-nord-11" alt="Tunnelbohrer T.R.U.D.E." width="1000" height="1334" >}}

{{< image src="stadtteile/barmbek-nord-10" alt="Tunnelbohrer T.R.U.D.E." width="1000" >}}

{{< image src="stadtteile/barmbek-nord-09" alt="Tunnelbohrer T.R.U.D.E." width="1000" >}}

{{< image src="stadtteile/barmbek-nord-08" alt="Tunnelbohrer T.R.U.D.E." width="1000" >}}

Ich bin weiter am Kanal entlang gegangen und habe mir dann die Wohngebiete südlich der Gleise angesehen. Wie überall in Barmbek-Nord ist alles in roten Backstein gebaut, z. T. wirklich sehr hübsch und auch in verschiedensten Rottönen und Mustern, so dass kein Einheitsbrei entsteht.

{{< image src="stadtteile/barmbek-nord-21" alt="Wohnungen in Barmbek-Nord" width="1000" height="1334" >}}

{{< image src="stadtteile/barmbek-nord-20" alt="Wohnungen in Barmbek-Nord" width="1000" height="1334" >}}

{{< image src="stadtteile/barmbek-nord-19" alt="Wohnungen in Barmbek-Nord" width="1000" >}}

{{< image src="stadtteile/barmbek-nord-18" alt="Wohnungen in Barmbek-Nord" width="1000" height="1334" >}}

{{< image src="stadtteile/barmbek-nord-17" alt="Wohnungen in Barmbek-Nord" width="1000" height="1334" >}}

{{< image src="stadtteile/barmbek-nord-16" alt="Wohnungen in Barmbek-Nord" width="1000" >}}

{{< image src="stadtteile/barmbek-nord-15" alt="Wohnungen in Barmbek-Nord" width="1000" >}}

{{< image src="stadtteile/barmbek-nord-14" alt="Wohnungen in Barmbek-Nord" width="1000" height="1334" >}}

{{< image src="stadtteile/barmbek-nord-13" alt="Wohnungen in Barmbek-Nord" width="1000" >}}

{{< image src="stadtteile/barmbek-nord-12" alt="Wohnungen in Barmbek-Nord" width="1000" >}}

Dann bin ich unter den Gleisen hindurchgegangen und durch die *Drosselgasse*, wo direkt vor dem Bahnhof kräftig gebaut wird. Der komplette Bereich soll umgebaut werden und Barmbek-Nord aufwerten. Neben einem riesigen Einkaufszentrum und Gewerbegebäude entstehen dort auch Eigentumswohnungen.

{{< image src="stadtteile/barmbek-nord-22" alt="Baustelle" width="1000" >}}

Ich bin nach Süden gegangen, wieder unter den Gleisen hindurch und am Gelände von *CrossFit HH* vorbeigekommen, wo mir auch gleich eine Gruppe von muskelbepackten Männern im Dauerlauf entgegen kam um auf das Gelände ein zu biegen. Ich schätze mal, das war das Aufwärmen.

{{< image src="stadtteile/barmbek-nord-23" alt="Graffiti" width="1000" >}}

Dann bin ich durch die Wohngebiete östlich der *Bramfelder Straße* gegangen und durch einen Kleingartenverein hinter der ehemaligen *Margarine-Fabrik Voss*, wo jetzt die *Techniker Krankenkasse* sitzt.

{{< image src="stadtteile/barmbek-nord-24" alt="Ehemalige Margarinefabrik Voss" width="1000" >}}

{{< image src="stadtteile/barmbek-nord-27" alt="Ehemalige Margarinefabrik Voss" width="1000" >}}

{{< image src="stadtteile/barmbek-nord-26" alt="Ehemalige Margarinefabrik Voss" width="1000" >}}

{{< image src="stadtteile/barmbek-nord-25" alt="Ehemalige Margarinefabrik Voss" width="1000" >}}

Die *Bramfelder Straße* hinunter, nach Norden zur *Hellbrookstraße* und wieder nach Süden zur Drosselstraße. Weiter durch die Wohngebiete und unter den Gleisen im Norden, am *Jim Block Barmbek* vorbei. Auf der *Fuhlsbüttler Straße* gibt es eine Menge Geschäfte.

{{< image src="stadtteile/barmbek-nord-28" alt="U-Bahn-Station Habichtstraße" width="1000" >}}

{{< image src="stadtteile/barmbek-nord-33" alt="Gewerbegebäude" width="1000" >}}

{{< image src="stadtteile/barmbek-nord-32" alt="Barmbeker-Ring-Brücke" width="1000" >}}

{{< image src="stadtteile/barmbek-nord-31" alt="Kreuzung" width="1000" >}}

{{< image src="stadtteile/barmbek-nord-30" alt="Schule mit hübschen Türen" width="1000" >}}

{{< image src="stadtteile/barmbek-nord-29" alt="Schule mit hübschen Türen" width="1000" >}}

Weiter nach Osten, dann nach Norden bis zu einem großen Park. Immer mal wieder nach Süden und Norden und am *Bürgerhaus Barmbek* vorbei, wo die Säulen und Geländer eines Gebäudes völlig mit Strickmustern zugedeckt sind.

{{< image src="stadtteile/barmbek-nord-34" alt="Das Bürgerhaus Barmbek" width="1000" >}}

{{< image src="stadtteile/barmbek-nord-35" alt="Säulen und Geländer mit Strickmustern verziert." width="1000" >}}

An der *Barmbeker-Ring-Brücke*, wo sich mehrere Straßen kreuzen bin ich nach Westen gegangen, dann nach Norden an der Stadtteilschule Barmbek vorbei.

Südlich der *Asklepios Kliniken* und westlich des *Wasserturm Palais* befindet sich ein wirklich schönes Wohngebiet, äußerst ruhig mit klassischen, denkmalgeschützten Gebäuden. Wer dort Wohnen möchte, muss aber tief in die Tasche greifen (5 Zimmer, 2 Bäder, 160m² für ungefähr 2.500 € im Monat).

{{< image src="stadtteile/barmbek-nord-36" alt="Wasserturm Palais" width="1000" height="1334" >}}

{{< image src="stadtteile/barmbek-nord-37" alt="Wasserturm Palais" width="1000" >}}

{{< image src="stadtteile/barmbek-nord-39" alt="Wasserturm Palais" width="1000" >}}

{{< image src="stadtteile/barmbek-nord-38" alt="Wasserturm Palais" width="1000" height="1334" >}}

{{< image src="stadtteile/barmbek-nord-45" alt="Wohngebiet" width="1000" height="1334" >}}

{{< image src="stadtteile/barmbek-nord-44" alt="Wohngebiet" width="1000" >}}

{{< image src="stadtteile/barmbek-nord-46" alt="Wohngebiet" width="1000" >}}

{{< image src="stadtteile/barmbek-nord-42" alt="Wohngebiet" width="1000" height="1334" >}}

Westlich der Klinik befindet sich die *S-Bahn-Station Rübenkamp*, wo ich meine Tour beendet habe.

{{< image src="stadtteile/barmbek-nord-47" alt="Asklepios Kliniken" width="1000" >}}

{{< image src="stadtteile/barmbek-nord-48" alt="S-Bahn-Station Rübenkamp" width="1000" >}}

{{< image src="map/barmbek-nord" alt="Barmbek-Nord" width="1000" height="956" >}}

<iframe class="map" src="https://www.google.com/maps/d/u/0/embed?mid=1yyBpChXiQJCHMY7Q8DfQbNAat5I" width="1000" height="500">
</iframe>
