---
title: Hamm
slug: hamm
author: Stefan Imhoff
date: 2015-09-06T18:00:00+02:00
distance: 23
duration: 4:48
---

My tour through *Hamm* was the longest I have done so far. *Hamm* consists of three parts: North, Middle and South. I started my tour at the subway station *Burgstraße* and first looked at the north.

The majority of *Hamm* was destroyed by *Operation Gomorrah* in 1943, so there are only occasionally old-style houses here and there. Besides that there are a lot of brick buildings, but also plastered houses. Many apartments have really small windows and I could often see that the light was switched on during bright daylight.

{{< image src="stadtteile/hamm-01" alt="Buildings in Hamm" width="1000" height="1334" >}}

{{< image src="stadtteile/hamm-02" alt="Buildings in Hamm" width="1000" >}}

{{< image src="stadtteile/hamm-03" alt="Buildings in Hamm" width="1000" height="1334" >}}

{{< image src="stadtteile/hamm-04" alt="Buildings in Hamm" width="1000" >}}

{{< image src="stadtteile/hamm-05" alt="Buildings in Hamm" width="1000" height="1334" >}}

{{< image src="stadtteile/hamm-06" alt="Buildings in Hamm" width="1000" height="1334" >}}

{{< image src="stadtteile/hamm-07" alt="Buildings in Hamm" width="1000" >}}

After walking past the first buildings in the northwest I went through the *Thörls Park*, which for the most part is a long, green strip that runs along the tracks.

{{< image src="stadtteile/hamm-08" alt="Thörls Park" width="1000" >}}

{{< image src="stadtteile/hamm-09" alt="Railway tracks at Thörls Park" width="1000" height="1334" >}}

{{< image src="stadtteile/hamm-10" alt="Thörls Park" width="1000" >}}

{{< image src="stadtteile/hamm-11" alt="Park" width="1000" height="1334" >}}

{{< image src="stadtteile/hamm-12" alt="Scupture" width="1000" height="1334" >}}

From the *Dreifaltigkeitskirche* I went north and then walked through the *Hammer Park*. In addition to a larger pond, there is also a beautiful flower and hedge garden, a playground, several meadows, a mini golf course and table tennis. Next to the children’s playground is a memorial stone for *Joachim Heinrich Campe*.

{{< image src="stadtteile/hamm-13" alt="Hammer Park" width="1000" caption="Hammer Park" >}}

{{< image src="stadtteile/hamm-14" alt="Hammer Park" width="1000" >}}

{{< image src="stadtteile/hamm-15" alt="Hammer Park" width="1000" height="1334" >}}

{{< image src="stadtteile/hamm-16" alt="Hammer Park" width="1000" height="466" >}}

{{< image src="stadtteile/hamm-17" alt="Hammer Park" width="1000" >}}

{{< image src="stadtteile/hamm-18" alt="Hammer Park" width="1000" >}}

{{< image src="stadtteile/hamm-19" alt="Hammer Park" width="1000" >}}

{{< image src="stadtteile/hamm-20" alt="Commemoration for J. H. Campe in Hammer Park" width="1000" height="1334" >}}

{{< image src="stadtteile/hamm-21" alt="Hammer Park" width="1000" height="1334" >}}

{{< image src="stadtteile/hamm-22" alt="Hammer Park" width="1000" height="1334" >}}

{{< image src="stadtteile/hamm-23" alt="Hammer Park" width="1000" height="1334" >}}

{{< image src="stadtteile/hamm-24" alt="Hammer Park" width="1000" height="1334" >}}

Then I went through the residential areas north of the park and through the allotment garden *Hammer Hof*. From there again south to the *Dreifaltigkeitskirche* with the historic, listed cemetery. In addition to a memorial for the war there is also a memorial for the fallen of the First World War and the *Sieveking Mausoleum*.

{{< image src="stadtteile/hamm-25" alt="Allotment garden Hammer Hof" width="1000" caption="Allotment garden <em>Hammer Hof</em>" >}}

{{< image src="stadtteile/hamm-26" alt="Dreifaltigkeitskirche" width="1000" height="1334" caption="Dreifaltigkeitskirche" >}}

{{< image src="stadtteile/hamm-27" alt="Dreifaltigkeitskirche" width="1000" height="1334" >}}

{{< image src="stadtteile/hamm-28" alt="Dreifaltigkeitskirche" width="1000" height="1334" >}}

{{< image src="stadtteile/hamm-29" alt="Dreifaltigkeitskirche mit dem historischen, denkmalgeschützten Friedhof" width="1000" height="1334" >}}

{{< image src="stadtteile/hamm-30" alt="Sieveking Mausoleum" width="1000" caption="Sieveking-Mausoleum" >}}

{{< image src="stadtteile/hamm-31" alt="Sieveking Mausoleum" width="1000" >}}

{{< image src="stadtteile/hamm-32" alt="Dreifaltigkeitskirche with the historic, listed cemetery" width="1000" >}}

{{< image src="stadtteile/hamm-33" alt="Dreifaltigkeitskirche with the historic, listed cemetery" width="1000" >}}

{{< image src="stadtteile/hamm-34" alt="Dreifaltigkeitskirche with the historic, listed cemetery" width="1000" >}}

I went on through the park south of the *Dreifaltigkeitskirche* and along the *Hammer Landstraße* to the subway station *Rauhes Haus*. Continue south and the *Droopweg* along. Down the *Döhnerstraße*, which did not get its name from the Döner kebab, but from *Friedrich Adolf Döhner*, a merchant and politician. In *Hamm* live probably quite a lot of Muslims, because I have met a lot of women with headscarves.

{{< image src="stadtteile/hamm-35" alt="U-Bahn-Station Rauhes Haus" width="1000" >}}

Living in *Hamm* is not for people who need peace, because the noise level, especially along the *Eiffelstraße* is enormous. In addition, Hamm is right in the approach path of the airport and the jets cross the district very often in fairly low altitude flight.

I walked the residential areas in *Hamm-Mitte* in a large arc and then went along the *Freibads Aschberg* south. There is a shelter and the properties of the police dog sports club. South along the *Bille* there are only large allotment garden areas, as well as on the *Billerhuder Insel* (which belongs to *Rothenburgsort*). Countless yachts are anchored on the banks here, and families drive their boats around the canals.

{{< image src="stadtteile/hamm-36" alt="Industrial building in Hamm" width="1000" height="1334" >}}

{{< image src="stadtteile/hamm-37" alt="Channels in Hamm" width="1000" >}}

{{< image src="stadtteile/hamm-38" alt="Channels in Hamm" width="1000" >}}

{{< image src="stadtteile/hamm-39" alt="Bille at Hamm" width="1000" >}}

{{< image src="stadtteile/hamm-40" alt="Bille at Hamm" width="1000" >}}

{{< image src="stadtteile/hamm-41" alt="Bille at Hamm" width="1000" >}}

{{< image src="stadtteile/hamm-42" alt="Bille at Hamm" width="1000" height="1334" >}}

{{< image src="stadtteile/hamm-43" alt="Bille at Hamm" width="1000" height="1334" >}}

{{< image src="stadtteile/hamm-44" alt="Braune Brücke" width="1000" >}}

On the *Rückerskanal* I went west, where there are mainly industrial and commercial areas. Further south via the south channel around the pillar decorated with cannons and a golden ship in front of the *Störtebeker-Haus*. Then to the east the *Süderstraße* along past the *Elbschloss an der Bille*. Along the *Bille* are some beautiful new buildings with water views.

{{< image src="stadtteile/hamm-45" alt="Störtebeker-Haus" width="1000" height="1334" >}}

{{< image src="stadtteile/hamm-46" alt="Störtebeker-Haus" width="1000" height="1334" >}}

{{< image src="stadtteile/hamm-47" alt="Störtebeker-Haus" width="1000" height="1334" >}}

{{< image src="stadtteile/hamm-48" alt="Störtebeker-Haus" width="1000" height="1334" >}}

{{< image src="stadtteile/hamm-49" alt="Störtebeker-Haus" width="1000" height="1334" >}}

{{< image src="stadtteile/hamm-50" alt="Störtebeker-Haus" width="1000" height="1334" >}}

{{< image src="stadtteile/hamm-51" alt="Elbschloss at the Bille" width="1000" >}}

{{< image src="stadtteile/hamm-52" alt="Elbschloss at the Bille" width="1000" height="1334" >}}

{{< image src="stadtteile/hamm-53" alt="Häuser at the Bille" width="1000" >}}

{{< image src="stadtteile/hamm-54" alt="Häuser at the Bille" width="1000" >}}

{{< image src="stadtteile/hamm-55" alt="Häuser at the Bille" width="1000" height="1334" >}}

{{< image src="stadtteile/hamm-56" alt="Houses at the Bille" width="1000" height="273" caption="Häuser an der Bille" >}}

{{< image src="stadtteile/hamm-57" alt="Boats on the banks of the Bille" width="1000" >}}

{{< image src="stadtteile/hamm-58" alt="Boats on the banks of the Bille" width="1000" >}}

In the western part of *Hamm-Süd* there are only commercial areas. Numerous warehouses, printshops, sailing clubs and a huge area of nudist nightclub *Babylon*. From there I went back north, over *south channel* and *middle channel*, then the *Eiffelstraße* along. My tour ended again at the subway station *Burgstraße*.

{{< image src="map/hamm" alt="Hamm" width="1000" height="956" >}}

<iframe class="map" src="https://www.google.com/maps/d/u/0/embed?mid=1lFkhA9I9nRWtbpbqIQbtFf5_nGw" width="1000" height="500"></iframe>
