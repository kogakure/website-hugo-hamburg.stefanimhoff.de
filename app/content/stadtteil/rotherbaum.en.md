---
title: Rotherbaum
slug: rotherbaum
author: Stefan Imhoff
date: 2016-10-22T18:00:00+02:00
distance: 16
duration: 4:15
---

From *Dammtorbahnhof* I went along the tracks *Alsterglacis* down to the Alster and then along the banks of the Outer Alster along. There is an Italian restaurant and two rowing clubs, most of which are built on the water. Past the US consulate that is called inofficially *Little White House at the Alster*. The streets around the consulate are not passable and heavy steel columns and high steel fences prevent uninvited guests from reaching the grounds by car. Moreover, it is constantly guarded by several German police officers.

{{< image src="stadtteile/rotherbaum-01" alt="Rotherbaum" width="1000" height="667" >}}

{{< image src="stadtteile/rotherbaum-02" alt="Rotherbaum" width="1000" height="1500" >}}

{{< image src="stadtteile/rotherbaum-03" alt="Rotherbaum" width="1000" height="667" >}}

{{< image src="stadtteile/rotherbaum-04" alt="Rotherbaum" width="1000" height="1500" >}}

{{< image src="stadtteile/rotherbaum-05" alt="Rotherbaum" width="1000" height="667" >}}

At Mittelweg I passed by the *Max Planck Institute for Comparative and International Private Law* and then at the *Garden House Fontenay*, where today the Consulate of Egypt is located.

{{< image src="stadtteile/rotherbaum-06" alt="Max Planck Institute for Comparative and International Private Law" width="1000" height="1500" >}}

{{< image src="stadtteile/rotherbaum-07" alt="Garden House Fontenay" caption="Garden House Fontenay" width="1000" height="667" >}}

Through the Fontenay, past the still under construction *The Fontenay Hotel*, which has a wavy shape similar to the *Hamburger Welle*, I went again direction Alster and continue along the shore to the north.

{{< image src="stadtteile/rotherbaum-08" alt="The Fontenay Hotel" caption="The Fontenay Hotel" width="1000" height="667" >}}

{{< image src="stadtteile/rotherbaum-09" alt="Rotherbaum" width="1000" height="667" >}}

{{< image src="stadtteile/rotherbaum-10" alt="Rotherbaum" width="1000" height="1500" >}}

{{< image src="stadtteile/rotherbaum-11" alt="Rotherbaum" width="1000" height="1500" >}}

{{< image src="stadtteile/rotherbaum-12" alt="Rotherbaum" width="1000" height="667" >}}

{{< image src="stadtteile/rotherbaum-13" alt="Rotherbaum" width="1000" height="1500" >}}

{{< image src="stadtteile/rotherbaum-14" alt="Rotherbaum" width="1000" height="667" >}}

{{< image src="stadtteile/rotherbaum-15" alt="Rotherbaum" width="1000" height="667" >}}

{{< image src="stadtteile/rotherbaum-16" alt="Rotherbaum" width="1000" height="1500" >}}

Many houses in Rotherbaum are extremely beautiful and well maintained. In the Milchstraße is the *Villa Beit*, which looks almost like a small castle. There are numerous consulates in this district.

I went further south to the Park Moorweide and around the Hotel Grand Elysée Hamburg.

{{< image src="stadtteile/rotherbaum-17" alt="Villa Beit" caption="Villa Beit" width="1000" height="667" >}}

{{< image src="stadtteile/rotherbaum-18" alt="Villa Beit" width="1000" height="1500" >}}

{{< image src="stadtteile/rotherbaum-19" alt="Rotherbaum" width="1000" height="1500" >}}

In Feldbrunnenstrasse there is an extraordinary building: The Chinese Teahouse *Yu Garden* with the typical curved roof gables, stone lion statues and round gates. From there in a curve back to the south, past the *Museum of Ethnology Hamburg*.

{{< image src="stadtteile/rotherbaum-20" alt="Chinese Teahouse Yu Garden" width="1000" height="667" >}}

{{< image src="stadtteile/rotherbaum-21" alt="Chinese Teahouse Yu Garden" width="1000" height="667" >}}

{{< image src="stadtteile/rotherbaum-22" alt="Chinese Teahouse Yu Garden" width="1000" height="1500" >}}

{{< image src="stadtteile/rotherbaum-24" alt="Church St. Johannis" caption="Church St. Johannis" width="1000" height="1500" >}}

{{< image src="stadtteile/rotherbaum-23" alt="Museum of Ethnology Hamburg" caption="Museum of Ethnology Hamburg" width="1000" height="1500" >}}

{{< image src="stadtteile/rotherbaum-25" alt="Museum of Ethnology Hamburg" caption="Museum of Ethnology Hamburg" width="1000" height="1500" >}}

{{< image src="stadtteile/rotherbaum-26" alt="Rotherbaum" width="1000" height="667" >}}

Then I walked past the main building of the *University of Hamburg* and across *Grindelviertel* and the university campus. Past the *State and University Library of Hamburg Carl von Ossietzky*, past the *Von-Melle-Park* and the *Mensa philosopher's tower*.

{{< image src="stadtteile/rotherbaum-27" alt="Main building of the University of Hamburg" width="1000" height="667" >}}

{{< image src="stadtteile/rotherbaum-28" alt="Main building of the University of Hamburg" width="1000" height="667" >}}

{{< image src="stadtteile/rotherbaum-29" alt="Rotherbaum" width="1000" height="667" >}}

{{< image src="stadtteile/rotherbaum-30" alt="Rotherbaum" width="1000" height="667" >}}

{{< image src="stadtteile/rotherbaum-31" alt="Rotherbaum" width="1000" height="666" >}}

{{< image src="stadtteile/rotherbaum-32" alt="Rotherbaum" width="1000" height="667" >}}

{{< image src="stadtteile/rotherbaum-33" alt="Rotherbaum" width="1000" height="1500" >}}

{{< image src="stadtteile/rotherbaum-34" alt="Rotherbaum" width="1000" height="667" >}}

Back to the south past the *Abaton Cinema* and then through Grindelallee, passing the police station 17, the *Zoological Museum* and the *Max Planck Institute for Meteorology*, my tour ended at the Schlump subway station.

{{< image src="stadtteile/rotherbaum-35" alt="Abaton Kino" caption="Abaton Kino" width="1000" height="1500" >}}

{{< image src="map/rotherbaum" alt="Karte Rotherbaum" width="1000" >}}

<iframe class="map" src="https://www.google.com/maps/d/u/0/embed?mid=11jjb8Lljf57P78rxMt0Er3Mn944" width="1000" height="500"></iframe>
