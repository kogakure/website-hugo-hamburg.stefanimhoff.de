---
title: Eilbek
slug: eilbek
author: Stefan Imhoff
date: 2015-07-22T18:00:00+02:00
distance: 10
duration: 2:09
---

From the subway station *Wartenau* I went south through the *Conventstraße*.

Through the *Hasselbrookstraße* past the *Fundus Theater* and then south.

Eilbek is intersected by two traffic routes. In the north, the *Wandsbeker Chaussee*, on which there is much traffic and in the south of the tracks of the S-Bahn and trains of the Deutsche Bahn. Nevertheless, it was extremely quiet south of the tracks, except for the wind, I could hear nothing. Eilbek’s flats consist mainly of brick or plaster buildings that were erected after the war, as the *Operation Gomorrah* had destroyed large parts of the district in July 1943. In between, however, there is always a house that has been spared and still has the typical Hamburg fronts with plaster and ornaments.

{{< image src="stadtteile/eilbek-01" alt="Buildings of Eilbek" width="1000" height="1709" >}}

{{< image src="stadtteile/eilbek-03" alt="Buildings of Eilbek" width="1000" height="1751" >}}

{{< image src="stadtteile/eilbek-02" alt="Buildings of Eilbek" width="1000" height="1710" >}}

{{< image src="stadtteile/eilbek-04" alt="Buildings of Eilbek" width="1000" height="1334" >}}

{{< image src="stadtteile/eilbek-05" alt="Buildings of Eilbek" width="1000" height="1334" >}}

{{< image src="stadtteile/eilbek-06" alt="Buildings of Eilbek" width="1000" height="1709" >}}

{{< image src="stadtteile/eilbek-10" alt="A passive house that depicts CO² savings on a digital display" width="1000" >}}

{{< image src="stadtteile/eilbek-11" alt="A passive house that depicts CO² savings on a digital display" width="1000" height="1334" caption="A passive house that depicts CO² savings on a digital display" >}}

{{< image src="stadtteile/eilbek-12" alt="The tracks of the railway" width="1000" caption="The tracks of the S-Bahn and Deutsche Bahn cut through Eilbek. Still, it's pretty quiet, even near the rails." >}}

{{< image src="stadtteile/eilbek-13" alt="The tracks of the railway" width="1000" >}}

{{< image src="stadtteile/eilbek-14" alt="The tracks of the railway" width="1000" >}}

{{< image src="stadtteile/eilbek-15" alt="Crossing Hammer Street / Marienthaler Straße" width="1000" height="961" caption="The intersection Hammer Street / Marienthaler Straße is a kind of center of Eilbek. There are cafes, shops and the Hasselbrook S-Bahn station." >}}

{{< image src="stadtteile/eilbek-16" alt="District school Hamburg-Mitte" width="1000" height="959" caption="The district school Hamburg-Mitte is located just east of Eilbek, right on the tracks." >}}

I walked on *Marienthaler Straße* to the intersection *Hammer Steindamm*, which is probably the center of Eilbek. There are several shops, cafes and the *Hasselbrook* S-Bahn station. Continue on the same street until the district school Hamburg-Mitte. Then again over the tracks and to the north. Past the *Factory Hasselbrook*, a restaurant with a cozy beer garden, which was also filled to the last seat.

{{< image src="stadtteile/eilbek-18" alt="Old round bunker" width="1000" height="1334" caption="Directly at the Hasselbrook S-Bahn station is an old round bunker." >}}

{{< image src="stadtteile/eilbek-19" alt="Restaurant Factory Hasselbrook" width="1000" caption="Restaurant Factory Hasselbrook." >}}

{{< image src="stadtteile/eilbek-20" alt="Restaurant Factory Hasselbrook." width="1000" >}}

At the southern end of the *Jacobipark* stands an old civil protection bunker from 1942, which is currently being rebuilt and where expensive condominiums are being built. The penthouse with over 200 m² is already sold, but there are still apartments with about 125 m² from 540.000 € to get.  The conversion is carried out by a company that specializes in concrete milling, because the bunker currently has no windows.

{{< image src="stadtteile/eilbek-21" alt="Civil Defense bunker" width="1000" >}}

{{< image src="stadtteile/eilbek-22" alt="Civil Defense bunker" width="1000" height="1334" >}}

Continue on the *Hasselbrookstraße* along the *Hasselbrook* school, which is a cultural monument and was built in 1905, through the Ritterstraße and then past the *Friedenskirche Eilbek* (1885).

{{< image src="stadtteile/eilbek-23" alt="The cultural monument school Hasselbrook" width="1000" caption="The cultural monument school Hasselbrook" >}}

{{< image src="stadtteile/eilbek-24" alt="Das Kulturdenkmal Schule Hasselbrook" width="1000" height="1334" caption="Das Kulturdenkmal Schule Hasselbrook." >}}

{{< image src="stadtteile/eilbek-27" alt="Friedenskirche Eilbek" width="1000" caption="Friedenskirche Eilbek." >}}

{{< image src="stadtteile/eilbek-26" alt="Friedenskirche Eilbek" width="1000" height="1334" caption="Friedenskirche Eilbek." >}}

{{< image src="stadtteile/eilbek-25" alt="Friedenskirche Eilbek" width="1000" height="1723" caption="Friedenskirche Eilbek." >}}

Then head north over the *Wandsbeker Chaussee*, which has plenty of shops, and a lot of cafes, restaurants and hookah lounges. North I went along the Eilbeker way, then the *Maxstraße* to the south and a while along the *Wandsbeker Chaussee*, through the *Eilbeker Bürgerpark* and then along a park strip to the east to *Peterskampweg*.

From the south I went through the *Jacobipark*, a former cemetery, which is now a park. Some larger graves were preserved as monuments. You can find graves and a family tombs with doctors, mayors or senators. There is a larger pond and a lot of people have grilled in the park, picnics or practiced slackline among the trees. In the north is the Easter church. Unfortunately, the park also seems to be a popular location for drunks who sleep on the benches.

{{< image src="stadtteile/eilbek-28" alt="Jacobipark" width="1000" caption="The Jacobipark, a former cemetery, is the destination of numerous picnic friends, barbecue fans, slackline and sports, but is also ideal for a good night’s sleep." >}}

{{< image src="stadtteile/eilbek-31" alt="Jacobipark" width="1000" >}}

{{< image src="stadtteile/eilbek-30" alt="Jacobipark" width="1000" >}}

{{< image src="stadtteile/eilbek-29" alt="Jacobipark" width="1000" height="961" >}}

{{< image src="stadtteile/eilbek-35" alt="Jacobipark" width="1000" caption="In Jacobipark some old family graves and a tomb have been preserved. Here lie known doctors, mayors, actresses or senators." >}}

{{< image src="stadtteile/eilbek-36" alt="Jacobipark" width="1000" >}}

{{< image src="stadtteile/eilbek-39" alt="Jacobipark" width="1000" >}}

{{< image src="stadtteile/eilbek-40" alt="Osterkirche" width="1000" height="1334" caption="Im Norden des Jacobiparks steht die Osterkirche." >}}

{{< image src="stadtteile/eilbek-42" alt="Osterkirche" width="1000" height="1334" >}}

{{< image src="stadtteile/eilbek-41" alt="Osterkirche" width="1000" height="1334" >}}

Once again I walked through a residential area in the north and then down the *Hammer Steindamm* and the *Pappelallee* again to the north. There are the huge, modern glass buildings of the professional association for health service and welfare, the AOK headquarters Wandsbek and the employment agency Hamburg-Wandsbek.

{{< image src="stadtteile/eilbek-43" alt="Office buildings" width="1000" caption="Near the U-Bahn Wandsbeker Chaussee there are some office buildings: Berufsgenossenschaft für Gesundheitsdienst und Wohlfahrtspflege, AOK headquarters Wandsbek and the employment agency Hamburg-Wandsbek." >}}

{{< image src="stadtteile/eilbek-45" alt="Office buildings" width="1000" height="1334" >}}

{{< image src="stadtteile/eilbek-44" alt="Office buildings" width="1000" >}}

Directly at the subway and suburban train station *Wandsbeker Chaussee* is a huge Edeka market, the only major supermarket in Eilbek (apart from a very small Edeka in *Marienthaler street*). Here ended my tour through Eilbek.

{{< image src="stadtteile/eilbek-46" alt="Edeka marktet" width="1000" height="959" caption="The Wandsbeker Chaussee is busy. There are numerous shops, restaurants and a large Edeka market." >}}

{{< image src="map/eilbek" alt="Eilbek" width="1000" height="956" >}}

<iframe class="map" src="https://www.google.com/maps/d/u/0/embed?mid=1XenP-27JCfW47s4pevgaR6qdMiU" width="1000" height="500"></iframe>
