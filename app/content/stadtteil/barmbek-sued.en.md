---
title: Barmbek-Süd
slug: barmbek-sued
author: Stefan Imhoff
date: 2015-07-11T18:00:00+02:00
distance: 16
duration: 3:40
---

From the underground station *Mundsburg* I went over the large intersection at the *Mundsburg-Center*. Directly at the intersection is a memorial commemorating the 370 people who died on July 30, 1943 in the bombed Karstadt bunker on Hamburger Straße.

{{< image src="stadtteile/barmbek-sued-01" alt="The three skyscrapers of the Mundsburg Center" width="1125" height="1500" caption="The three skyscrapers of the Mundsburg Center" >}}

{{< image src="stadtteile/barmbek-sued-03" alt="Memorial in front of the Mundsburg Center" width="1000" caption="Memorial in front of the Mundsburg Center for the 370 people who died on July 30, 1943 in Karstadt bunker by a bomb attack." >}}

{{< image src="stadtteile/barmbek-sued-02" alt="Underground station Mundsburg" width="1000" caption="Underground station Mundsburg, which is still in Uhlenhorst. But starting from the street Barmbek-Süd begins." >}}

I turned into the residential area behind the *Hamburger Meile* and went north. A lot of red brick buildings, but in between older buildings with lots of stucco and decorations.

Then walking again to the south and past the *Old Schützenhof* (1867), in which the professional criminal *Adolf Petersen* (*Lord of Barmbeck*) in the early 20th century had a pub, which was a meeting place for criminals. Later, he gave up his pub and devoted himself entirely to the safe cracking and his raids, which brought him repeatedly prison prison, even once 12 years. When he was imprisoned again in 1933, he hanged himself in the cell.

{{< image src="stadtteile/barmbek-sued-04" alt="Alter Schützenhof" width="1000" >}}

Ich bin weiter hinter der Hamburger Meile entlang gegangen, am Bartholomäusbad vorbei.

{{< image src="stadtteile/barmbek-sued-05" alt="Bartholomäusbad" width="1125" height="1500" >}}

In the area are very beautiful old buildings with stucco and a lot of jewelry on the facades.

{{< image src="stadtteile/barmbek-sued-06" alt="Old buildings" width="1125" height="1500" caption="Beautiful building with lots of stucco, here in the construction “Hamburger Burg”." >}}

{{< image src="stadtteile/barmbek-sued-07" alt="Fire station Barmbek" width="1125" height="1500" >}}

I walked through the Johannes-Prassek-Park near Alster-City and looked from a bridge over the Osterbekkanal to expensive penthouse apartments.

{{< image src="stadtteile/barmbek-sued-14" alt="Osterbekkanal" width="1000" >}}

{{< image src="stadtteile/barmbek-sued-13" alt="Osterbekkanal" width="1125" height="1500" >}}

{{< image src="stadtteile/barmbek-sued-12" alt="Penthouse apartments at the Osterbekkanal" width="1000" >}}

{{< image src="stadtteile/barmbek-sued-11" alt="Penthouse apartments at the Osterbekkanal" width="1000" >}}

I once looked at the *Alster-City* from close by and went right next to it through the residential area, which is extremely quiet and idyllic.

{{< image src="stadtteile/barmbek-sued-17" alt="Beautiful residential areas around Alster-Nord, quiet, modern and green." width="1000" caption="Beautiful residential areas around Alster-Nord, quiet, modern and green." >}}

{{< image src="stadtteile/barmbek-sued-09" alt="Beautiful residential areas around Alster-Nord, quiet, modern and green." width="1000" caption="Beautiful residential areas around Alster-Nord, quiet, modern and green." >}}

{{< image src="stadtteile/barmbek-sued-08" alt="Alster-City" width="1000" >}}

{{< image src="stadtteile/barmbek-sued-16" alt="Beautiful residential areas around Alster-Nord, quiet, modern and green." width="1000" caption="Beautiful residential areas around Alster-Nord, quiet, modern and green." >}}

{{< image src="stadtteile/barmbek-sued-15" alt="Beautiful residential areas around Alster-Nord, quiet, modern and green." width="1000" caption="Beautiful residential areas around Alster-Nord, quiet, modern and green." >}}

{{< image src="stadtteile/barmbek-sued-10" alt="Alster-City" width="1000" >}}

I walked past the *district court Hamburg-Barmbek*, back to the north and then on the *Biedermannplatz* south. Then every now and then to the north and south. At the intersection *Barmbeker Markt* and *Hamburger Straße* I went to the *Reesestraße* north to the canal.

{{< image src="stadtteile/barmbek-sued-18" alt="Theater at Biedermannplatz" width="1125" height="1500" >}}

{{< image src="stadtteile/barmbek-sued-19" alt="Old buildings in Barmbek south" width="1125" height="1500" >}}

{{< image src="stadtteile/barmbek-sued-20" alt="Crossing" width="1000" >}}

{{< image src="stadtteile/barmbek-sued-22" alt="Barmbeker Markt" width="1000" >}}

{{< image src="stadtteile/barmbek-sued-21" alt="Subway station Dehnhaide" width="1125" height="1500" >}}

I continued along on the road *Flachsland*, where I could take a look at the *T.R.U.D.E.* (Tief runter unter die Elbe, engl. deep down under the Elbe) on the other side of the canal, which stands in the courtyard of the *Museum für Arbeit*. It is the former largest boring head in the world, with which the tubes of the Elbtunnel were dug. Along the *Bramfelder Straße* and back to the south, a long time along *Eilbekkanal* and back to the north.

{{< image src="stadtteile/barmbek-sued-23" alt="View of the T.R.U.D.E." width="1000" >}}

{{< image src="stadtteile/barmbek-sued-24" alt="Theater an der Marschnerstrasse" width="1125" height="1500" >}}

{{< image src="stadtteile/barmbek-sued-25" alt="Eilbekkanal" width="1000" >}}

Past the *Hansa-Kolleg* and then through a park along the *Wandse*. Once around the huge *Schön-Klinik*, a hospital with 700 beds and 1500 employees, and further north.

{{< image src="stadtteile/barmbek-sued-26" alt="Hansa-Kolleg" width="1000" >}}

{{< image src="stadtteile/barmbek-sued-27" alt="Schön-Klinik" width="1000" >}}

{{< image src="stadtteile/barmbek-sued-29" alt="Schön-Klinik" width="1000" >}}

{{< image src="stadtteile/barmbek-sued-28" alt="Schön-Klinik" width="1000" >}}

Then past the *Monument to the starving Barmbeks and refugees of the French siege of Hamburg*. Finally along the road *Dehnhaide* to the subway *Dehnhaide*.

{{< image src="stadtteile/barmbek-sued-30" alt="Monument to the starving Barmbeks and refugees of the French siege of Hamburg" width="1125" height="1500" >}}

{{< image src="map/barmbek-sued" alt="Barmbek-Süd" width="1000" height="956" >}}

<iframe class="map" src="https://www.google.com/maps/d/u/0/embed?mid=1Be0ZZcIQaxNoPTXlfab19m-Fj20" width="1000" height="500">
</iframe>
