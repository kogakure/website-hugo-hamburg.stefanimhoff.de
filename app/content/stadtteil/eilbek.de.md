---
title: Eilbek
slug: eilbek
author: Stefan Imhoff
date: 2015-07-22T18:00:00+02:00
distance: 10
duration: 2:09
---

Von der U-Bahn-Station *Wartenau* aus bin ich durch die *Conventstraße* nach Süden gegangen.

Durch die *Hasselbrookstraße* vorbei am *Fundus Theater* und dann nach Süden.

Eilbek wird von zwei Verkehrswegen durchschnitten. Im Norden die *Wandsbeker Chaussee*, auf der viel Verkehr ist und im Süden von den Gleisen der S-Bahn und Zügen der Deutschen Bahn. Trotzdem ist es südlich der Gleise außerordentlich ruhig gewesen, bis auf den Wind konnte ich nichts hören. Eilbeks Wohnungen bestehen zum größtenteils aus Backstein oder verputzen Bauten, die nach dem Krieg entstanden sind, da die *Operation Gomorrha* im Juli 1943 große Teile des Stadtteils zerstört hatten. Zwischendurch findet sich aber immer mal wieder ein Haus, welches verschont wurde und noch die typischen Hamburger Fronten mit Putz und Verzierungen hat.

{{< image src="stadtteile/eilbek-01" alt="Bebauung von Eilbek" width="1000" height="1709" >}}

{{< image src="stadtteile/eilbek-03" alt="Bebauung von Eilbek" width="1000" height="1751" >}}

{{< image src="stadtteile/eilbek-02" alt="Bebauung von Eilbek" width="1000" height="1710" >}}

{{< image src="stadtteile/eilbek-04" alt="Bebauung von Eilbek" width="1000" height="1334" >}}

{{< image src="stadtteile/eilbek-05" alt="Bebauung von Eilbek" width="1000" height="1334" >}}

{{< image src="stadtteile/eilbek-06" alt="Bebauung von Eilbek" width="1000" height="1709" >}}

{{< image src="stadtteile/eilbek-10" alt="Ein Passivhaus, das die CO²-Einsparung auf einer digitalen Anzeige abbildet" width="1000" >}}

{{< image src="stadtteile/eilbek-11" alt="Ein Passivhaus, das die CO²-Einsparung auf einer digitalen Anzeige abbildet" width="1000" height="1334" caption="Ein Passivhaus, das die CO²-Einsparung auf einer digitalen Anzeige abbildet." >}}

{{< image src="stadtteile/eilbek-12" alt="Die Gleise der Bahn" width="1000" caption="Die Gleise der S-Bahn und Deutschen Bahn durchschneiden Eilbek. Dennoch ist es ziemlich leise, selbst nah bei den Schienen." >}}

{{< image src="stadtteile/eilbek-13" alt="Die Gleise der Bahn" width="1000" >}}

{{< image src="stadtteile/eilbek-14" alt="Die Gleise der Bahn" width="1000" >}}

{{< image src="stadtteile/eilbek-15" alt="Kreuzung Hammer Straße/Marienthaler Straße" width="1000" height="961" caption="Die Kreuzung Hammer Straße/Marienthaler Straße ist eine Art Zentrum von Eilbek. Hier gibt es Cafés, Geschäfte und die S-Bahn-Station Hasselbrook." >}}

{{< image src="stadtteile/eilbek-16" alt="Stadtteilschule Hamburg-Mitte" width="1000" height="959" caption="Die Stadtteilschule Hamburg-Mitte liegt ganz östlich von Eilbek, direkt an den Gleisen." >}}

Ich bin auf der *Marienthaler Straße* bis zur Kreuzung *Hammer Steindamm* gegangen, was wohl das Zentrum von Eilbek darstellt. Hier gibt es einige Geschäfte, Cafés und die S-Bahn-Station *Hasselbrook*. Weiter auf der gleichen Straße bis zur Stadtteilschule Hamburg-Mitte. Danach wieder über die Gleise und nach Norden. Vorbei an der *Factory Hasselbrook*, einer Gaststätte mit gemütlichem Biergarten, der auch bis auf den letzten Platz gefüllt war.

{{< image src="stadtteile/eilbek-18" alt="Alter Rundbunker" width="1000" height="1334" caption="Direkt an der S-Bahn-Station Hasselbrook steht ein alter Rundbunker." >}}

{{< image src="stadtteile/eilbek-19" alt="Gaststädte Factory Hasselbrook" width="1000" caption="Gaststädte Factory Hasselbrook." >}}

{{< image src="stadtteile/eilbek-20" alt="Gaststädte Factory Hasselbrook" width="1000" >}}

Am südlichen Ende des *Jacobipark* steht ein alter Zivilschutzbunker aus dem Jahr 1942, der derzeit umgebaut wird und in dem teure Eigentumswohnungen entstehen. Das Penthouse mit über 200 m² ist bereits verkauft, aber es sind noch Wohnungen mit ca. 125 m² ab 540.000 € zu haben … Der Umbau wird von einer Firma durchgeführt, die auf Betonfräsen spezialisiert ist, denn der Bunker hat derzeit keine Fenster.

{{< image src="stadtteile/eilbek-21" alt="Zivilschutzbunker" width="1000" >}}

{{< image src="stadtteile/eilbek-22" alt="Zivilschutzbunker" width="1000" height="1334" >}}

Weiter auf der *Hasselbrookstraße* an der *Schule Hasselbrook* vorbei, die ein Kulturdenkmal ist und 1905 gebaut wurde, durch die Ritterstraße und dann an der *Friedenskirche Eilbek* (1885) vorbei.

{{< image src="stadtteile/eilbek-23" alt="Das Kulturdenkmal Schule Hasselbrook" width="1000" caption="Das Kulturdenkmal Schule Hasselbrook." >}}

{{< image src="stadtteile/eilbek-24" alt="Das Kulturdenkmal Schule Hasselbrook" width="1000" height="1334" caption="Das Kulturdenkmal Schule Hasselbrook." >}}

{{< image src="stadtteile/eilbek-27" alt="Die Friedenskirche Eilbek" width="1000" caption="Die Friedenskirche Eilbek." >}}

{{< image src="stadtteile/eilbek-26" alt="Die Friedenskirche Eilbek" width="1000" height="1334" caption="Die Friedenskirche Eilbek." >}}

{{< image src="stadtteile/eilbek-25" alt="Die Friedenskirche Eilbek" width="1000" height="1723" caption="Die Friedenskirche Eilbek." >}}

Anschließend nach Norden über die *Wandsbeker Chaussee*, in der es zahlreiche Geschäfte gibt, auch eine Menge Cafés, Restaurants und Shisha-Lounges. Nördlich bin ich den Eilbeker Weg entlanggegangen, dann die *Maxstraße* nach Süden und eine Weile die *Wandsbeker Chaussee* entlang, durch den *Eilbeker Bürgerpark* und dann einen Parkstreifen entlang nach Osten bis zum *Peterskampweg*.

Von Süden aus bin ich durch den *Jacobipark* gegangen, ein ehemaliger Friedhof, der jetzt eine Grünanlage ist. Einige größere Gräber wurden als Denkmäler erhalten. So findet man Gräber und eine Familien-Gruft mit bedeuteten Ärzten, Bürgermeistern oder Senatoren. Es gibt einen größeren Teich und eine Menge Leute haben im Park gegrillt, Picknick gemacht oder Slackline zwischen den Bäumen geübt. Im Norden steht die Osterkirche. Leider scheint der Park auch ein beliebter Aufenthaltsort für Betrunkene zu sein, die ihren Rausch auf den Bänken ausschlafen.

{{< image src="stadtteile/eilbek-28" alt="Jacobipark" width="1000" caption="Der Jacobipark, ein ehemaliger Friedhof, ist Ziel zahlreicher Picknick-Freunde, Grill-Fans, Slackline- und Sporttreibender, eignet sich aber wohl auch hervorragend, um dort einen Rausch auszuschlafen." >}}

{{< image src="stadtteile/eilbek-31" alt="Jacobipark" width="1000" >}}

{{< image src="stadtteile/eilbek-30" alt="Jacobipark" width="1000" >}}

{{< image src="stadtteile/eilbek-29" alt="Jacobipark" width="1000" height="961" >}}

{{< image src="stadtteile/eilbek-35" alt="Jacobipark" width="1000" caption="Im Jacobipark sind einige alte Familiengräber und eine Gruft erhalten geblieben. Hier liegen bekannte Ärzte, Bürgermeister, Schauspielerinnen oder Senatoren." >}}

{{< image src="stadtteile/eilbek-36" alt="Jacobipark" width="1000" >}}

{{< image src="stadtteile/eilbek-39" alt="Jacobipark" width="1000" >}}

{{< image src="stadtteile/eilbek-40" alt="Osterkirche" width="1000" height="1334" caption="Im Norden des Jacobiparks steht die Osterkirche." >}}

{{< image src="stadtteile/eilbek-42" alt="Osterkirche" width="1000" height="1334" >}}

{{< image src="stadtteile/eilbek-41" alt="Osterkirche" width="1000" height="1334" >}}

Ich bin noch einmal im Norden durch ein Wohngebiet gegangen und dann den *Hammer Steindamm* hinunter und die *Pappelallee* wieder nach Norden. Dort stehen die riesigen, modernen Glasgebäude der Berufsgenossenschaft für Gesundheitsdienst und Wohlfahrtspflege, der AOK Hauptgeschäftsstelle Wandsbek und der Arbeitsagentur Hamburg-Wandsbek.

{{< image src="stadtteile/eilbek-43" alt="Bürogebäude" width="1000" caption="In der Nähe der U-Bahn Wandsbeker Chaussee befinden sich einige Bürogebäude: Berufsgenossenschaft für Gesundheitsdienst und Wohlfahrtspflege, AOK Hauptgeschäftsstelle Wandsbek und die Arbeitsagentur Hamburg-Wandsbek." >}}

{{< image src="stadtteile/eilbek-45" alt="Bürogebäude" width="1000" height="1334" >}}

{{< image src="stadtteile/eilbek-44" alt="Bürogebäude" width="1000" >}}

Direkt an der U-Bahn- und S-Bahn-Station *Wandsbeker Chaussee* steht ein riesiger Edeka-Markt, die einzige größere Einkaufsmöglichkeit in Eilbek (abgesehen von einem sehr kleinen Edeka in der *Marienthaler Straße*). Hier endete auch meine Tour durch Eilbek.

{{< image src="stadtteile/eilbek-46" alt="Edeka-Markt" width="1000" height="959" caption="Die Wandsbeker Chaussee ist stark befahren. Dort befinden sich zahlreiche Geschäfte, Restaurants und auch ein großer Edeka-Markt." >}}

{{< image src="map/eilbek" alt="Eilbek" width="1000" height="956" >}}

<iframe class="map" src="https://www.google.com/maps/d/u/0/embed?mid=1XenP-27JCfW47s4pevgaR6qdMiU" width="1000" height="500"></iframe>
