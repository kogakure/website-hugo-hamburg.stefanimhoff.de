---
title: Neustadt
slug: neustadt
author: Stefan Imhoff
date: 2016-09-10T18:00:00+02:00
distance: 16
duration: 4:57
---

Die *Neustadt* Hamburgs ist für mich besonders interessant gewesen, da ich den Stadtteil berufsbedingt schon gut kannte und gespannt war, ob ich noch Dinge entdecken würde, die ich nicht kenne.

Von der U-Bahnstation *Baumwall* aus bin ich direkt am Hafen gestartet, wo ich die *Elbpromenade* entlang gegangen bin und einen Abstecher über die *Überseebrücke* gemacht habe um die dort liegenden Schiffe zu fotografieren.

{{< image src="stadtteile/neustadt-01" alt="Neustadt" width="1000" height="853" >}}

{{< image src="stadtteile/neustadt-02" alt="Neustadt" width="1000" height="853" >}}

{{< image src="stadtteile/neustadt-03" alt="Neustadt" width="1000" height="854" >}}

{{< image src="stadtteile/neustadt-04" alt="Neustadt" width="1000" height="853" >}}

{{< image src="stadtteile/neustadt-05" alt="Neustadt" width="1000" height="853" >}}

{{< image src="stadtteile/neustadt-06" alt="Neustadt" width="1000" height="853" >}}

{{< image src="stadtteile/neustadt-07" alt="Neustadt" width="1000" height="853" >}}

Anschließend bin ich durch die südliche Neustadt gegangen, das sogenannte *Portugiesenviertel*, wo sich Portugiesische und Spanische Restaurants aneinanderreihen. Es gibt viele schöne Gebäude im hanseatischen Stil.

Direkt neben dem Bürokomplex von G+J befindet sich die *Michelwiese*, ein Park der sich vom Michel bis zum Hafen erstreckt.

{{< image src="stadtteile/neustadt-08" alt="Neustadt" width="1000" height="1501" >}}

Über den Venusberg bin ich am *Alten Elbpark* mit Bismarck-Denkmal vorbei Richtung *St. Michaelis* (Michel) gegangen. Da ich schon öfter auf der Spitze des Michel war, habe ich mir das dieses Mal gesparrt und stattdessen die Statuen und Kunstwerke rund um die Kirche näher angeschaut.

Nahe des Michels steht das *Denkmal der Zitronenjette*, deren Finger völlig blank gescheuert ist, da angeblich Glück bringen soll an ihrem Finger zu reiben.

{{< image src="stadtteile/neustadt-09" alt="Neustadt" width="1000" height="853" >}}

{{< image src="stadtteile/neustadt-10" alt="Neustadt" width="1000" height="853" >}}

{{< image src="stadtteile/neustadt-11" alt="Neustadt" width="1000" height="1501" >}}

{{< image src="stadtteile/neustadt-12" alt="Neustadt" width="1000" height="853" >}}

{{< image src="stadtteile/neustadt-13" alt="Neustadt" width="1000" height="1501" >}}

{{< image src="stadtteile/neustadt-14" alt="Neustadt" width="1000" height="1501" >}}

Ich bin dann am kleinen Michel vorbeigegangen und Richtung *Großneumarkt*. Mit einigen Umwegen folge danach die nördliche Neustadt mit ihren teuren Einkaufsstraßen. Hier sind alle teuren Mode- und Luxusgutmarken angesiedelt und die Sportwagendichte ist äußerst hoch. Ich bin direkt am *Alsterfleet* und den *Alsterarkaden* entlang gegangen, durch *Neuer Wall* und *Große Bleichen*. Ich habe das *Hanse-Viertel*, eine große Einkaufspassage in der Neustadt umrundet.

{{< image src="stadtteile/neustadt-15" alt="Neustadt" width="1000" height="853" >}}

{{< image src="stadtteile/neustadt-16" alt="Neustadt" width="1000" height="853" >}}

{{< image src="stadtteile/neustadt-17" alt="Neustadt" width="1000" height="1501" >}}

{{< image src="stadtteile/neustadt-18" alt="Neustadt" width="1000" height="853" >}}

{{< image src="stadtteile/neustadt-19" alt="Neustadt" width="1000" height="853" >}}

{{< image src="stadtteile/neustadt-20" alt="Neustadt" width="1000" height="1501" >}}

{{< image src="stadtteile/neustadt-21" alt="Neustadt" width="1000" height="1501" >}}

{{< image src="stadtteile/neustadt-22" alt="Neustadt" width="1000" height="1501" >}}

{{< image src="stadtteile/neustadt-23" alt="Neustadt" width="1000" height="853" >}}

{{< image src="stadtteile/neustadt-24" alt="Neustadt" width="1000" height="853" >}}

{{< image src="stadtteile/neustadt-25" alt="Neustadt" width="1000" height="853" >}}

{{< image src="stadtteile/neustadt-26" alt="Neustadt" width="1000" height="1501" >}}

{{< image src="stadtteile/neustadt-27" alt="Neustadt" width="1000" height="853" >}}

{{< image src="stadtteile/neustadt-28" alt="Neustadt" width="1000" height="1501" >}}

{{< image src="stadtteile/neustadt-29" alt="Neustadt" width="1000" height="1501" >}}

{{< image src="stadtteile/neustadt-30" alt="Neustadt" width="1000" height="1501" >}}

{{< image src="stadtteile/neustadt-31" alt="Neustadt" width="1000" height="1501" >}}

{{< image src="stadtteile/neustadt-32" alt="Neustadt" width="1000" height="1501" >}}

{{< image src="stadtteile/neustadt-33" alt="Neustadt" width="1000" height="853" >}}

{{< image src="stadtteile/neustadt-34" alt="Neustadt" width="1000" height="853" >}}

{{< image src="stadtteile/neustadt-35" alt="Neustadt" width="1000" height="1501" >}}

{{< image src="stadtteile/neustadt-36" alt="Neustadt" width="1000" height="1501" >}}

{{< image src="stadtteile/neustadt-37" alt="Neustadt" width="1000" height="853" >}}

{{< image src="stadtteile/neustadt-38" alt="Neustadt" width="1000" height="853" >}}

{{< image src="stadtteile/neustadt-39" alt="Neustadt" width="1000" height="1501" >}}

{{< image src="stadtteile/neustadt-40" alt="Neustadt" width="1000" height="853" >}}

{{< image src="stadtteile/neustadt-41" alt="Neustadt" width="1000" height="1501" >}}

Vorbei an Polizeikommissariat Hamburg-Mitte und der Axel-Springer-Passage. Auch Internet-Firmen wie Google (ABC-Straße) und Facebook (Caffamacherreihe) haben hier Büros.

{{< image src="stadtteile/neustadt-42" alt="Neustadt" width="1000" height="1501" >}}

Dann bin ich an der *Laeiszhalle* vorbei durch den Dammtorwall gegangen, über den *Gänsemarkt* und durch die *Collonaden*. Zum Abschluss meiner Tour noch einmal um das *Casino Espanade* herum und dann einmal um die *Binnenalster*. Meine Tour endete am Jungfernstieg.

{{< image src="stadtteile/neustadt-43" alt="Neustadt" width="1000" height="853" >}}

{{< image src="stadtteile/neustadt-44" alt="Neustadt" width="1000" height="1501" >}}

{{< image src="stadtteile/neustadt-45" alt="Neustadt" width="1000" height="853" >}}

{{< image src="stadtteile/neustadt-46" alt="Neustadt" width="1000" height="853" >}}

{{< image src="map/neustadt" alt="Karte Neustadt" width="1000" >}}

<iframe class="map" src="https://www.google.com/maps/d/u/0/embed?mid=1SFmPYmjakPn5W_7rWfEl1qM7YlY" width="1000" height="500"></iframe>
