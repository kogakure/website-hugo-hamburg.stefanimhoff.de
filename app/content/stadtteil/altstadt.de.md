---
title: Altstadt
slug: altstadt
author: Stefan Imhoff
date: 2016-05-07T18:00:00+02:00
distance: 18
duration: 5:17
---

Es war ein sonniger Tag mit Temperaturen über 20° C., als ich mich auf meine Tour durch die Altstadt von Hamburg aufgemacht habe. Vom *Hauptbahnhof* aus bin ich Richtung Deichtorhallen gegangen, vorbei an der *City-Hof-Passage*, einigen der ersten Hochhäusern Hamburgs, die in den 1950er-Jahren gebaut wurden. Auch wenn sie nicht wirklich eine Schönheit sind, so haben sie doch historischen Wert.

{{< image src="stadtteile/altstadt-01" alt="City-Hof-Passage" width="1000" caption="City-Hof-Passage" >}}

{{< image src="stadtteile/altstadt-02" alt="Platz vor den Deichtorhallen" width="1000" >}}

Ich bin am Ufer des Zollkanals entlang, das Spiegel-Gebäude links, bis zum *Meßberg* gegangen und von dort aus nach Norden am *Hachez Chocoversum* vorbei, das trotzt der hohen Temperaturen gut besucht war. Durch das historische *Kontorhausviertel* (welches zum UNESCO-Welterbe gehört) mit *Sprinkenhof*, *Mohlenhof*, *Chilehaus* und anderen schönen Fassaden.

{{< image src="stadtteile/altstadt-03" alt="Ufer des Zollkanals" width="1000" >}}

{{< image src="stadtteile/altstadt-04" alt="Ufer des Zollkanals" width="1000" >}}

{{< image src="stadtteile/altstadt-05" alt="Hachez Chocoversum" width="1000" >}}

{{< image src="stadtteile/altstadt-06" alt="Kontorhausviertel" width="1000" caption="Kontorhausviertel" >}}

{{< image src="stadtteile/altstadt-07" alt="Kontorhausviertel" width="1000" >}}

{{< image src="stadtteile/altstadt-08" alt="Kontorhausviertel" width="1000" >}}

{{< image src="stadtteile/altstadt-09" alt="Kontorhausviertel" width="1000" >}}

{{< image src="stadtteile/altstadt-10" alt="Kontorhausviertel: Chilehaus" width="1000" height="1334" >}}

{{< image src="stadtteile/altstadt-11" alt="Kontorhausviertel: Sprinken-Hof" width="1000" >}}

{{< image src="stadtteile/altstadt-12" alt="Kontorhausviertel: Chilehaus" width="1000" >}}

{{< image src="stadtteile/altstadt-13" alt="Kontorhausviertel: Mohlenhof" width="1000" height="1334" >}}

{{< image src="stadtteile/altstadt-14" alt="Gebäude in der Altstadt" width="1000" >}}

{{< image src="stadtteile/altstadt-15" alt="Gebäude in der Altstadt" width="1000" >}}

{{< image src="stadtteile/altstadt-16" alt="Gebäude in der Altstadt" width="1000" height="1334" >}}

Am *Domplatz*, wo sich heute ein Park mit Sitzgelegenheiten befindet, stand früher vermutlich die *Hammaburg*, die Hamburg ihren Namen gab. Es war aber weniger eine *Burg*, sondern mehr ein Erdwall mit Holzpalisaden, fünf bis sechs Meter hoch, 15 Meter breit und ungefähr 130 Meter in Länge und Breite.

Dort befindet sich auch der Sitz von *Parship*, der *Zeit* und die *Scientology Kirche Hamburg*, in deren Schaufenster *L. Ron Hubbards* Bücher ausliegen und allerlei Werbung für die Psychotests der Kirche gemacht werden. Außerdem steht dort die *Sankt Petri Kirche*, die ich später auch noch besucht habe.

{{< image src="stadtteile/altstadt-17" alt="Domplatz" width="1000" caption="Domplatz" >}}

{{< image src="stadtteile/altstadt-18" alt="Domplatz mit Blick auf Sankt Petri Kirche" width="1000" height="1334" >}}

{{< image src="stadtteile/altstadt-19" alt="Domplatz" width="1000" >}}

{{< image src="stadtteile/altstadt-20" alt="Domplatz" width="1000" >}}

Vorerst habe ich aber den Weg nach Süden eingeschlagen und die nördliche *Speicherstadt* durchwandert. Die Speicherstadt wurde 1883 gebaut. Vorbei an *Dialog im Stillen* und *Dialog im Dunkeln*, dem *Wasserschloß* und am *Holländischem Brook* und dem *Fleetschlösschen* vorbei. Auf den Kanälen waren zahlreiche Boote der Hafenrundfahrten unterwegs.

{{< image src="stadtteile/altstadt-22" alt="Speicherstadt" width="1000" >}}

{{< image src="stadtteile/altstadt-23" alt="Speicherstadt" width="1000" >}}

{{< image src="stadtteile/altstadt-24" alt="Speicherstadt" width="1000" height="1334" >}}

{{< image src="stadtteile/altstadt-25" alt="Speicherstadt" width="1000" height="1334" >}}

{{< image src="stadtteile/altstadt-26" alt="Speicherstadt" width="1000" >}}

Von dort aus bin ich über die Jungfernbrücke Richtung Norden gegangen und habe die *Kirche St. Katharinen* umrundet und durch die *Grimm* gegangen, ehe ich noch einmal in die Speicherstadt gegangen bin. Vor dem Miniatur Wunderland war es brechend voll, außerdem wurden Busladungen von Touristen zum Hafengeburtstag heran gekarrt.

{{< image src="stadtteile/altstadt-27" alt="Kirche St. Katharinen" width="1000" height="1334" caption="Kirche St. Katharinen" >}}

{{< image src="stadtteile/altstadt-28" alt="Blick auf die St. Nikolai Kirche" width="1000" >}}

{{< image src="stadtteile/altstadt-29" alt="Kirche St. Katharinen" width="1000" height="1334" >}}

{{< image src="stadtteile/altstadt-30" alt="Modernes Gebäude" width="1000" >}}

{{< image src="stadtteile/altstadt-31" alt="Speicherstadt" width="1000" >}}

{{< image src="stadtteile/altstadt-32" alt="Schiffschraube" width="1000" >}}

{{< image src="stadtteile/altstadt-33" alt="Gebäude in der Altstadt" width="1000" >}}

Nach einer Runde über die *Cremon-Insel* habe ich mir den *Nikolaifleet* angesehen, wo 1188 der Hamburger Hafen entstand. Hier ist noch einiges der althamburgischen Bebauung erhalten. Die *Deichstraße* hinauf, wo man die historischen Bürgerhäuser der Stadt ansehen kann. Hier hat auch der *Hamburger Brand* vom 5. Mai 1842 begonnen. Das Haus, in dem das Feuer ausbrach, heißt heute *Zum Brandanfang*.

{{< image src="stadtteile/altstadt-34" alt="Nikolaifleet" width="1000" caption="Nikolaifleet" >}}

{{< image src="stadtteile/altstadt-35" alt="Deichstraße" width="1000" height="1334" caption="Deichstraße" >}}

{{< image src="stadtteile/altstadt-36" alt="Deichstraße" width="1000" >}}

Von der *Steintwiete* aus bin ich dann den *Rödingsmarkt* hinuntergegangen, wo die Gleise der U-Bahn oberirdisch auf einer Brücke verlaufen. Ich habe einen Abstecher durch die Straße *Herrlichkeit* gemacht, die entlang des *Alsterfleets* verläuft. Hier stehen moderne Wohnungen mit Blick auf den Fleet.

{{< image src="stadtteile/altstadt-37" alt="Rödingsmarkt" width="1000" caption="Rödingsmarkt" >}}

{{< image src="stadtteile/altstadt-38" alt="Wohnungen am Alsterfleet" width="1000" >}}

Anschließend bin ich Richtung *St. Nikolai Kirche* gegangen, die heute ein Mahnmal gegen den Krieg ist. Das Kirchenschiff wurde im zweiten Weltkrieg völlig von Bomben zerstört, es stehen nur noch die Aussenmauern und der Turm.

{{< image src="stadtteile/altstadt-39" alt="St. Nikolai Kirche" width="1000" height="1334" >}}

{{< image src="stadtteile/altstadt-40" alt="Denkmal an der St. Nikolai Kirche" width="1000" >}}

{{< image src="stadtteile/altstadt-41" alt="St. Nikolai Kirche" width="1000" height="1334" >}}

{{< image src="stadtteile/altstadt-42" alt="St. Nikolai Kirche" width="1000" >}}

{{< image src="stadtteile/altstadt-43" alt="St. Nikolai Kirche" width="1000" >}}

{{< image src="stadtteile/altstadt-44" alt="St. Nikolai Kirche" width="1000" >}}

Über die Trostbrücke, an der alten Börse vorbei, wo bis 1842 auch das alte Rathaus stand, was vergeblich gesprengt wurde, um den *Hamburger Brand* aufzuhalten. An der *Handelskammer Hamburg* bin ich in den *Mönkedamm* abgebogen und dann über den *Alten Wall* Richtung *Rathaus* gegangen. Im Innenhof des Rathauses befindet sich ein schöner Brunnen und die Aussenbereiche es *Restaurants Parlament Hamburg*. Im Inneren des Rathauses habe ich eine kleine Pause gemacht, ehe ich weiter Richtung Sankt Petri Kirche gegangen bin.

{{< image src="stadtteile/altstadt-45" alt="Zum Alten Rathaus" width="1000" >}}

{{< image src="stadtteile/altstadt-46" alt="Handelskammer Hamburg" width="1000" >}}

{{< image src="stadtteile/altstadt-47" alt="Rathaus" width="1000" caption="Rathaus" >}}

{{< image src="stadtteile/altstadt-48" alt="Rathaus" width="1000" >}}

{{< image src="stadtteile/altstadt-49" alt="Rathaus" width="1000" height="1334" >}}

{{< image src="stadtteile/altstadt-50" alt="Rathaus" width="1000" >}}

{{< image src="stadtteile/altstadt-51" alt="Rathaus" width="1000" height="1334" >}}

{{< image src="stadtteile/altstadt-52" alt="Rathaus" width="1000" height="1334" >}}

{{< image src="stadtteile/altstadt-53" alt="Rathaus" width="1000" height="1334" >}}

{{< image src="stadtteile/altstadt-54" alt="Rathaus" width="1000" height="1334" >}}

{{< image src="stadtteile/altstadt-55" alt="Rathaus" width="1000" height="1334" >}}

{{< image src="stadtteile/altstadt-56" alt="Rathaus" width="1000" height="1334" >}}

Ich habe mich kurzerhand entschlossen den Kirchturm zu ersteigen, da dies der höchste Aussichtspunkt Hamburgs ist. Im Gegensatz zur *Kirche St. Michaelis* gibt es hier aber keinen Fahrstuhl, sondern nur eine Menge Treppenstufen, die in recht schwindelerregender Höhe verlaufen. Es gibt mehrere Etagen, in denen man aus runden Fenstern einen Blick auf die Stadt bekommen kann. Der höchste Punkt von 123 Metern ist direkt in der Spitze des Kirchturms, wo gerade noch Platz für 3-4 Personen ist. Durch das Gewicht der Besucher oder den Wind hat es auch leicht geschwankt. Definitiv nichts für Personen mit Höhenangst, doch die Aussicht ist fantastisch.

{{< image src="stadtteile/altstadt-57" alt="Scientology Kirche Hamburg" width="1000" height="1334" >}}

{{< image src="stadtteile/altstadt-58" alt="Sankt Petri Kirche" width="1000" height="1334" caption="Sankt Petri Kirche" >}}

{{< image src="stadtteile/altstadt-59" alt="Glocken in der Sankt Petri Kirche" width="1000" height="1334" >}}

{{< image src="stadtteile/altstadt-60" alt="Blick vom Kirchturm der Sankt Petri Kirche" width="1000" >}}

{{< image src="stadtteile/altstadt-61" alt="Blick vom Kirchturm der Sankt Petri Kirche" width="1000" height="1334" >}}

{{< image src="stadtteile/altstadt-62" alt="Blick vom Kirchturm der Sankt Petri Kirche" width="1000" >}}

{{< image src="stadtteile/altstadt-63" alt="Blick vom Kirchturm der Sankt Petri Kirche" width="1000" >}}

{{< image src="stadtteile/altstadt-64" alt="Blick vom Kirchturm der Sankt Petri Kirche" width="1000" >}}

{{< image src="stadtteile/altstadt-66" alt="Kirchturm der Sankt Petri Kirche" width="1000" height="1334" >}}

{{< image src="stadtteile/altstadt-65" alt="Kirchturm der Sankt Petri Kirche" width="1000" >}}

{{< image src="stadtteile/altstadt-67" alt="Kirchturm der Sankt Petri Kirche" width="1000" height="1334" >}}

Ich bin dann durch die *Mönckebergstraße* gegangen und das *Alstertor* hinauf Richtung *Binnenalster*, vorbei am *Thalia Theater*. Ich bin noch einmal Richtung Süden gegangen durch *Spitalerstraße* und um die *Kirche St. Jacobi* herum. Dann bin ich die Bugenhagenstraße entlanggegangen und habe Saturn einmal umrundet, ehe ich noch einmal die *Mönckebergstraße* und *Spitalerstraße* entlang gegangen bin. Es war unglaublich voll und laut: Peruanische Panflötenspieler, Plastikschrott-Trommler und hämmernde Rockmusik aus den Mode-Geschäften.

{{< image src="stadtteile/altstadt-68" alt="Gebäude nahe der Mönckebergstraße" width="1000" >}}

{{< image src="stadtteile/altstadt-69" alt="Thalia Theater" width="1000" >}}

{{< image src="stadtteile/altstadt-70" alt="Gebäude" width="1000" >}}

{{< image src="stadtteile/altstadt-71" alt="Elbphilharmonie-Kulturcafé" width="1000" >}}

{{< image src="stadtteile/altstadt-72" alt="Hauptbahnhof" width="1000" caption="Hauptbahnhof" >}}

{{< image src="stadtteile/altstadt-73" alt="Gebäude in Brandsende" width="1000" >}}

{{< image src="stadtteile/altstadt-74" alt="Gebäude" width="1000" >}}

Anschließend bin ich noch durch den nördlichen Teil der Altstadt gegangen und durch die Straße *Brandsende*, wo der *Hamburger Brand* sein Ende fand. Als letztes bin ich den *Glockengießerwall* hinunter zum *Hauptbahnhof* gegangen, wo meine Tour endete.

{{< image src="stadtteile/altstadt-75" alt="Glockengießerwall" width="1000" >}}

{{< image src="stadtteile/altstadt-76" alt="Glockengießerwall" width="1000" >}}

{{< image src="map/altstadt" alt="Altstadt" width="1000" >}}

<iframe class="map" src="https://www.google.com/maps/d/u/0/embed?mid=12OxD8wbv4uwG-kZjHADAHLsgvhI" width="1000" height="500"></iframe>
