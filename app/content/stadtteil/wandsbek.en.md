---
title: Wandsbek
slug: wandsbek
author: Stefan Imhoff
date: 2015-06-20T18:00:00+02:00
distance: 16
duration: 3:27
---

In the north of *Wandsbek* there are many small settlements, which are very pretty, semi-detached, everything is well-kept and clean. You can tell that the Wandsbeker are proud of their district.

{{< image src="stadtteile/wandsbek-01" alt="Wandsbeker cottage" width="1000" caption="Typical Wandsbeker cottage, well maintained, with hedge and roses in front of the house." >}}

Near the *Bundeswehr Hospital* you will find mostly higher buildings (skyscrapers), some Aldi and Lidl markets. The Bundeswehr Hospital is not accessible to private persons, it is military terrain.

Then walking south again you’ll see many small houses, nice and well maintained, continue over the Friedrich-Ebert-Damm. Past a large REWE market and then the past *Betriebshof Wandsbek* of the Hochbahn, where huge amounts of buses are parked.

Walk along the *Wandse* through a park and south to the *Staatsarchiv*, where the original documents on the town’s history and numerous files are kept.

{{< image src="stadtteile/wandsbek-02" alt="Staatsarchiv" width="1000" >}}

Right next to it is the *Jewish cemetery Wandsbek* from 1637, closed in 1886, but it can not be entered and is a memorial. On it are nothing but ancient tombstones with Hebrew signs.

{{< image src="stadtteile/wandsbek-03" alt="Jewish cemetery Wandsbek" width="1000" >}}

{{< image src="stadtteile/wandsbek-04" alt="Jewish cemetery Wandsbek" width="1000" >}}

{{< image src="stadtteile/wandsbek-05" alt="Jewish cemetery Wandsbek" width="1000" >}}

Then walk through the *shopping center Quarree*, which is really huge, it stretches over several streets and has bridges between the buildings.

{{< image src="stadtteile/wandsbek-06" alt="Shopping Center Quarree" width="1000" >}}

Continue south to *Wandsbeker Marktplatz*, where you can find two lion sculptures. They were originally located at the main entrance of the palace complex of *Heinrich Carl von Schimmelmann*. They are under protection of monuments and are today on the marketplace.

{{< image src="stadtteile/wandsbek-07" alt="wandsbek-07" width="1000" >}}

{{< image src="stadtteile/wandsbek-08" alt="wandsbek-08" width="1000" >}}

Near the former castle there are many, beautiful, old houses with stucco. The castle itself doesn’t exist no longer, there is today the city administration of Wandsbek.

{{< image src="stadtteile/wandsbek-09" alt="Nice, old stucco buildings" width="1000" >}}

In Wandsbeker wood is a *Memorial stone for Matthias Claudius*, the most famous Wandsbeker inhabitant. He wrote many poems and directed the newspaper *Wandsbeker Bote* from 1771-1775.

{{< image src="stadtteile/wandsbek-10" alt="Memorial stone for Matthias Claudius" width="1125" height="1500" >}}

Then past the *Matthias-Claudius-Gymnasium* and past the Roman Catholic *parish of St. Joseph*.

{{< image src="stadtteile/wandsbek-11" alt="Church of St. Joseph" width="1000" >}}

{{< image src="stadtteile/wandsbek-12" alt="Church of St. Joseph" width="1125" height="1500" >}}

In the Böhmerstraße is the *Heimatmuseum Wandsbek*, in a building that was founded in 1870 by the daughters of the 1784 immigrated merchant *Joseph Morewood* from England as a retirement home.

{{< image src="stadtteile/wandsbek-13" alt="Heimatmuseum Wandsbek (Morewood-Stift)" width="1000" >}}

Then walk past *Charlotte-Paulsen-Gymnasium*, which was built in 1914.

{{< image src="stadtteile/wandsbek-14" alt="Charlotte-Paulsen-Gymnasium" width="1000" >}}

{{< image src="stadtteile/wandsbek-16" alt="Charlotte-Paulsen-Gymnasium" width="1000" >}}

{{< image src="stadtteile/wandsbek-15" alt="Charlotte-Paulsen-Gymnasium" width="1125" height="1500" >}}

Next to it you’ll find the *Bovehaus*, which was built in 1861 by *Christian Bove*, who returned from Argentina after 30 years as a successful merchant.

{{< image src="stadtteile/wandsbek-17" alt="Bovehaus" width="1000" height="934" >}}

Then walk under the railway bridge and along the tracks. On the right side you’ll find quite nice settlements. On the edge of Wandsbek walk again north and on the other side along the tracks.

Then walk past *Nestlé Chocolate Factory Hamburg*, where you can smell the fantastic sweets.

{{< image src="stadtteile/wandsbek-18" alt="Nestlé Chocolate Factory Hamburg" width="1000" >}}

Walk north past Staples and Kentucky Fried Chicken. Continue through a few settlements and the commercial area, where there are a lot of car dealerships. Pass the *UCI-Wandsbek* and continue north through another commercial area, where in addition to numerous car dealerships there is also the flatrate brothel *Geizhaus*.

{{< image src="map/wandsbek" alt="Wandsbek" width="1000" height="956" >}}

<iframe class="map" src="https://www.google.com/maps/d/u/0/embed?mid=1hN1sxADZkZP93w3CgHGzvs9HoLg" width="1000" height="500">
</iframe>
