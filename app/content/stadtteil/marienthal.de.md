---
title: Marienthal
slug: marienthal
author: Stefan Imhoff
date: 2015-06-21T18:00:00+02:00
distance: 9
duration: 1:53
---

Von der U-Bahn *Wandsbeker Chaussee* bin ich nach Süden gegangen. Die *Hammer Str.* war gesperrt, weil alles neu gebaut wird.

Marienthal ist nur Wohngebiet, aber eine bessere Gegend. Es gibt viele restaurierte, wunderschöne Bauten, sauber und gepflegt. Alles ist gut gesichert, überall Alarmanlagen und Warnschilder gegen Einbrecher. Sehr viele teuere Autos in den Einfahrten. Je näher man der Autobahn kommt desto weniger schön sind die Häuser.

{{< image src="stadtteile/marienthal-01" alt="Häuser in Marienthal" width="1000" >}}

{{< image src="stadtteile/marienthal-07" alt="Häuser in Marienthal" width="1000" >}}

{{< image src="stadtteile/marienthal-06" alt="Häuser in Marienthal" width="1000" >}}

{{< image src="stadtteile/marienthal-05" alt="Häuser in Marienthal" width="1000" height="938" >}}

{{< image src="stadtteile/marienthal-02" alt="Häuser in Marienthal" width="1000" >}}

{{< image src="stadtteile/marienthal-04" alt="Häuser in Marienthal" width="1000" >}}

{{< image src="stadtteile/marienthal-03" alt="Häuser in Marienthal" width="1000" >}}

Es gibt zwei geometrische Parks mit Seen, in einem davon steht das *Husarendenkmal*.

{{< image src="stadtteile/marienthal-08" alt="Husarendenkmal" width="1125" height="1500" >}}

{{< image src="stadtteile/marienthal-09" alt="Park" width="1000" >}}

{{< image src="stadtteile/marienthal-10" alt="Park" width="1000" >}}

An der Autobahn wurde eine riesige Lärmschutzwand errichtet, die sehr hässlich ist, und laut ist es dort trotzdem. In der Nähe wurde der Siel neu gemacht, die Röhren sind riesig.

{{< image src="stadtteile/marienthal-11" alt="Lärmschutzwand" width="1000" >}}

{{< image src="stadtteile/marienthal-12" alt="Sielarbeiten" width="1000" >}}

Im *Wandsbeker Gehölz* ist es schön ruhig, es gibt mehrere Seen und Sonntags waren viele Leute unterwegs. Entlang des Waldes gibt es viele Altersheime und Seniorensitze.

Nahe des Bahnhofs Wandsbek gibt es eine bizarre Verkehrsführung: Eine Unterführung für Fußgänger, darüber die Gleise der Deutschen Bahn und darüber eine Schnellstraße.

{{< image src="map/marienthal" alt="Marienthal" width="1000" height="956" >}}

<iframe class="map" src="https://www.google.com/maps/d/u/0/embed?mid=1-lfaf5JwRwenBQ5K5bkhn2OPlmQ" width="1000" height="500">
</iframe>
