---
title: St. Georg
slug: st-georg
author: Stefan Imhoff
date: 2015-10-11T18:00:00+02:00
distance: 12
duration: 3:03
---

Vom Hauptbahnhof aus bin ich vorbei am *Museum für Kunst und Gewerbe* nach Süden gegangen, durch die *Kurt-Schuchermacher-Allee*, vorbei am *Besenbinderhof* und dann die *Norderstraße* an den Gleisen der Bahn entlang. Ich bin an der *Agentur für Arbeit* vorbeigekommen, wo ein riesiges metallenes Kunstwerk steht. Im *Kurt-Schumacher-Haus* hat die SPD Hamburg ihren Sitz, die in *St. Georg* seit Jahren die stärkste Partei ist.

{{< image src="stadtteile/st-georg-01" alt="Museum für Kunst und Gewerbe" width="1125" height="1500" >}}

{{< image src="stadtteile/st-georg-02" alt="Museum für Kunst und Gewerbe" width="1125" height="1500" >}}

{{< image src="stadtteile/st-georg-03" alt="Graffiti" width="1000" >}}

{{< image src="stadtteile/st-georg-04" alt="Fußgängertunnel nach Hammerbrook" width="1125" height="1500" >}}

{{< image src="stadtteile/st-georg-05" alt="Entlang der Gleise" width="1000" >}}

{{< image src="stadtteile/st-georg-06" alt="Bundesagentur für Arbeit" width="1125" height="1500" >}}

{{< image src="stadtteile/st-georg-07" alt="Bundesagentur für Arbeit" width="1125" height="1500" >}}

{{< image src="stadtteile/st-georg-08" alt="Denkmal" width="1125" height="1500" >}}

{{< image src="stadtteile/st-georg-10" alt="Kurt-Schumacher-Haus" width="1000" >}}

Weiter durch den *Jürgen-W-Scheutzow-Park* zum S-Bahnhof *Berliner Tor*, von wo aus das Dach des *Berliner Bogens* zu sehen war. Ich bin zwischen den Hochhäusern des *Hamburger Business Center* am *Berliner Tor* gegangen, wo große Firmen wie Siemens, IBM oder die Commerzbank ihre Büros haben.

{{< image src="stadtteile/st-georg-11" alt="Jürgen-W-Scheutzow-Park" width="1125" height="1500" >}}

{{< image src="stadtteile/st-georg-12" alt="S-Bahnhof Berliner Tor mit Blick auf den Berliner Bogen" width="1000" >}}

{{< image src="stadtteile/st-georg-13" alt="Hamburger Business Center" width="1000" >}}

{{< image src="stadtteile/st-georg-14" alt="Hamburger Business Center" width="1000" >}}

{{< image src="stadtteile/st-georg-15" alt="Hamburger Business Center" width="1125" height="1500" >}}

Vorbei am *Studierendenwerk Hamburg* und an der *Hochschule für Angewandte Wissenschaften Hamburg* (HAW) und dann gerade nach Südwesten bis zur *Lindenstraße*. Schon von weitem konnte ich die Minarette der *Centrum-Moschee* Hamburg sehen, die 2009 im Rahmen eines Kunstprojektes mit grünen und weißen Sechsecken bemalt wurden. Weiter durch die *Böckmannstraße*, vorbei an der Moschee und in den *Kleinen Pulverteich*, wo die *Al Nour Moschee* steht.

{{< image src="stadtteile/st-georg-16" alt="Studierendenwerk Hamburg" width="1000" caption="Studierendenwerk Hamburg" >}}

{{< image src="stadtteile/st-georg-17" alt="Hochschule für Angewandte Wissenschaften Hamburg (HAW)" width="1125" height="1500" caption="Hochschule für Angewandte Wissenschaften Hamburg (HAW)" >}}

{{< image src="stadtteile/st-georg-18" alt="Stift" width="1125" height="1500" >}}

{{< image src="stadtteile/st-georg-19" alt="Stift" width="1125" height="1500" >}}

{{< image src="stadtteile/st-georg-20" alt="Graffiti" width="1000" >}}

{{< image src="stadtteile/st-georg-21" alt="Centrum-Moschee Hamburg" width="1125" height="1500" >}}

{{< image src="stadtteile/st-georg-22" alt="Centrum-Moschee Hamburg" width="1125" height="1500" >}}

*St. Georg* ist ein Stadtteil der Gegensätze: nur wenige Meter entfernt vom Fraueneingang der Moschee finden sich Schwulen- und Lesbenbars und Kneipen. Je weiter man sich nach Norden bewegt, desto hipper wird der Stadtteil. Im *Steindamm* finden sich unzählige arabische, türkische und indische Restaurants; hier gibt es die leckersten Döner der Stadt. Aber ebenso finden sich hier zahlreiche Erotik-Geschäfte, Sexclubs, zahlreiche Hotels und das Kino *Savoy*, in dem viele Filme in Originalsprache laufen.

*St. Georg* hat einige der schönsten Fassaden in spätklassizistischem Stil, insbesondere rund um den *Hansaplatz*, *Steinplatz*, *Brennerstraße* und *Lange Reihe*. Vom *Hansaplatz* aus bin ich durch die *Brennerstraße*, den *Lohmühlenpark* zur großen *Asklepios Klinik St. Georg* gegangen. *St. Georg* verdankt seinen Namen einem 1194 außerhalb der Mauern Hamburgs gegründeten Lepra-Krankenhaus, was dem *Heiligen Georg* gewidmet war. Das Krankenhaus bleibt dieser Tradition also treu. Ebenso gibt es zahlreiche Stifte in *St. Georg*, die von wohlhabenden Kaufleuten für Bedürftige gegründet wurden. Ich bin über das Klinikgelände nach Nordwesten bis zur Alster gegangen.

{{< image src="stadtteile/st-georg-23" alt="Die schönen Fassaden St. Georgs im spätklassizistischen Stil" width="1125" height="1500" >}}

{{< image src="stadtteile/st-georg-24" alt="Die schönen Fassaden St. Georgs im spätklassizistischen Stil" width="1000" >}}

{{< image src="stadtteile/st-georg-25" alt="Die schönen Fassaden St. Georgs im spätklassizistischen Stil" width="1125" height="1500" >}}

{{< image src="stadtteile/st-georg-26" alt="St. Georg" width="1000" >}}

{{< image src="stadtteile/st-georg-27" alt="Die schönen Fassaden St. Georgs im spätklassizistischen Stil" width="1125" height="1500" >}}

{{< image src="stadtteile/st-georg-28" alt="Die schönen Fassaden St. Georgs im spätklassizistischen Stil" width="1125" height="1500" >}}

{{< image src="stadtteile/st-georg-29" alt="Die schönen Fassaden St. Georgs im spätklassizistischen Stil" width="1125" height="1500" >}}

{{< image src="stadtteile/st-georg-30" alt="Die schönen Fassaden St. Georgs im spätklassizistischen Stil" width="1125" height="1500" >}}

{{< image src="stadtteile/st-georg-31" alt="Die schönen Fassaden St. Georgs im spätklassizistischen Stil" width="1000" >}}

{{< image src="stadtteile/st-georg-32" alt="Die schönen Fassaden St. Georgs im spätklassizistischen Stil" width="1125" height="1500" >}}

{{< image src="stadtteile/st-georg-33" alt="Moderne Gebäude in St. Georg" width="1000" >}}

{{< image src="stadtteile/st-georg-34" alt="Die schönen Fassaden St. Georgs im spätklassizistischen Stil" width="1125" height="1500" >}}

{{< image src="stadtteile/st-georg-35" alt="Die schönen Fassaden St. Georgs im spätklassizistischen Stil" width="1000" >}}

{{< image src="stadtteile/st-georg-36" alt="Die schönen Fassaden St. Georgs im spätklassizistischen Stil" width="1125" height="1500" >}}

{{< image src="stadtteile/st-georg-37" alt="Die schönen Fassaden St. Georgs im spätklassizistischen Stil" width="1000" >}}

{{< image src="stadtteile/st-georg-38" alt="Die schönen Fassaden St. Georgs im spätklassizistischen Stil" width="1125" height="1500" >}}

{{< image src="stadtteile/st-georg-39" alt="Brunnen" width="1125" height="1500" >}}

{{< image src="stadtteile/st-georg-40" alt="Stift" width="1000" >}}

{{< image src="stadtteile/st-georg-41" alt="Stift" width="1125" height="1500" >}}

Von dort aus entlang des Ufers, an Restaurants und Segelschulen vorbei bis zum *Hotel Atlantic*. Dann den *Holzdamm* hinunter bis zum Hauptbahnhof. Hier finden sich das *Ohnsorg-Theater* und das *Deutsche Schauspielhaus*. Am Bahnhof entlang nach Süden – hier starten auch die Stadtrundfahrten mit dem Bus – dann durch den *Steintorweg* wieder nach Norden.

{{< image src="stadtteile/st-georg-42" alt="Park" width="1000" >}}

{{< image src="stadtteile/st-georg-43" alt="Asklepios Klinik St. Georg" width="1125" height="1500" >}}

{{< image src="stadtteile/st-georg-44" alt="Moderne Architektur an der Alster" width="1000" >}}

{{< image src="stadtteile/st-georg-45" alt="Moderne Architektur an der Alster" width="1000" >}}

{{< image src="stadtteile/st-georg-46" alt="Die schönen Fassaden St. Georgs im spätklassizistischen Stil" width="1000" >}}

{{< image src="stadtteile/st-georg-47" alt="Hotel Atlantic" width="1000" >}}

{{< image src="stadtteile/st-georg-48" alt="Jachthafen an der Alster" width="1000" >}}

{{< image src="stadtteile/st-georg-49" alt="Jachthafen an der Alster" width="1000" >}}

{{< image src="stadtteile/st-georg-50" alt="Jachthafen an der Alster" width="1000" >}}

{{< image src="stadtteile/st-georg-51" alt="Jachthafen an der Alster" width="1000" height="458" >}}

{{< image src="stadtteile/st-georg-52" alt="Hauptbahnhof" width="1000" >}}

{{< image src="stadtteile/st-georg-53" alt="Die schönen Fassaden St. Georgs im spätklassizistischen Stil" width="1000" >}}

{{< image src="stadtteile/st-georg-54" alt="Deutsches Schauspielhaus" width="1000" >}}

{{< image src="stadtteile/st-georg-55" alt="Deutsches Schauspielhaus" width="1000" >}}

{{< image src="stadtteile/st-georg-56" alt="Die schönen Fassaden St. Georgs im spätklassizistischen Stil" width="1125" height="1500" >}}

Um die *Dreieinigkeitskirche* herum und die ganze Straße *Koppel* entlang. Zurück durch die *Lange Reihe*. Seit 1998 ist es wieder chic in *St. Georg* zu wohnen, besonders rund um die *Lange Reihe* sind die Mietpreise kräftig gestiegen. In der Straße findet man zahlreiche interessante Geschäfte, Handwerk, Kunst, Bars und Restaurants.

{{< image src="stadtteile/st-georg-57" alt="Dreieinigkeitskirche" width="1125" height="1500" caption="Dreieinigkeitskirche" >}}

{{< image src="stadtteile/st-georg-58" alt="Dreieinigkeitskirche" width="1125" height="1500" >}}

{{< image src="stadtteile/st-georg-59" alt="Gebäude in St. Georg" width="1125" height="1500" >}}

{{< image src="stadtteile/st-georg-60" alt="Gebäude in St. Georg" width="1125" height="1500" >}}

{{< image src="stadtteile/st-georg-61" alt="Gebäude in St. Georg" width="1125" height="1500" >}}

{{< image src="stadtteile/st-georg-62" alt="Gebäude in St. Georg" width="1125" height="1500" >}}

{{< image src="stadtteile/st-georg-63" alt="Gebäude in St. Georg" width="1000" >}}

{{< image src="stadtteile/st-georg-64" alt="Gebäude in St. Georg" width="1125" height="1500" >}}

{{< image src="stadtteile/st-georg-65" alt="Gebäude in St. Georg" width="1000" >}}

{{< image src="stadtteile/st-georg-66" alt="Gebäude in St. Georg" width="1000" >}}

{{< image src="stadtteile/st-georg-67" alt="Gebäude in St. Georg" width="1125" height="1500" >}}

{{< image src="stadtteile/st-georg-68" alt="Gebäude in St. Georg" width="1000" >}}

{{< image src="stadtteile/st-georg-69" alt="Gebäude in St. Georg" width="1000" >}}

{{< image src="stadtteile/st-georg-70" alt="Gebäude in St. Georg" width="1125" height="1500" >}}

Hier wird multikulturelle Vielfalt gelebt: Neben lesbischen und schwulen Pärchen, die Hand in Hand bummeln, voll-tätowierten-voll-bärtigen Familienväter mit mehreren Kindern, Skatern mit Kopfhörern findet man ebenso stark verschleierte muslimische Frauen. Einzig ein *Waffen-verboten-Schild*, das Pistolen, Messer, Baseballschläger und Pfefferspray untersagt, lässt darauf schließen, dass es hier nachts vielleicht nicht immer so friedlich zugeht.

{{< image src="stadtteile/st-georg-71" alt="Waffen-Verboten-Schild" width="1125" height="1500" >}}

Durch *Kirchweg* und *Rostocker Straße* bin ich weitergegangen und am neuen *Mariendom* vorbei, wo ein Denkmal für *St. Ansgar* steht. Nach ein paar weiteren Straßen endete meine Tour an der U-Bahn-Station *Lohmühlenstraße*.

{{< image src="stadtteile/st-georg-72" alt="Mariendom" width="1125" height="1500" >}}

{{< image src="stadtteile/st-georg-73" alt="Denkmal für St. Ansgar am Mariendom" width="1125" height="1500" caption="Denkmal für St. Ansgar" >}}

{{< image src="stadtteile/st-georg-74" alt="Denkmal" width="1000" >}}

{{< image src="map/st-georg" alt="St. Georg" width="1000" height="956" >}}

<iframe class="map" src="https://www.google.com/maps/d/u/0/embed?mid=1G9X5uJngjJHg0tdY-ina7vWCCn4" width="1000" height="500"></iframe>
