---
title: Tonndorf
slug: tonndorf
author: Stefan Imhoff
date: 2015-06-14T18:00:00+02:00
distance: 15
duration: 3:32
---

In Tonndorf befindet sich die *Bio-Bäckerei Springer*, wo es fantastisch nach frischem Brot und Backwaren riecht. Gegenüber befindet sich eine großer Bürokomplex, in dem Firmen wie *Stressless* oder auch Otto Büros haben.

{{< image src="stadtteile/tonndorf-01" alt="Bürokomplex in Tonndorf" width="1000" >}}

Südlich des *Friedrich-Ebert-Damm* gibt es ein Wohngebiet, was wenig gepflegt ist. Alles ist mit Moos bewachsen, sogar die Straßenschilder. Die Grundstücke sind oft mit Grün überwachsen und sehen vernachlässigt aus. Die Fußwege sind schlecht, oft kaum richtig zu Fuß zu begehen.

Noch weiter südlich befindet sich überwiegend Industrie und Gewerbe. Alles ist ziemlich trostlos, ungepflegt und hässlich. Es gibt unzählige Gelände mit Industrie für Auto (Auto-Reparatur, Reifenhandel). Und ein Gewerbegelände für Schienenbedarf.

{{< image src="stadtteile/tonndorf-02" alt="Gewerbegelände" width="1000" caption="Man kann Schienen mieten? Sachen gibt’s …" >}}

Im Wald zwischen *Rahlau* und *Wandse* ist es ruhig und schön. Dort befindet sich eine *KZ-Gedenkstätte* für ca. 500 Frauen des *KZ Drägerwerk*, die dort gefangen waren. Mehrere Frauen wurden misshandelt und eine sogar getötet, weil man ihr angebliche Sabotage vorwarf (ihr war eine Gasmaske heruntergefallen).

{{< image src="stadtteile/tonndorf-03" alt="KZ-Gedenkstätte Drägerwerk" width="1000" >}}

{{< image src="stadtteile/tonndorf-04" alt="KZ-Gedenkstätte Drägerwerk" width="1000" >}}

{{< image src="stadtteile/tonndorf-05" alt="KZ-Gedenkstätte Drägerwerk" width="1000" >}}

Auf dem *Friedhof Tonndorf* gibt es sehr alte Gräber, viele Soldaten des 2. Weltkrieges und einige größere Grabanlagen für Familien.

{{< image src="stadtteile/tonndorf-06" alt="Friedhof Tonndorf" width="1125" height="1500" >}}

Südlich der Gleise ist überwiegend Industriegelände (Kostümverleih, Autohäuser, THW-Gelände). Es gibt ein riesiges Gelände, was wie ausgestorben aussieht, welches der Bundeswehr gehört. Es ist ein Entsorgungslager und ist gut gesichert.

{{< image src="stadtteile/tonndorf-07" alt="Bahnübergang" width="1000" caption="Bahnübergang. Hier muss man wirklich sehr oft und sehr lange warten." >}}

{{< image src="stadtteile/tonndorf-08" alt="Ein alter, verwitterter Friedhof" width="1000" caption="Zwischen zwei Industriegeländen: Ein alter, verwitterter Friedhof." >}}

{{< image src="stadtteile/tonndorf-09" alt="THW" width="1000" caption="Den Mitarbeitern des THW war wohl langweilig: Eine Bank gebaut aus Euro-Paletten." >}}

{{< image src="stadtteile/tonndorf-10" alt="Entsorgungslager der Bundeswehr" width="1000" caption="Mitten in Tonndorf: Ein Entsorgungslager der Bundeswehr. Was hier wohl entsorgt wird? Gut gesichert ist es jedenfalls." >}}

Nahe des Bahnhofs Hamburg-Tonndorf befindet sich das *Studio Hamburg* und ein großes Einkaufszentrum. Östlich davon befindet sich wieder Wohngebiet, überwiegen Einzelhäuser, sehr klein, z. T. etwas verwittert, zwischendurch aber auch gepflegte Häuser und ein paar Neubauten. Nördlich der Gleise sieht es ähnlich aus.

{{< image src="stadtteile/tonndorf-11" alt="Studio Hamburg" width="1000" caption="Das Studio Hamburg, die haben eine Menge Antennen und Satelliten-Schüsseln auf dem Dach." >}}

Am *Ostender Teich* befindet sich ein Freibad, das Tor war offen und ich konnte einfach so hineingehen. Es gibt einen kleinen Strand und einen Bereich, in dem man schwimmen kann.

{{< image src="stadtteile/tonndorf-12" alt="Ostender Teich" width="1000" >}}

{{< image src="stadtteile/tonndorf-13" alt="Ostender Teich" width="1000" >}}

{{< image src="stadtteile/tonndorf-14" alt="Ostender Teich" width="1000" >}}

Nördlich der *Wandse* befinden sich 3-5-stöckige Häuser, einige sehr neu und äußerst hübsch. Eine nagelneue Wohnanlage ist gerade am Bau, die direkt am Wald liegt.

{{< image src="stadtteile/tonndorf-15" alt="Wohngebiet nördlich der Wandse" width="1000" >}}

{{< image src="stadtteile/tonndorf-16" alt="Wohngebiet nördlich der Wandse" width="1000" >}}

Um die *Pohlmannteiche* befinden sich wieder Kleingartenanlagen, ein Zugang zu den Seen ist nur Besitzern möglich (*Strandpark Ziegelsee*).

{{< image src="stadtteile/tonndorf-17" alt="Strandpark Ziegelsee: Wer in diesem Kleingartenverein ein Grundstück sein Eigen nennt, hat exklusiven Zugang zu einigen wunderschönen Seen." width="1125" height="1500" caption="Strandpark Ziegelsee: Wer in diesem Kleingartenverein ein Grundstück sein Eigen nennt, hat exklusiven Zugang zu einigen wunderschönen Seen." >}}

{{< image src="stadtteile/tonndorf-20" alt="Strandpark Ziegelsee" width="1000" >}}

{{< image src="stadtteile/tonndorf-19" alt="Strandpark Ziegelsee" width="1000" >}}

{{< image src="stadtteile/tonndorf-18" alt="Strandpark Ziegelsee" width="1125" height="1500" >}}

{{< image src="map/tonndorf" alt="Tonndorf" width="1000" height="956" >}}

<iframe class="map" src="https://www.google.com/maps/d/u/0/embed?mid=15seOWRQJj3cIfHjq74xqjrgD7Mg" width="1000" height="500">
</iframe>
