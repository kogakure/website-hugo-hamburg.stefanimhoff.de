---
title: Borgfelde
slug: borgfelde
author: Stefan Imhoff
date: 2015-08-28T18:00:00+02:00
distance: 6
duration: 1:23
---

*Borgfelde* ist einer der kleinsten Stadtteile Hamburgs. Meine Tour habe ich an der U-Bahn-Station *Burgstraße* begonnen und bin zuerst durch das *obere Borgfelde* gegangen.

{{< image src="stadtteile/borgfelde-01" alt="Gebäude in Borgfelde" width="1000" height="1334" >}}

{{< image src="stadtteile/borgfelde-02" alt="Kirche in Borgfelde" width="1000" height="1334" >}}

*Borgfelde* ist geteilt in *oberes* und *unteres Borgfelde*. Früher lebten die reichen Bürger in schönen Häusern auf dem Hang und im unteren *Borgfelde* lebten die Arbeiter. Dazwischen befindet sich ein steiler Abhang, der *Geesthang* heißt.

{{< image src="stadtteile/borgfelde-03" alt="Park am Geethang" width="1000" >}}

Da große Teile von *Borgfelde* den Bomben zum Opfer gefallen sind, gibt es kaum ältere Häuser, meist 4-5 stöckiger Bau der Nachkriegszeit. Im *oberen Borgfelde* vor dem *Geesthang* gibt es einen langen Park, in dem die Plastik *Drei Vogelsäulen für Borgfelde* von *Klaus Becker* steht. Eine Reihe von Bänken stehen entlang des Parkwegs, doch ist es durch den Lärm der *Borgfelder Straße* keine Freude hier zu sitzen.

{{< image src="stadtteile/borgfelde-04" alt="Park am Geesthang" width="1000" >}}

{{< image src="stadtteile/borgfelde-05" alt="Plastik &quot;Drei Vogelsäulen für Borgfelde&quot; von Klaus Becker" width="1000" height="1334" caption="Plastik <em>Drei Vogelsäulen für Borgfelde</em> von Klaus Becker" >}}

An der Wand der *Erlöserkirche* in der *Jungestraße* gibt es das Kriegsopfermahnmal von *Hans Kock*.

{{< image src="stadtteile/borgfelde-06" alt="Erlöserkirche" width="1000" height="1334" caption="Erlöserkirche" >}}

{{< image src="stadtteile/borgfelde-07" alt="Kriegsopfermahnmal von Hans Kock" width="1000" caption="Kriegsopfermahnmal von Hans Kock" >}}

Nachdem ich durch die Straßen des *oberen Borgfelde* gegangen war, bin ich über die einzige Steintreppe am *Geesthang* in den unteren Teil des Stadtteils hinabgestiegen. Hier gibt es kaum noch Wohnhäuser, sondern überwiegend Gewerbe: viele Autohäuser, Banken, Bürogebäude, mehrere Schulen, einige Ämter und städtische Gebäude und das HCAT (*Hamburg Centre of Aviation Training*).

{{< image src="stadtteile/borgfelde-08" alt="Borgfelde" width="1000" height="1334" >}}

{{< image src="stadtteile/borgfelde-09" alt="Borgfelde" width="1000" height="1334" >}}

{{< image src="stadtteile/borgfelde-10" alt="Borgfelde" width="1000" height="1334" >}}

{{< image src="stadtteile/borgfelde-11" alt="Die Treppe vom oberen Borgfelde in das untere Borgfelde" width="1000" caption="Die Treppe vom oberen Borgfelde in das untere Borgfelde" >}}

{{< image src="stadtteile/borgfelde-12" alt="Unten Borgfelde und der Geesthang" width="1000" >}}

{{< image src="stadtteile/borgfelde-13" alt="HCAT (Hamburg Centre of Aviation Training)" width="1000" >}}

{{< image src="stadtteile/borgfelde-14" alt="Unten Borgfelde" width="1000" height="1334" >}}

{{< image src="stadtteile/borgfelde-15" alt="Unten Borgfelde" width="1000" >}}

Ich bin die *Anckelmannstraße* hinunter gegangen und dann die *Eiffelstraße* entlang und dann über den *Mittelkanal* und durch die *Wendenstraße*. Am Ufer vor dem Zentrum für Aus- und Fortbildung haben mehrere Personen geangelt.

{{< image src="stadtteile/borgfelde-20" alt="Mittelkanal in Borgfelde" width="1000" caption="Mittelkanal in Borgfelde" >}}

{{< image src="stadtteile/borgfelde-21" alt="Mittelkanal in Borgfelde" width="1000" >}}

{{< image src="stadtteile/borgfelde-22" alt="Kanal in Borgfelde" width="1000" height="1334" >}}

{{< image src="stadtteile/borgfelde-23" alt="Kanal in Borgfelde" width="1000" >}}

{{< image src="stadtteile/borgfelde-24" alt="Mittelkanal in Borgfelde" width="1000" >}}

Das *untere Borgfelde* ist am Abend wie ausgestorben, bis auf ein paar Gesänge aus einem Gebäude in der Nähe des Kanals war keine Spur von Menschen zu finden. Ich bin an einem muslimischen Zentrum vorbeigekommen und an ein paar verwaisten Gewerbegeländen, die vergeblich nach Mietern suchen.

{{< image src="stadtteile/borgfelde-16" alt="Gewerbegebiete in Unten Borgelde" width="1000" height="1334" >}}

{{< image src="stadtteile/borgfelde-17" alt="Gewerbegebiete in Unten Borgelde" width="1000" >}}

{{< image src="stadtteile/borgfelde-18" alt="Gewerbegebiete in Unten Borgelde" width="1000" height="1334" >}}

{{< image src="stadtteile/borgfelde-19" alt="Gewerbegebiete in Unten Borgelde" width="1000" >}}

Direkt in der Nähe zum S- und U-Bahnhof *Berliner Tor* gibt es einen Park und dort steht ein Rundbunker aus dem Krieg. Schön ist es hier aber nicht, der Lärm der Autos, die von den *Elbbrücken* nach Hamburg einfahren und der zahlreiche Dreck und Müll sorgen nur dafür, dass man den Park nur wieder schnell verlassen möchte. Am U-Bahnhof *Berliner Tor* endete meine Tour durch *Borgfelde*.

{{< image src="stadtteile/borgfelde-25" alt="Nahe des Berliner Tors" width="1000" >}}

{{< image src="stadtteile/borgfelde-27" alt="Park mit Rundbunker in der Nähe des U- und S-Bahnhofs Berliner Tor" width="1000" height="1334" caption="Park mit Rundbunker in der Nähe des U- und S-Bahnhofs Berliner Tor" >}}

{{< image src="stadtteile/borgfelde-28" alt="Park in der Nähe des U- und S-Bahnhofs Berliner Tor" width="1000" >}}

{{< image src="stadtteile/borgfelde-26" alt="U- und S-Bahn-Station Berliner Tor" width="1000" >}}

{{< image src="map/borgfelde" alt="Borgfelde" width="1000" height="956" >}}

<iframe class="map" src="https://www.google.com/maps/d/u/0/embed?mid=1RpVtrvZpzpLrLvZACEnKWzRdyng" width="1000" height="500"></iframe>
