---
title: Hammerbrook
slug: hammerbrook
author: Stefan Imhoff
date: 2015-09-12T18:00:00+02:00
distance: 11
duration: 2:39
---

Vom Hauptbahnhof aus bin ich nach Süden zum *Deichtorplatz* gegangen, vorbei an den *Deichtorhallen* bis zum *Oberhafen* und dann nach Südosten. Direkt am *Oberhafen* steht der *Fruchthof*, hier beginnt auch der Stadtteil *Hammerbrook*.

{{< image src="stadtteile/hammerbrook-01" alt="Hauptbahnhof" width="1000" >}}

{{< image src="stadtteile/hammerbrook-02" alt="Baustelle" width="1000" height="1334" >}}

{{< image src="stadtteile/hammerbrook-03" alt="Blick auf den Michel" width="1000" height="1334" >}}

{{< image src="stadtteile/hammerbrook-04" alt="Deichtorhallen" width="1000" >}}

{{< image src="stadtteile/hammerbrook-05" alt="Deichtorhallen" width="1000" >}}

{{< image src="stadtteile/hammerbrook-06" alt="Deichtorhallen" width="1000" height="1334" >}}

{{< image src="stadtteile/hammerbrook-07" alt="Deichtorhallen" width="1000" height="361" >}}

{{< image src="stadtteile/hammerbrook-08" alt="Spiegel-Gebäude" width="1000" >}}

{{< image src="stadtteile/hammerbrook-09" alt="Oberhafen" width="1000" >}}

{{< image src="stadtteile/hammerbrook-10" alt="Fruchhof Hamburg" width="1000" caption="Fruchhof Hamburg" >}}

{{< image src="stadtteile/hammerbrook-11" alt="Fruchhof Hamburg" width="1000" >}}

{{< image src="stadtteile/hammerbrook-12" alt="Fruchhof Hamburg" width="1000" height="1334" >}}

{{< image src="stadtteile/hammerbrook-13" alt="Brücke am Oberhafen" width="1000" >}}

{{< image src="stadtteile/hammerbrook-14" alt="Oberhafen" width="1000" >}}

{{< image src="stadtteile/hammerbrook-15" alt="Oberhafen" width="1000" >}}

Ich bin am Wasser entlang gegangen und habe mir die Schleuse zwischen *Oberhafen* und *Schleusenkanal* angesehen. Leider nur aus der Ferne, da irgendein Schlaumeier auf die tolle Idee kam, das Informationsschild und den besten Platz für ein Foto zu einem eingezäunten Gewerbegelände umzufunktionieren. Ich bin eine Weile dem Wasser gefolgt, bis ich einsehen musste, dass das riesige Gewerbegelände, auf dem ausschließlich Früchte gehandelt werden und das mir den Weg nach *Hammerbrook* hinein versperrte, sich bis zur *Bille* erstreckte. Also musste ich umkehren und es von der anderen Seite umwandern.

{{< image src="stadtteile/hammerbrook-16" alt="Schleusenkanal" width="1000" caption="Schleusenkanal" >}}

{{< image src="stadtteile/hammerbrook-17" alt="Schleusenkanal" width="1000" >}}

{{< image src="stadtteile/hammerbrook-18" alt="Brückenschmuck am Schleusenkanal" width="1000" height="1334" >}}

{{< image src="stadtteile/hammerbrook-19" alt="Schleusenkanal" width="1000" >}}

{{< image src="stadtteile/hammerbrook-28" alt="Gewerbegelände am Oberhafen" width="1000" caption="Hammerbrook hat überwiegend Gewerbe- und Industriegelände: einen großen Fruchhandel am Oberhafen, nur sehr wenig Wohnungen, dafür alte Fabrikgebäude und große Lagerhallen." >}}

Ich bin die *Amsinckstraße* hinunter gegangen, wo sich auf der einen Seite ausschließlich riesige Glasbauten und auf der anderen Seite der Obsthandel befand. Über die *Süderstraße* bin ich in das riesige Gewerbegebiet hinein gegangen, und der Straße *Grüner Deich* bis zum *Hochwasserbassin* gefolgt. In *Hammerbrook* haben viele große Versicherungen, Banken, Autohäuser oder Handelsunternehmen ihre Zentralen.

Die Stadt wird von mehreren Bahngleisen durchzogen, sowohl die S-Bahn vom Hauptbahnhof aus nach Süden, als auch die Gleise der Deutschen Bahn. Die Gleise verlaufen in der Höhe auf Brücken über der Stadt.

{{< image src="stadtteile/hammerbrook-20" alt="Bahngleise über Hammerbrook" width="1000" height="1334" >}}

{{< image src="stadtteile/hammerbrook-21" alt="Gewerbe in Hammerbrook" width="1000" >}}

{{< image src="stadtteile/hammerbrook-22" alt="Gewerbe in Hammerbrook" width="1000" height="1334" >}}

{{< image src="stadtteile/hammerbrook-23" alt="Gewerbe in Hammerbrook" width="1000" height="1334" >}}

Wohnhäuser gibt es kaum in *Hammerbrook*, es wohnen gerade einmal knapp über 2000 Menschen in dem großen Stadtteil, die meisten Häuser sind in sehr schlechtem Zustand und bieten wenig erstrebenswerten Wohnraum.

{{< image src="stadtteile/hammerbrook-24" alt="Gewerbe in Hammerbrook" width="1000" >}}

{{< image src="stadtteile/hammerbrook-25" alt="Wohnraum in Hammerbrook" width="1000" >}}

Von den Brücken über das *Hochwasserbassin* aus hat man einen fantastischen Blick auf den *Berliner Bogen*. Weiter im Osten von *Hammerbrook* sitzen Unternehmen wie die Stadtreinigung und die *SVG-Hamburg Straßenverkehrsgenossenschaft*. Hier gibt es auch eine Event-Location, die in einer alten Lagerhalle ihren Sitz hat und für Modeshootings oder ähnliches gebucht werden kann. Wer sein Auto zulassen möchte oder ein Kennzeichen braucht, wird ebenfalls in diesen Teil von *Hammerbrook* fahren müssen. Rund um die Zulassungsstelle haben sich allerlei Autohäuser, TÜV Hanse, Geschäfte für Schilder oder Schnellzulassungen angesiedelt.

{{< image src="stadtteile/hammerbrook-26" alt="Lagerhallen und Event-Location" width="1000" >}}

{{< image src="stadtteile/hammerbrook-27" alt="Graffiti" width="1000" >}}

{{< image src="stadtteile/hammerbrook-29" alt="Blick auf den Berliner Bogen" width="1000" height="1334" >}}

{{< image src="stadtteile/hammerbrook-30" alt="Blick auf den Berliner Bogen" width="1000" >}}

{{< image src="stadtteile/hammerbrook-31" alt="Kanäle in Hammerbrook" width="1000" >}}

{{< image src="stadtteile/hammerbrook-32" alt="Kanäle in Hammerbrook" width="1000" >}}

{{< image src="stadtteile/hammerbrook-33" alt="Kanäle in Hammerbrook" width="1000" height="1334" >}}

{{< image src="stadtteile/hammerbrook-34" alt="Kanäle in Hammerbrook" width="1000" >}}

{{< image src="stadtteile/hammerbrook-35" alt="Kanäle in Hammerbrook" width="1000" >}}

{{< image src="stadtteile/hammerbrook-36" alt="Hamburger Stadtreinigung" width="1000" height="1334" >}}

{{< image src="stadtteile/hammerbrook-37" alt="Zahlreiche Autohäuser und Zulassungsstellen" width="1000" height="1334" >}}

Über den *Südkanal* bin ich in den nordöstlichen Teil von *Hammerbrook* gegangen. Hier gibt es die *Berufliche Schule Recycling- und Umwelttechnik*, die zu Teilen in einem schönen alten Backsteinbau mit kunstvollen Verzierungen sitzt. Am Eingang der Schule steht ein riesiger aus Schrotteilen gebauter Roboter.

{{< image src="stadtteile/hammerbrook-38" alt="Berufliche Schule Recycling- und Umwelttechnik" width="1000" height="1334" caption="Berufliche Schule Recycling- und Umwelttechnik" >}}

{{< image src="stadtteile/hammerbrook-39" alt="Berufliche Schule Recycling- und Umwelttechnik" width="1000" height="1334" >}}

Ich bin die *Wendenstraße* entlang nach Westen gegangen, auf deren einer Seite *Hammerbrook* liegt und auf der anderen Seite *Borgfelde*. Diese Straße bin ich schon teilweise bei meiner Tour durch *Borgfelde* entlang gegangen.

Am Ufer des *Victoriakais* (gut von der *Wendenbrücke* aus zu sehen) liegen mehrere schwimmende Häuser (*Floating Homes*) vor Anker. Hier kann man vor dem Kauf eines solchen Hauses die verschiedenen Modelle besichtigen.

{{< image src="stadtteile/hammerbrook-40" alt="Schwimmende Häuser am Victoriakai" width="1000" caption="Schwimmende Häuser am Victoriakai" >}}

{{< image src="stadtteile/hammerbrook-41" alt="Kai 10" width="1000" caption="Kai 10" >}}

{{< image src="stadtteile/hammerbrook-43" alt="Kanäle in Hammerbrook" width="1000" >}}

Über den *Heidenkampsweg*, den man entlangfährt, wenn man Hamburg über die Landungsbrücken verlassen oder betreten will bin ich weiter nach Westen gegangen, vorbei an der S-Bahnstation *Hammerbrook* und dann über den *Mittelkanal* nach Norden. Ein modernes Bürogebäude reiht sich hier an das andere.

{{< image src="stadtteile/hammerbrook-44" alt="Bürogebäude in Hammerbrook" width="1000" height="1334" >}}

{{< image src="stadtteile/hammerbrook-45" alt="Bürogebäude in Hammerbrook" width="1000" >}}

{{< image src="stadtteile/hammerbrook-46" alt="Bürogebäude in Hammerbrook" width="1000" >}}

{{< image src="stadtteile/hammerbrook-47" alt="Bürogebäude in Hammerbrook" width="1000" height="1334" >}}

{{< image src="stadtteile/hammerbrook-48" alt="Bürogebäude in Hammerbrook" width="1000" >}}

{{< image src="stadtteile/hammerbrook-49" alt="Bürogebäude in Hammerbrook" width="1000" >}}

{{< image src="stadtteile/hammerbrook-50" alt="Bürogebäude in Hammerbrook" width="1000" >}}

{{< image src="stadtteile/hammerbrook-51" alt="Bürogebäude in Hammerbrook" width="1000" height="1334" >}}

{{< image src="stadtteile/hammerbrook-52" alt="Bürogebäude in Hammerbrook" width="1000" >}}

{{< image src="stadtteile/hammerbrook-53" alt="Bürogebäude in Hammerbrook" width="1000" height="470" caption="Eine Vielzahl von Versicherungen hat ihre Zentralen in Hammerbrook." >}}

{{< image src="stadtteile/hammerbrook-54" alt="Baustelle in Hammerbrook" width="1000" >}}

Den *Nagelsweg* hinunter zum *Mittelkanal*, wo *Circus Roncalli* auf einem riesigen Platz ihre Lager aufgeschlagen hatte. Am Ufer des *Mittelkanals* lag der *Kai 10*, eine schwimmende Event-Location für Tagungen und Feiern vor Anker, ein großes Fest war gerade im Gange.

{{< image src="stadtteile/hammerbrook-55" alt="Circus Roncalli" width="1000" >}}

{{< image src="stadtteile/hammerbrook-42" alt="Kai 10" width="1000" >}}

Ich bin den *Högerdamm* hinauf gegangen, auf der linken Seite zahllose Abstellgleise des Hauptbahnhofs und ein Design-Hotel, auf der rechten Seite einige der wenigen, sehr schönen, alten Häuser, die den Bombenangriff überstanden haben. Die Nähe zum Bahnhof schmälert leider den Zustand der Häuser, die wenig gepflegt sind. Ich bin unter der *Nordkanalbrücke* entlang gegangen, durch *Repsoldstraße* und *Rosenallee*, wo weitere alte, schöne Häuser stehen.

{{< image src="stadtteile/hammerbrook-56" alt="Ältere Häuser in Hammerbrook" width="1000" height="1334" caption="Einige wenige Häuser haben den Bombenangriff auf Hammerbrook überstanden." >}}

{{< image src="stadtteile/hammerbrook-57" alt="Design-Hotel" width="1000" >}}

{{< image src="stadtteile/hammerbrook-58" alt="Ältere Häuser in Hammerbrook" width="1000" >}}

{{< image src="stadtteile/hammerbrook-59" alt="Auto-Überführung" width="1000" height="1334" >}}

{{< image src="stadtteile/hammerbrook-60" alt="Auto-Überführung" width="1000" >}}

Von da aus durch *Spaldingstraße* und *Nordkanalstraße* nach Osten. Auch hier wieder ein Bürogebäude nach dem anderen, einige Hotels, die von ihrer Nähe zum Bahnhof profitieren. Außerdem eine große Skateboard-Anlage mit Halle und zahlreichen Rampen in der *Spaldingstraße*.

{{< image src="stadtteile/hammerbrook-61" alt="Ältere Gebäude in Hammerbrook" width="1000" height="1334" >}}

{{< image src="stadtteile/hammerbrook-62" alt="Ältere Gebäude in Hammerbrook" width="1000" height="1334" >}}

{{< image src="stadtteile/hammerbrook-63" alt="Ältere Gebäude in Hammerbrook" width="1000" height="1334" >}}

{{< image src="stadtteile/hammerbrook-64" alt="Stadtmission Hamburg" width="1000" height="1334" >}}

{{< image src="stadtteile/hammerbrook-65" alt="Ältere Gebäude in Hammerbrook" width="1000" height="1334" >}}

{{< image src="stadtteile/hammerbrook-66" alt="Ältere Gebäude in Hammerbrook" width="1000" >}}

{{< image src="stadtteile/hammerbrook-67" alt="Fußgängertunnel" width="1000" height="1334" >}}

{{< image src="stadtteile/hammerbrook-68" alt="Auto-Überführung" width="1000" >}}

{{< image src="stadtteile/hammerbrook-69" alt="Gewerbegebäude in Hammerbrook" width="1000" >}}

Meine Tour endete auf dem *Ankelmannsplatz*, direkt bei der S- und U-Bahnstation *Berliner Tor*, wo auch das moderne Glasgebäude *Berliner Bogen* steht, das mit 14.000 Quadratmetern Glasfläche größte Glasgebäude in Europa.

{{< image src="stadtteile/hammerbrook-70" alt="Berliner Bogen" width="1000" height="521" caption="Das Glasgebäude Berliner Bogen, mit 14.000 Quadratmetern Glasfläche das größte Glasgebäude in Europa." >}}

{{< image src="stadtteile/hammerbrook-71" alt="Berliner Bogen" width="1000" >}}

{{< image src="stadtteile/hammerbrook-72" alt="Berliner Bogen" width="1000" >}}

{{< image src="map/hammerbrook" alt="Hammerbrook" width="1000" height="956" >}}

<iframe class="map" src="https://www.google.com/maps/d/u/0/embed?mid=1p3UJHRb9zhVSrtVda6V-FVTKe8w" width="1000" height="500"></iframe>
