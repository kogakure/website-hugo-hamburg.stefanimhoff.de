---
title: Wandsbek
slug: wandsbek
author: Stefan Imhoff
date: 2015-06-20T18:00:00+02:00
distance: 16
duration: 3:27
---

Im Norden von *Wandsbek* gibt es viele kleine Siedlungen, die sehr hübsch sind, Doppelhaushälften, alles ist gepflegt und sauber. Man merkt, dass die Wandsbeker stolz auf ihren Stadtteil sind.

{{< image src="stadtteile/wandsbek-01" alt="Wandsbeker Häuschen" width="1000" caption="Typische Wandsbeker Häuschen, gepflegt, mit Hecke und Rosen vorm Haus." >}}

Nahe des *Bundeswehrkrankenhauses* überwiegend höhere Häuser (Hochhäuser), einige Aldi und Lidl-Märkte. Das Bundeswehrkrankenhaus ist für Privatpersonen nicht zu betreten, da militärisches Gelände.

Dann südlich wieder viele kleine Häuser, hübsch und gepflegt, weiter über den Friedrich-Ebert-Damm. Vorbei an einem großen REWE-Markt und dann am *Betriebshof Wandsbek* der Hochbahn, wo riesige Mengen von Bussen geparkt sind.

Entlang an der *Wandse* durch einen Park und nach Süden zum *Staatsarchiv*, in dem die Original-Urkunden zur Stadtgeschichte und zahlreiche Akten aufbewahrt werden.

{{< image src="stadtteile/wandsbek-02" alt="Staatsarchiv" width="1000" >}}

Direkt daneben ist der *jüdische Friedhof Wandsbek* von 1637, geschlossen 1886, der aber nicht betreten werden kann und ein Denkmahl ist. Darauf befinden sich lauter uralte Grabsteine mit Hebräischen Zeichen.

{{< image src="stadtteile/wandsbek-03" alt="Jüdischer Friedhof Wandsbek" width="1000" >}}

{{< image src="stadtteile/wandsbek-04" alt="Jüdischer Friedhof Wandsbek" width="1000" >}}

{{< image src="stadtteile/wandsbek-05" alt="Jüdischer Friedhof Wandsbek" width="1000" >}}

Dann durch das *Einkaufszentrum Quarree*, das wirklich enorm groß ist, es erstreckt sich über mehrere Straßen und hat Brücken zwischen den Gebäuden.

{{< image src="stadtteile/wandsbek-06" alt="Einkaufszentrum Quarree" width="1000" >}}

Weiter nach Süden zum *Wandsbeker Marktplatz*, wo sich zwei Löwen-Plastiken befinden. Sie standen ursprünglich am Haupteingang der Schlossanlage *Heinrich Carl von Schimmelmanns*. Sie stehen unter Denkmahlsschutz und stehen heute auf dem Marktplatz.

{{< image src="stadtteile/wandsbek-07" alt="wandsbek-07" width="1000" >}}

{{< image src="stadtteile/wandsbek-08" alt="wandsbek-08" width="1000" >}}

In der Nähe des ehemaligen Schlosses gibt es viele, schöne, alte Häuser mit Stuck. Das Schloss selbst steht nicht mehr, dort befindet sich heute die Stadtverwaltung von Wandsbek.

{{< image src="stadtteile/wandsbek-09" alt="Schöne, alte Stuckbauten" width="1000" >}}

Im Wandsbeker Gehölz befindet sich ein *Gedenkstein für Matthias Claudius*, des bekanntesten Wandsbeker Einwohners. Er hat viele Gedichte geschrieben und leitete die Zeitung *Wandsbeker Bote* von 1771-1775.

{{< image src="stadtteile/wandsbek-10" alt="Gedenkstein für Matthias Claudius" width="1125" height="1500" >}}

Dann vorbei am *Matthias-Claudius-Gymnasium* und an der Römisch katholischen *Kirchengemeinde St. Joseph* vorbei.

{{< image src="stadtteile/wandsbek-11" alt="Kirche St. Joseph" width="1000" >}}

{{< image src="stadtteile/wandsbek-12" alt="Kirche St. Joseph" width="1125" height="1500" >}}

In der Böhmerstraße steht das *Heimatmuseum Wandsbek*, in einem Gebäude, dass 1870 von den Töchtern des 1784 aus England zugewanderten Kaufmanns *Joseph Morewood* als Altenwohnstift gegründet wurde.

{{< image src="stadtteile/wandsbek-13" alt="Heimatmuseum Wandsbek (Morewood-Stift)" width="1000" >}}

Dann am *Charlotte-Paulsen-Gymnasium* vorbei, was 1914 gebaut wurde.

{{< image src="stadtteile/wandsbek-14" alt="Charlotte-Paulsen-Gymnasium" width="1000" >}}

{{< image src="stadtteile/wandsbek-16" alt="Charlotte-Paulsen-Gymnasium" width="1000" >}}

{{< image src="stadtteile/wandsbek-15" alt="Charlotte-Paulsen-Gymnasium" width="1125" height="1500" >}}

Daneben das *Bovehaus*, das 1861 von *Christian Bove* errichtet wurde, der nach 30 Jahren als erfolgreicher Kaufmann aus Argentinien zurückgekehrt war.

{{< image src="stadtteile/wandsbek-17" alt="Bovehaus" width="1000" height="934" >}}

Dann unter der Eisenbahnbrücke hindurch an den Gleisen entlang. Auf der rechten Seite recht hübsche Siedlungen. Am Rand von Wandsbek wieder nach Norden und auf der anderen Seite entlang der Gleise.

Dann am *Nestlé Chocoladen Werk Hamburg* vorbei, wo es fantastisch nach Süßigkeiten roch.

{{< image src="stadtteile/wandsbek-18" alt="Nestlé Chocoladen Werk Hamburg" width="1000" >}}

Nach Norden vorbei an Staples und Kentucky Fried Chicken. Weiter durch ein paar Siedlungen und das Gewerbegebiet, wo sich eine Menge Autohäuser befinden. Am *UCI-Wandsbek* vorbei und weiter nach Norden durch ein weiteres Gewerbegebiet, wo sich neben zahlreichen Autohäusern auch das Flatrate-Bordell *Geizhaus* befindet.

{{< image src="map/wandsbek" alt="Wandsbek" width="1000" height="956" >}}

<iframe class="map" src="https://www.google.com/maps/d/u/0/embed?mid=1hN1sxADZkZP93w3CgHGzvs9HoLg" width="1000" height="500">
</iframe>
