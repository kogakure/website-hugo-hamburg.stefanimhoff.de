---
layout: error
title: Serverfehler!
url: "/500.html"
robots: "noindex,follow"
body_class: error
sitemap_exclude: true
---

## Doh! Etwas ist richtig kaputt!

Gehe zurück zur [Homepage](/) und versucht dein Glück dort noch einmal. Schick mir doch bitte eine [E-Mail](/impressum/), damit ich den Serverfehler behebe.

{{% youtube uRGljemfwUE %}}
